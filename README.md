# Overview

B-B-Breakpad is a fork of [Google's Breakpad](https://code.google.com/p/google-breakpad/) (based on SVN r1458) and it is mainly aimed to support BSD systems. Due to personal needs, I have also improved support for Linux targets completing support for PowerPC (but not PowerPC64).

The project builds with CMake, and the autoconf stuff is outdated and has been removed unless someone feels the need to fix it.

The Win32 support has never been tested, it's there, but I am not sure it will work. You will need a C99 compliant stdint.h and inttypes.h. You're better off with some recent version of Visual Studio in case. There's no install phase for this target.

Solaris is known but is completely left alone and untested.

There is only one library and it is shared, so there's no client/server split at the moment.

The iOS and Android are probably non functional.

# Building

## Prerequsities

You'll need the following libraries installed in the system:

* [curl](http://curl.haxx.se)

the following projects in source form:

* [gflags](http://gflags.github.io/gflags/)
* [gtest](https://code.google.com/p/googletest/)
* [gmock](https://code.google.com/p/googlemock/)

the following programs:

* [cmake](http://www.cmake.org)

## Building

1. Create a working directory where to put bbbreakpad, gflags, gtest, gmock.


		~ $ mkdir workdir
  
2. Check out the code

		~ $ cd workdir
		~/workdir $ git clone https://bitbucket.com/tienex/bbbreakpad.git
		~/workdir $ git clone https://github.com/gflags/gflags.git
		~/workdir $ svn checkout http://googletest.googlecode.com/svn/trunk/ gtest
		~/workdir $ svn checkout http://googlemock.googlecode.com/svn/trunk/ gmock

2. If you're targeting a BSD system, apply the patch for gtest

		~/workdir $ cd gtest
		~/workdir/gtest $ patch -p1 < ../bbbreakpad/patches/gtest-bsd.patch
		~/workdir/gtest $ cd ..

3. Link the gtest, gflags, gmock directories in the bbbreakpad directory

		~/workdir $ cd bbbreakpad
		~/workdir/bbbreakpad $ ln -sf ../gflags .
		~/workdir/bbbreakpad $ ln -sf ../gtest .
		~/workdir/bbbreakpad $ ln -sf ../gmock .
		~/workdir $ cd ..

3. Create a directory and build

		~/workdir $ mkdir build
		~/workdir $ cd build
		~/workdir/build $ cmake ../bbbreakpad
		<cmake output here>
		~/workdir/build $ make

4. Execute tests (optionally)

		~/workdir/build $ make test

5. Install

		~/workdir/build $ make install

# Usage

The main difference with Google Breakpad is that to use bbbreakpad, you need to simply include &lt;bbbreakpad/bbbreakpad.h&gt; and link against bbbreakpad library (built in both static and shared form), or bbbreakpad_debug.

Refer to [Google Breakpad wiki](https://code.google.com/p/google-breakpad/w/list) for examples and usage.

A simple example is included, additions/examples/hello.cc.

		
# Status

## Versions used for testing

* Linux 3.4 (ARM)
* Linux 3.16 (MIPS)
* Linux 3.19 (PowerPC, i386, x86-64)
* FreeBSD 11-CURRENT
* NetBSD 6.1.5
* OpenBSD 5.7
* DragonFly 4.0.5
* Mac OS X Yosemite

## Targets

		Target   |Architecture|Status      |Caveats
		---------+------------+------------+-----------------------
		Mac OS X |    i386    |Supported   | No UI code is built otherwise fully supported.
		Mac OS X |   x86_64   |Supported   | No UI code is built otherwise fully supported.
		Linux    |    i386    |Supported   | None, fully supported.
		Linux    |   x86_64   |Supported   | None, fully supported.
		Linux    |    mips    |Supported   | None, fully supported.
		Linux    |    arm     |Supported   | None, fully supported.
		Linux    |   powerpc  |Supported   | None, fully supported.
		FreeBSD  |    i386    |Supported   | None, fully supported.
		FreeBSD  |   x86_64   |Supported   | None, fully supported.
		DragonFly|   x86_64   |Experimental| Need kernel patch to obtain threads context.
		OpenBSD  |    i386    |Experimental| Lost signals on ptrace(2) detach (ugly workaround included).
		OpenBSD  |   x86_64   |Experimental| Lost signals on ptrace(2) detach (ugly workaround included).
		NetBSD   |    i386    |Experimental| Some tests hang on completion through ctest, but pass.
		NetBSD   |   x86_64   |Experimental| Some tests hang on completion through ctest, but pass.
		NetBSD   |    vax     |Reference   | Used as a reference to port to other architectures.

# Issues

## OpenBSD

OpenBSD requires special care in order to make bbbreakpad work, in general these changes may weaken the security provided by the system:

1. You need to setuid the /usr/sbin/procmap binary, as it is the only way to obtain the mappings of a process from non-root processes. For root, the code will try to obtain the mappings via sysctl(2).
2. You need to set sysctl(2) variable kern.global_ptrace to 1 or information cannot be obtained.

## DragonFly

DragonFly needs a kernel patch in order to work, found in patches.

# Wish list

Generally speaking, FreeBSD is the one doing things best. All the information about a process can be obtained via sysctl(2), this includes ELF Aux Vector, Memory Mappings (with paths), Command Line Arguments and Environment. While some of the BSD do allow some of the information to be obtained the same way, some other do not. Due to this constraint, some information must be obtained by scavenging the stack of the target process, which makes everything difficult as a child is denied to attach via ptrace(2) a parent.

Follows a wish list for every target to improve support.

## FreeBSD

1. Get rid of libcxxrt and use LLVM libcxxabi, or at least replace the cxa_demangle with the LLVM version, the shipped one is extremely broken. Bbbreakpad includes LLVM cxa_demangle and will compile it if Clang is detected.
 
## DragonFly

1. Obtain the ELF Aux Vector like FreeBSD via sysctl(2) so to avoid scavenging the stack, or alternatively via /proc.
2. Obtain the process strings like FreeBSD via sysctl(2) so to avoid scavenging the stack, or alternatively via /proc.
3. Apply the kernel patch included to allow obtaining registers per thread.
4. It is the only BSD that when a process is attached via ptrace(2), and another is wait(2)ing on it, the waiting one is interrupted with errno ECHILD. Is this intended?
5. What is wrong with the FPU context? Where is the x86-64 state? Why is it looking like i386?
6. ELF Aux Vector in the core file, so to avoid to scavenge the stored stack.
7. Pointer to Process Strings in the core file, so to avoid to scavenge the stored stack.

## OpenBSD

1. Fix the lost signals on ptrace(2) detach. This can be seen also with GDB, attaching a process with many (40 are a good start on a 4-way SMP system) threads, all in a busy loop, and then detaching, will leave the process in stopped state.
2. Would be nice obtaining the ELF Aux Vector via sysctl(2) rather than ptrace(2).
3. Pointer to Process Strings in the core file, so to avoid to scavenge the stored stack.
4. Too secure makes life hard! :-)

## NetBSD

1. Obtain the ELF AUXV like FreeBSD via sysctl(2) so to avoid scavenging the stack, or alternatively via /proc.
2. Obtain the process strings like FreeBSD via sysctl(2) so to avoid scavenging the stack, or alternatively via /proc.
3. Make /proc/&lt;pid&gt;/maps work like DragonFly; it is annoying that reading the procfs files will result in truncated contents.
4. Why are some tests getting stuck?
5. Write PT_XMMREGS in core files on i386.
6. ELF Aux Vector in the core file, so to avoid to scavenge the stored stack.
7. Pointer to Process Strings in the core file, so to avoid to scavenge the stored stack.
8. 32-bits processes on 64-bits architecture fail badly when ptrace(2) is used. Invoking ptrace(PT_IO) will result in EINVAL and the process abruptly terminates thereafter with no apparent reason. Trying to debug the process is impossible.

# License

The licensing terms are included in file LICENSE found in the top level directory.
