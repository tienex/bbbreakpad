#! /bin/sh

if [ $# -lt 1 ]; then
    echo "usage: $0 <sourcepath>"
    exit 1
fi


system="$(uname -s)"
machine="i386"

sourcedir="$(pwd)/${1+"$@"}"
builddir="build-$system-$machine"
release_builddir="$(pwd)/$builddir-release"
debug_builddir="$(pwd)/$builddir-debug"

# Release
(rm -fr "$release_builddir" && \
 mkdir -p "$release_builddir" && \
 cd "$release_builddir" && \
 cmake -G Ninja -DCMAKE_TOOLCHAIN_FILE="$sourcedir/additions/toolchain32.cmake" -DCMAKE_INSTALL_PREFIX="/opt/bbbreakpad" -DCMAKE_BUILD_TYPE="Release" "$sourcedir" && \
 ninja && \
 ctest -VV)

(rm -fr "$debug_builddir" && \
 mkdir -p "$debug_builddir" && \
 cd "$debug_builddir" && \
 cmake -G Ninja -DCMAKE_TOOLCHAIN_FILE="$sourcedir/additions/toolchain32.cmake" -DCMAKE_INSTALL_PREFIX="/opt/bbbreakpad" -DCMAKE_BUILD_TYPE="Debug" "$sourcedir" && \
 ninja && \
 ctest -VV)

