#include <bbbreakpad/bbbreakpad.h>

#if defined(__APPLE__)
static bool dumpCallback(const char *dump_dir,
                         const char *minidump_id,
                         void *context,
                         bool succeeded)
{
  printf("Dump path: %s/%s.dmp\n", dump_dir, minidump_id);
  return succeeded;
}
#else
static bool dumpCallback(const google_breakpad::MinidumpDescriptor &descriptor,
                         void *context,
                         bool succeeded)
{
  printf("Dump path: %s\n", descriptor.path());
  return succeeded;
}
#endif

void crashAndBurn()
{
  volatile int* a = (int*)(NULL);
  *a = 1;
}


int main(int argc, char *argv[])
{
#if defined(__APPLE__)
  google_breakpad::ExceptionHandler eh("/tmp", NULL, dumpCallback,
                                       NULL, true, NULL);
#else
  google_breakpad::MinidumpDescriptor descriptor("/tmp");
  google_breakpad::ExceptionHandler eh(descriptor, NULL, dumpCallback,
                                       NULL, true, -1);
#endif

  crashAndBurn();

  return 0;
}
