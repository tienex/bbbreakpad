// Copyright (c) 2015 Orlando Bassotto.
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are
// met:
//
//     * Redistributions of source code must retain the above copyright
// notice, this list of conditions and the following disclaimer.
//     * Redistributions in binary form must reproduce the above
// copyright notice, this list of conditions and the following disclaimer
// in the documentation and/or other materials provided with the
// distribution.
//     * Neither the name of Google Inc. nor the names of its
// contributors may be used to endorse or promote products derived from
// this software without specific prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
// "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
// LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
// A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
// OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
// SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
// LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
// DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
// THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
// (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
// OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

#include "processor/stackwalker_vax.h"
#include "google_breakpad/processor/call_stack.h"
#include "google_breakpad/processor/memory_region.h"
#include "google_breakpad/processor/stack_frame_cpu.h"
#include "processor/logging.h"

namespace google_breakpad {


StackwalkerVAX::StackwalkerVAX(const SystemInfo* system_info,
                               const MDRawContextVAX* context,
                               MemoryRegion* memory,
                               const CodeModules* modules,
                               StackFrameSymbolizer* resolver_helper)
    : Stackwalker(system_info, memory, modules, resolver_helper),
      context_(context) {
  if (memory_ && memory_->GetBase() + memory_->GetSize() - 1 > 0xffffffff) {
    BPLOG(ERROR) << "Memory out of range for stackwalking: " <<
                    HexString(memory_->GetBase()) << "+" <<
                    HexString(memory_->GetSize());
    memory_ = NULL;
  }
}


StackFrame* StackwalkerVAX::GetContextFrame() {
  if (!context_) {
    BPLOG(ERROR) << "Can't get context frame without context";
    return NULL;
  }

  StackFrameVAX* frame = new StackFrameVAX();

  // The instruction pointer is stored directly in a register, so pull it
  // straight out of the CPU context structure.
  frame->context = *context_;
  frame->context_validity = StackFrameVAX::CONTEXT_VALID_ALL;
  frame->trust = StackFrame::FRAME_TRUST_CONTEXT;
  frame->instruction = frame->context.iregs[MD_CONTEXT_VAX_REG_PC];

  return frame;
}


StackFrame* StackwalkerVAX::GetCallerFrame(const CallStack* stack,
                                           bool stack_scan_allowed) {
  if (!memory_ || !stack) {
    BPLOG(ERROR) << "Can't get caller frame without memory or stack";
    return NULL;
  }

  StackFrameVAX* last_frame = static_cast<StackFrameVAX*>(
      stack->frames()->back());

  uint32_t last_fp = last_frame->context.iregs[MD_CONTEXT_VAX_REG_FP];

  uint32_t caller_ap = 0;
  if (!memory_->GetMemoryAtAddress(last_fp + 8, &caller_ap))
    return NULL;

  uint32_t caller_fp = 0;
  if (!memory_->GetMemoryAtAddress(last_fp + 12, &caller_fp))
    return NULL;

  uint32_t caller_pc = 0;
  if (!memory_->GetMemoryAtAddress(last_fp + 16, &caller_pc))
    return NULL;

  uint32_t caller_sp = last_fp ? last_fp + 20 :
      last_frame->context.iregs[MD_CONTEXT_VAX_REG_SP];

  StackFrameVAX* frame = new StackFrameVAX();

  frame->context = last_frame->context;
  frame->context.iregs[MD_CONTEXT_VAX_REG_PC] = caller_pc;
  frame->context.iregs[MD_CONTEXT_VAX_REG_AP] = caller_ap;
  frame->context.iregs[MD_CONTEXT_VAX_REG_FP] = caller_fp;
  frame->context.iregs[MD_CONTEXT_VAX_REG_SP] = caller_sp;
  frame->context_validity = StackFrameVAX::CONTEXT_VALID_FP |
                            StackFrameVAX::CONTEXT_VALID_AP |
                            StackFrameVAX::CONTEXT_VALID_SP |
                            StackFrameVAX::CONTEXT_VALID_PC;
  frame->trust = StackFrame::FRAME_TRUST_FP;

  // An instruction address of zero marks the end of the stack.
  if (frame->context.iregs[MD_CONTEXT_ARM_REG_PC] == 0)
    return NULL;

  // If the new stack pointer is at a lower address than the old, then
  // that's clearly incorrect. Treat this as end-of-stack to enforce
  // progress and avoid infinite loops.
  if (frame->context.iregs[MD_CONTEXT_ARM_REG_SP]
      < last_frame->context.iregs[MD_CONTEXT_ARM_REG_SP])
    return NULL;

  frame->instruction = frame->context.iregs[MD_CONTEXT_VAX_REG_PC];

  return frame;
}


}  // namespace google_breakpad
