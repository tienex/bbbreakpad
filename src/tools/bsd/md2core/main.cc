// Copyright (c) 2015, Orlando Bassotto.
// Copyright (c) 2009, Google Inc.
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are
// met:
//
//     * Redistributions of source code must retain the above copyright
// notice, this list of conditions and the following disclaimer.
//     * Redistributions in binary form must reproduce the above
// copyright notice, this list of conditions and the following disclaimer
// in the documentation and/or other materials provided with the
// distribution.
//     * Neither the name of Google Inc. nor the names of its
// contributors may be used to endorse or promote products derived from
// this software without specific prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
// "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
// LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
// A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
// OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
// SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
// LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
// DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
// THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
// (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
// OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

// Converts a minidump file to a core file which gdb can read.
// Large parts lifted from the userspace core dumper:
//   http://code.google.com/p/google-coredumper/
//
// Usage: minidump-2-core [-v] [--usesobase] [--sobasedir <path>] 1234.dmp 1234.core

#include "tools/bsd/md2core/core_dump_writer.h"

using google_breakpad::MemoryMappedFile;
using google_breakpad::MinidumpMemoryRange;
using google_breakpad::md2core::CoreDumpWriter;

static int usage(const char* argv0) {
  fprintf(stderr, "Usage: %s [--usesobase] [--sobasedir <path>] [-v] "
          "<minidump file> <core file>\n", argv0);
  return EXIT_FAILURE;
}

int
main(int argc, char** argv) {
  int argi = 1;
  bool verbose = false;
  bool use_so_basedir = false;
  std::string custom_so_basedir;
  while (argi < argc && argv[argi][0] == '-') {
    if (!strcmp(argv[argi], "-v")) {
      verbose = true;
    } else if (!strcmp(argv[argi], "--usesobase")) {
      use_so_basedir = true;
    } else if (!strcmp(argv[argi], "--sobasedir")) {
      argi++;
      if (argi >= argc) {
        fprintf(stderr, "--sobasedir expects an argument.");
        return usage(argv[0]);
      }

      custom_so_basedir = argv[argi];
    } else {
      return usage(argv[0]);
    }
    argi++;
  }

  if (argc != argi + 2)
    exit(usage(argv[0]));

  MemoryMappedFile mapped_file(argv[argi + 0], 0);
  if (!mapped_file.data()) {
    fprintf(stderr, "md2core: error: failed to mmap dump file '%s', "
            "error=%s\n", argv[argi + 0], strerror(errno));
    return 1;
  }

  MinidumpMemoryRange dump(mapped_file.data(), mapped_file.size());

  CoreDumpWriter writer;
  writer.set_verbose(verbose);
  if (!custom_so_basedir.empty() || use_so_basedir) {
    writer.set_sobase_path(custom_so_basedir, use_so_basedir);
  }

  bool success = writer.parse(dump);
  if (!success)
    exit(EXIT_FAILURE);

  int fd = open(argv[argi + 1], O_CREAT|O_TRUNC|O_WRONLY, 0600);
  if (fd < 0) {
    fprintf(stderr, "md2core: error: failed to open core file '%s', "
            "error=%s\n", argv[argi + 1], strerror(errno));
    exit(EXIT_FAILURE);
  }

  success = writer.write(fd);

  close(fd);

  if (!success) {
    fprintf(stderr, "md2core: error: cannot write core file\n");
    exit(EXIT_FAILURE);
  }

  exit(EXIT_SUCCESS);
  return 0;
}
