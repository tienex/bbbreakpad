// Copyright (c) 2015, Orlando Bassotto.
// Copyright (c) 2009, Google Inc.
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are
// met:
//
//     * Redistributions of source code must retain the above copyright
// notice, this list of conditions and the following disclaimer.
//     * Redistributions in binary form must reproduce the above
// copyright notice, this list of conditions and the following disclaimer
// in the documentation and/or other materials provided with the
// distribution.
//     * Neither the name of Google Inc. nor the names of its
// contributors may be used to endorse or promote products derived from
// this software without specific prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
// "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
// LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
// A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
// OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
// SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
// LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
// DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
// THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
// (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
// OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

#ifndef BREAKPAD_TOOLS_BSD_MD2CORE_CRASHED_PROCESS_H__
#define BREAKPAD_TOOLS_BSD_MD2CORE_CRASHED_PROCESS_H__

#include "tools/bsd/md2core/base.h"

namespace google_breakpad { namespace md2core {

// We parse the minidump file and keep the parsed information in this structure
struct CrashedProcess {
  CrashedProcess()
      : crashing_lwpid(-1),
        auxv(NULL),
        auxv_length(0) {
    memset(&debug, 0, sizeof(debug));
  }

  struct Mapping {
    Mapping()
      : shared(false),
        permissions(0xFFFFFFFF),
        start_address(0),
        end_address(0),
        offset(0),
        inode(0) {
    }

    bool shared;
    uint32_t permissions;
    uint64_t start_address, end_address, offset;
    uint64_t inode;
    std::string filename;
    std::string data;
  };
  std::map<uint64_t, Mapping> mappings;
  std::map<uint64_t, Mapping> full_mappings;

  lwpid_t crashing_lwpid;
  int fatal_signal;
  int fatal_signal_code;
  int fatal_errno;

  struct Thread {
    lwpid_t lwpid;
    bsd::ThreadContext context;
    uintptr_t stack_addr;
    const uint8_t* stack;
    size_t stack_length;
  };
  std::vector<Thread> threads;

  const uint8_t* auxv;
  size_t auxv_length;

  std::map<uintptr_t, std::string> signatures;

  std::string dynamic_data;
  MDRawDebug debug;
  MDRawMiscInfo miscinfo;
  bool miscinfo_valid;
  std::vector<MDRawLinkMap> link_map;
  std::vector<const char*> argv;
  std::vector<const char*> env;
};

} }

#endif  // !BREAKPAD_TOOLS_BSD_MD2CORE_CRASHED_PROCESS_H__
