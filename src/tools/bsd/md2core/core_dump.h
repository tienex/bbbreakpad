// Copyright (c) 2015, Orlando Bassotto.
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are
// met:
//
//     * Redistributions of source code must retain the above copyright
// notice, this list of conditions and the following disclaimer.
//     * Redistributions in binary form must reproduce the above
// copyright notice, this list of conditions and the following disclaimer
// in the documentation and/or other materials provided with the
// distribution.
//     * Neither the name of Google Inc. nor the names of its
// contributors may be used to endorse or promote products derived from
// this software without specific prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
// "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
// LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
// A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
// OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
// SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
// LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
// DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
// THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
// (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
// OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

#ifndef BREAKPAD_TOOLS_BSD_MD2CORE_CORE_DUMP_H__
#define BREAKPAD_TOOLS_BSD_MD2CORE_CORE_DUMP_H__

#include "tools/bsd/md2core/crashed_process.h"

namespace google_breakpad { namespace md2core {

class CoreDump {
 public:
  CoreDump() {
    set_verbose(false);
    set_sobase_path(std::string(), false);
  }

 public:
  void set_verbose(bool verbose) { verbose_ = verbose; }
  void set_sobase_path(const std::string &dir, bool use);

 public:
  bool parse(const MinidumpMemoryRange &dump);

 protected:
  void ParseThreadRegisters(bsd::ThreadContext &context,
                            const MinidumpMemoryRange &range);
  void ParseThreadList(const MinidumpMemoryRange &range,
                       const MinidumpMemoryRange &full_file);
  bool ParseSystemInfo(const MinidumpMemoryRange &range,
                       const MinidumpMemoryRange &full_file);
  void ParseMiscInfo(const MinidumpMemoryRange &range);
  void ParseCPUInfo(const MinidumpMemoryRange &range);
  void ParseMaps(const MinidumpMemoryRange &range);
  virtual void ParseEnvironment(const MinidumpMemoryRange &range);
  virtual void ParseCmdLine(const MinidumpMemoryRange &range);
  void ParseAuxVector(const MinidumpMemoryRange &range);
  void ParseDSODebugInfo(const MinidumpMemoryRange &range,
                         const MinidumpMemoryRange &full_file);
  void ParseExceptionStream(const MinidumpMemoryRange &range);
  void ParseModuleStream(const MinidumpMemoryRange &range,
                         const MinidumpMemoryRange &full_file);

 private:
  void AddDataToMapping(const std::string& data, uintptr_t addr);
  void AugmentMappings(const MinidumpMemoryRange &full_file);

 private:
  struct FakeDSO {
    bool main;
    uintptr_t base;
    std::string name;
    std::string refname;
  };

  struct FakeDebugInfo {
    uintptr_t loader_base;
    std::vector <FakeDSO> modules;
  };

  bool BuildFakeDebugInfo(FakeDebugInfo &info) const;
  bool ReadDynamicPhdrs(const FakeDebugInfo &info, std::vector<Phdr> &phdrs,
                        std::string &contents) const;
  void BuildDSOLinkMap(const MinidumpMemoryRange &full_file);
  void BuildFakeDSOLinkMap(const MinidumpMemoryRange &full_file);

 protected:
  std::string GetExecutableFullPath() const;
  std::string GetExecutableBaseName() const;
  std::string GetRewrittenExecutableFullPath() const;

 protected:
  CrashedProcess crashinfo_;
  bool verbose_;
  std::string sobasedir_;
  bool default_sobasedir_;
  bool use_sobasedir_;
};

} }

#endif  // !BREAKPAD_TOOLS_BSD_MD2CORE_CORE_DUMP_H__
