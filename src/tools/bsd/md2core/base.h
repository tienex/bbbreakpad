// Copyright (c) 2015, Orlando Bassotto.
// Copyright (c) 2009, Google Inc.
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are
// met:
//
//     * Redistributions of source code must retain the above copyright
// notice, this list of conditions and the following disclaimer.
//     * Redistributions in binary form must reproduce the above
// copyright notice, this list of conditions and the following disclaimer
// in the documentation and/or other materials provided with the
// distribution.
//     * Neither the name of Google Inc. nor the names of its
// contributors may be used to endorse or promote products derived from
// this software without specific prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
// "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
// LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
// A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
// OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
// SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
// LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
// DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
// THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
// (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
// OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

#ifndef BREAKPAD_TOOLS_BSD_MD2CORE_BASE_H__
#define BREAKPAD_TOOLS_BSD_MD2CORE_BASE_H__

#include <errno.h>
#include <inttypes.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

#include <map>
#include <string>
#include <vector>

#include "common/bsd/bsd_utils.h"
#include "common/bsd/memory_mapped_file.h"
#include "common/minidump_type_helper.h"
#include "common/scoped_ptr.h"
#include "google_breakpad/common/minidump_format.h"
#include "tools/bsd/md2core/minidump_memory_range.h"

#if defined(__x86_64__)
  #define ELF_ARCH EM_X86_64
#elif defined(__i386__)
  #define ELF_ARCH EM_386
#elif defined(__vax__)
  #define ELF_ARCH EM_VAX
#else
#error "This code has not been ported to your platform yet"
#endif

#undef ELF_DATA
#if defined(__BIG_ENDIAN__)
  #define ELF_DATA ELFDATA2MSB
#else
  #define ELF_DATA ELFDATA2LSB
#endif

#ifndef DEFAULT_SO_BASE_PATH
#define DEFAULT_SO_BASE_PATH "/var/lib/breakpad/"
#endif

namespace google_breakpad { namespace md2core {

typedef ElfW(Ehdr) Ehdr;
typedef ElfW(Phdr) Phdr;
typedef ElfW(Shdr) Shdr;
typedef ElfW(Nhdr) Nhdr;
typedef ElfW(Auxinfo) Auxinfo;

typedef MDTypeHelper<sizeof(ElfW(Addr))>::MDRawDebug MDRawDebug;
typedef MDTypeHelper<sizeof(ElfW(Addr))>::MDRawLinkMap MDRawLinkMap;

static const MDRVA kInvalidMDRVA = static_cast<MDRVA>(-1);

} }

#endif  // !BREAKPAD_TOOLS_BSD_MD2CORE_BASE_H__
