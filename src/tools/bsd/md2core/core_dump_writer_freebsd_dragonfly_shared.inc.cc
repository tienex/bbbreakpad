// Copyright (c) 2015, Orlando Bassotto.
// Copyright (c) 2009, Google Inc.
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are
// met:
//
//     * Redistributions of source code must retain the above copyright
// notice, this list of conditions and the following disclaimer.
//     * Redistributions in binary form must reproduce the above
// copyright notice, this list of conditions and the following disclaimer
// in the documentation and/or other materials provided with the
// distribution.
//     * Neither the name of Google Inc. nor the names of its
// contributors may be used to endorse or promote products derived from
// this software without specific prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
// "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
// LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
// A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
// OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
// SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
// LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
// DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
// THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
// (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
// OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

using google_breakpad::bsd::ThreadContext;

namespace google_breakpad { namespace md2core {

#if defined(__DragonFly__)
#define NOTE_OWNER "CORE"
#else
#define NOTE_OWNER "FreeBSD"
#endif

bool CoreDumpWriter::write_note_first() const {
  return true;
}

bool CoreDumpWriter::prepare_notes() {
  // DragonFly / FreeBSD
  Note note_prpsinfo(NOTE_OWNER, NT_PRPSINFO, sizeof(prpsinfo_t));
  prpsinfo_t *prpsinfo = note_prpsinfo.get <prpsinfo_t> ();

  prpsinfo->pr_version = PRPSINFO_VERSION;
  prpsinfo->pr_psinfosz = sizeof(prpsinfo_t);

  std::string fname(GetExecutableBaseName());
  std::string psargs;

  for (size_t n = 1;
       psargs.length() < PRARGSZ && n < crashinfo_.argv.size(); n++) {
    if (!psargs.empty())
      psargs += ' ';

    psargs += crashinfo_.argv[n];
  }

  strlcpy(prpsinfo->pr_fname, fname.c_str(), sizeof(prpsinfo->pr_fname));
  strlcpy(prpsinfo->pr_psargs, psargs.c_str(), sizeof(prpsinfo->pr_psargs));

  notes_.push_back(note_prpsinfo);

  // Write first crashing thread
  for (size_t n = 0; n < crashinfo_.threads.size(); ++n) {
    if (crashinfo_.threads[n].lwpid == crashinfo_.crashing_lwpid) {
      if (!prepare_thread_note(crashinfo_.threads[n], true))
        return false;
      break;
    }
  }

  // Write all other threads
  for (size_t n = 0; n < crashinfo_.threads.size(); ++n) {
    // Skip crashing thread
    if (crashinfo_.threads[n].lwpid == crashinfo_.crashing_lwpid)
      continue;

    if (!prepare_thread_note(crashinfo_.threads[n], false))
      return false;
  }

#if defined(__FreeBSD__)
  // NT_PROCSTAT_PROC
  Note note_proc(NOTE_OWNER, NT_PROCSTAT_PROC,
                 sizeof(uint32_t) + sizeof(struct kinfo_proc));
  uint32_t *kp_size = note_proc.get <uint32_t> ();
  struct kinfo_proc *kp = reinterpret_cast<kinfo_proc*>(kp_size + 1);
  *kp_size = sizeof(struct kinfo_proc);
  kp->ki_ppid = 1;
  kp->ki_stat = SRUN;
  kp->ki_numthreads = crashinfo_.threads.size();
  strlcpy(kp->ki_comm, fname.c_str(), sizeof(kp->ki_comm));
  if (crashinfo_.miscinfo_valid) {
    kp->ki_pid = crashinfo_.miscinfo.process_id;
    kp->ki_start.tv_sec = crashinfo_.miscinfo.process_create_time;
    kp->ki_rusage.ru_utime.tv_sec = crashinfo_.miscinfo.process_user_time;
    kp->ki_rusage.ru_stime.tv_sec = crashinfo_.miscinfo.process_kernel_time;
  } else {
    kp->ki_pid = 1234;
  }
  notes_.push_back(note_proc);

  // NT_PROCSTAT_VMMAP
  Note note_vmmap(NOTE_OWNER, NT_PROCSTAT_VMMAP, sizeof(uint32_t));
  *note_vmmap.get <uint32_t> () = sizeof(struct kinfo_vmentry);
  for (std::map<uint64_t, CrashedProcess::Mapping>::const_iterator
           iter = crashinfo_.full_mappings.begin();
           iter != crashinfo_.full_mappings.end();
           ++iter) {
    const CrashedProcess::Mapping &mapping = iter->second;

    struct kinfo_vmentry kve;
    memset(&kve, 0, sizeof(kve));
    kve.kve_structsize = sizeof(struct kinfo_vmentry) - sizeof(kve.kve_path);
    if (mapping.filename.empty()) {
      kve.kve_type = KVME_TYPE_DEFAULT;
    } else {
      kve.kve_type = KVME_TYPE_VNODE;
      kve.kve_vn_fileid = mapping.inode;
      strlcpy(kve.kve_path, mapping.filename.c_str(),
              sizeof(kve.kve_path));
      kve.kve_structsize += mapping.filename.length() + 1;
    }
    kve.kve_start  = mapping.start_address;
    kve.kve_end    = mapping.end_address;
    kve.kve_offset = mapping.offset;
    if (!mapping.shared) {
      kve.kve_flags = KVME_FLAG_COW;
    }
    if (mapping.permissions & PF_R) {
      kve.kve_protection |= KVME_PROT_READ;
    }
    if (mapping.permissions & PF_W) {
      kve.kve_protection |= KVME_PROT_WRITE;
    }
    if (mapping.permissions & PF_X) {
      kve.kve_protection |= KVME_PROT_EXEC;
    }
    // Align to natural word size
    kve.kve_structsize = (kve.kve_structsize + sizeof(uintptr_t) - 1) &
                         -sizeof(uintptr_t);
    if (kve.kve_structsize > sizeof(kve)) {
      kve.kve_structsize = sizeof(kve);
    }

    note_vmmap.contents_.insert(
        note_vmmap.contents_.end(),
        reinterpret_cast <uint8_t*> (&kve),
        reinterpret_cast <uint8_t*> (&kve) + kve.kve_structsize);
  }
  notes_.push_back(note_vmmap);

  // NT_PROCSTAT_PSSTRINGS
  // In order to build the PSSTRINGS we need to locate the environment in
  // the memory.
  
  // Build the environment string.
  std::string env;
  for (size_t n = 0; n < crashinfo_.env.size(); n++) {
    const char *str = crashinfo_.env[n];
    env.insert(env.end(), str, str + strlen(str) + 1);
  }

  // Locate the environment.
  for (std::map<uint64_t, CrashedProcess::Mapping>::const_iterator iter =
         crashinfo_.mappings.begin();
       iter != crashinfo_.mappings.end(); ++iter) {
    const CrashedProcess::Mapping &mapping = iter->second;
    if (mapping.data.empty())
      continue;

    if (memmem(mapping.data.c_str(), mapping.data.length(), env.c_str(),
                env.length()) != NULL) {
      // We found the segment containing the ps_strings; it is located
      // at the very end.
      Note note_psstrings(NOTE_OWNER, NT_PROCSTAT_PSSTRINGS,
                          sizeof(uint32_t) + sizeof(uintptr_t));
      uint32_t *pss_size = note_psstrings.get <uint32_t> ();
      uintptr_t *pss_offset = reinterpret_cast<uintptr_t *>(pss_size + 1);
      *pss_size = sizeof(uintptr_t);
      *pss_offset = mapping.end_address - sizeof(struct ps_strings);
      notes_.push_back(note_psstrings);
      break;
    }
  }

  if (crashinfo_.auxv_length != 0) {
    // NT_PROCSTAT_AUXV
    Note note_auxv(NOTE_OWNER, NT_PROCSTAT_AUXV,
                   sizeof(uint32_t) + crashinfo_.auxv_length);
    uint32_t *count = note_auxv.get <uint32_t> ();
    *count = crashinfo_.auxv_length / sizeof(ElfW(Auxinfo));
    memcpy(count + 1, crashinfo_.auxv, crashinfo_.auxv_length);
    notes_.push_back(note_auxv);
  }
#endif

  return true;
}

bool CoreDumpWriter::prepare_thread_note(const CrashedProcess::Thread& thread,
                                         bool crashed) {
  // DragonFly / FreeBSD
  Note note_prstatus(NOTE_OWNER, NT_PRSTATUS, sizeof(prstatus_t));
  Note note_fpregset(NOTE_OWNER, NT_FPREGSET, sizeof(fpregset_t));

  prstatus_t *prstatus = note_prstatus.get <prstatus_t> ();
  fpregset_t *fpregset = note_fpregset.get <fpregset_t> ();

  prstatus->pr_version = PRSTATUS_VERSION;
  prstatus->pr_statussz = sizeof(prstatus_t);
  prstatus->pr_gregsetsz = sizeof(gregset_t);
  prstatus->pr_fpregsetsz = sizeof(fpregset_t);
  prstatus->pr_cursig = crashed ? crashinfo_.fatal_signal : 0;
  prstatus->pr_pid = thread.lwpid;
  memcpy(&prstatus->pr_reg, &thread.context.gregs, sizeof(prstatus->pr_reg));

#if defined(__i386__)
  {
    struct save87 s87;
    thread.context.getx87(s87);
    memcpy(fpregset, &s87, sizeof(fpregset_t));
  }
#else
  memcpy(fpregset, &thread.context.fregs, sizeof(fpregset_t));
#endif
  notes_.push_back(note_prstatus);
  notes_.push_back(note_fpregset);

#if defined(__FreeBSD__)
  // Do nothing with thread misc.
  Note note_thrmisc(NOTE_OWNER, NT_THRMISC, sizeof(thrmisc_t));
  notes_.push_back(note_thrmisc);

#if defined(__i386__) || defined(__x86_64__)

#if defined(__i386__)
#define savefpu_ymm savexmm_ymm
#endif

  // Build savexmm_ymm
  Note note_xstate(NOTE_OWNER, NT_X86_XSTATE, sizeof(struct savefpu_ymm));
  struct savefpu_ymm *xstate = note_xstate.get <struct savefpu_ymm> ();
  // Enable YMM
  *reinterpret_cast<uint32_t*>(reinterpret_cast<uint8_t*>(xstate)+464) = 7;
  xstate->sv_xstate.sx_hd.xstate_bv = 7;
  xstate->sv_env.en_cw =
      thread.context.get_freg_ctl(ThreadContext::FREG_CTL_CW);
  xstate->sv_env.en_sw =
      thread.context.get_freg_ctl(ThreadContext::FREG_CTL_SW);
  xstate->sv_env.en_tw =
      thread.context.get_freg_ctl(ThreadContext::FREG_CTL_TW);
  xstate->sv_env.en_opcode =
      thread.context.get_freg_ctl(ThreadContext::FREG_CTL_FOP);
#if defined(__i386__)
  xstate->sv_env.en_fip =
      thread.context.get_freg_ctl(ThreadContext::FREG_CTL_FIP);
  xstate->sv_env.en_fcs =
      thread.context.get_freg_ctl(ThreadContext::FREG_CTL_FCS);
  xstate->sv_env.en_foo =
      thread.context.get_freg_ctl(ThreadContext::FREG_CTL_FOO);
  xstate->sv_env.en_fos =
      thread.context.get_freg_ctl(ThreadContext::FREG_CTL_FOS);
#elif defined(__x86_64__)
  xstate->sv_env.en_rip =
      thread.context.get_freg_ctl(ThreadContext::FREG_CTL_RIP);
  xstate->sv_env.en_rdp =
      thread.context.get_freg_ctl(ThreadContext::FREG_CTL_RDP);
  xstate->sv_env.en_mxcsr_mask =
      thread.context.get_freg_ctl(ThreadContext::FREG_CTL_MXCSR_MASK);
#else
#error "Architecture not supported."
#endif
  xstate->sv_env.en_mxcsr =
      thread.context.get_freg_ctl(ThreadContext::FREG_CTL_MXCSR);
  for (size_t n = 0; n < 8; n++) {
    const void *st =
           thread.context.get_freg(static_cast<ThreadContext::freg_name_t>
                                   (ThreadContext::FREG_ST0 + n));
    if (st != NULL) {
      memcpy(&xstate->sv_fp[n].fp_acc, st,
             sizeof(xstate->sv_fp[n].fp_acc));
    }
  }
  for (size_t n = 0; n < 16; n++) {
    const void *xmm =
           thread.context.get_freg(static_cast<ThreadContext::freg_name_t>
                                   (ThreadContext::FREG_XMM0 + n));
    if (xmm != NULL) {
      memcpy(&xstate->sv_xmm[n], xmm, sizeof(xstate->sv_xmm[n]));
    }
  }
  notes_.push_back(note_xstate);
#elif defined(__powerpc__) || defined(__powerpc64__)
#warning "TODO: Should save VMX here"
#else
  // To be done.
#endif
#endif
  return true;
}

} }
