// Copyright (c) 2015, Orlando Bassotto.
// Copyright (c) 2009, Google Inc.
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are
// met:
//
//     * Redistributions of source code must retain the above copyright
// notice, this list of conditions and the following disclaimer.
//     * Redistributions in binary form must reproduce the above
// copyright notice, this list of conditions and the following disclaimer
// in the documentation and/or other materials provided with the
// distribution.
//     * Neither the name of Google Inc. nor the names of its
// contributors may be used to endorse or promote products derived from
// this software without specific prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
// "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
// LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
// A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
// OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
// SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
// LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
// DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
// THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
// (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
// OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

#ifndef BREAKPAD_TOOLS_BSD_MD2CORE_CORE_DUMP_WRITER_H__
#define BREAKPAD_TOOLS_BSD_MD2CORE_CORE_DUMP_WRITER_H__

#include "tools/bsd/md2core/core_dump.h"

namespace google_breakpad { namespace md2core {

class CoreDumpWriter : public CoreDump {
 protected:
  struct Note {
    Nhdr nhdr_;
    std::string name_;
    std::vector<uint8_t> contents_;

    Note(const char *name = NULL, uint32_t type = 0,
         size_t initial_size = 0) {
      if (name != NULL) {
        name_ = name;
      }
      nhdr_.n_type   = type;
      nhdr_.n_namesz = name_.length() + 1;
      nhdr_.n_descsz = initial_size;
      contents_.resize(initial_size);
    }

    template <typename T> T *get()
    { return reinterpret_cast <T*> (&contents_[0]); }
  };

 public:
  CoreDumpWriter();

 protected:
   bool prepare_elf_header();
   bool prepare_program_headers();
   bool prepare_notes();
   bool prepare_thread_note(const CrashedProcess::Thread& thread, bool crashed);

 private:
   bool write_note_first() const;
   bool prepare_note_program_header();

 public:
   bool write(int fd);

 private:
   bool write_headers(int fd);
   bool write_notes(int fd, size_t index);
   bool write_contents(int fd);

 private:
  Ehdr ehdr_;
  std::vector<Phdr> phdrs_;
  std::vector<Note> notes_;
};

} }

#endif  // !BREAKPAD_TOOLS_BSD_MD2CORE_CORE_DUMP_WRITER_H__
