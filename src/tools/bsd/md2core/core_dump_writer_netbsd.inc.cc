// Copyright (c) 2015, Orlando Bassotto.
// Copyright (c) 2009, Google Inc.
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are
// met:
//
//     * Redistributions of source code must retain the above copyright
// notice, this list of conditions and the following disclaimer.
//     * Redistributions in binary form must reproduce the above
// copyright notice, this list of conditions and the following disclaimer
// in the documentation and/or other materials provided with the
// distribution.
//     * Neither the name of Google Inc. nor the names of its
// contributors may be used to endorse or promote products derived from
// this software without specific prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
// "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
// LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
// A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
// OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
// SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
// LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
// DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
// THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
// (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
// OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

using google_breakpad::bsd::ThreadContext;

namespace google_breakpad { namespace md2core {

bool CoreDumpWriter::write_note_first() const {
  return false;
}

bool CoreDumpWriter::prepare_notes() {
  // NetBSD
  std::string fname(GetExecutableBaseName());

  Note note_procinfo(ELF_NOTE_NETBSD_CORE_NAME,
                     ELF_NOTE_NETBSD_CORE_PROCINFO,
                     sizeof(struct netbsd_elfcore_procinfo));
  struct netbsd_elfcore_procinfo *procinfo =
      note_procinfo.get <struct netbsd_elfcore_procinfo> ();

  procinfo->cpi_version = NETBSD_ELFCORE_PROCINFO_VERSION;
  procinfo->cpi_cpisize = sizeof(struct netbsd_elfcore_procinfo);
  procinfo->cpi_ppid = 1;
  if (crashinfo_.miscinfo_valid) {
    procinfo->cpi_pid = crashinfo_.miscinfo.process_id;
  } else {
    procinfo->cpi_pid = 1234;
  }
  procinfo->cpi_nlwps = crashinfo_.threads.size();
  procinfo->cpi_signo = crashinfo_.fatal_signal;
  procinfo->cpi_sigcode = crashinfo_.fatal_signal_code;
  strlcpy(reinterpret_cast<char *>(procinfo->cpi_name), fname.c_str(),
          sizeof(procinfo->cpi_name));
  notes_.push_back(note_procinfo);

  // Write first crashing thread
  for (size_t n = 0; n < crashinfo_.threads.size(); ++n) {
    if (crashinfo_.threads[n].lwpid == crashinfo_.crashing_lwpid) {
      if (!prepare_thread_note(crashinfo_.threads[n], true))
        return false;
      break;
    }
  }

  // Write all other threads
  for (size_t n = 0; n < crashinfo_.threads.size(); ++n) {
    // Skip crashing thread
    if (crashinfo_.threads[n].lwpid == crashinfo_.crashing_lwpid)
      continue;

    if (!prepare_thread_note(crashinfo_.threads[n], false))
      return false;
  }

  return true;
}

bool CoreDumpWriter::prepare_thread_note(const CrashedProcess::Thread& thread,
                                         bool crashed) {
  // NetBSD
  char name[64];
  snprintf(name, sizeof(name), "%s@%lu", ELF_NOTE_NETBSD_CORE_NAME,
           thread.lwpid);

  Note note_regs(name, PT_GETREGS, sizeof(struct reg));
  memcpy(note_regs.get <struct reg> (), &thread.context.gregs,
         sizeof(struct reg));
  notes_.push_back(note_regs);

#if defined(BSD_HAVE_MACHINE_FPU_H) || defined(BSD_HAVE_MACHINE_NPX_H)
#if defined(__i386__)
  {
    struct save87 s87;
    thread.context.getx87(s87);
    Note note_fpregs(name, PT_GETFPREGS, sizeof(struct fpreg));
    memcpy(note_fpregs.get <void> (), &s87, sizeof(struct fpreg));
    notes_.push_back(note_fpregs);
  }

  if (!thread.context.is_x87) {
    Note note_xfpregs(name, PT_GETXMMREGS, sizeof(struct savexmm));
    memcpy(note_xfpregs.get <void> (), &thread.context.fregs,
           sizeof(struct savexmm));
    notes_.push_back(note_xfpregs);
  }
#else
  Note note_fpregs(name, PT_GETFPREGS, sizeof(struct fpreg));
  memcpy(note_fpregs.get <struct fpreg> (), &thread.context.fregs,
         sizeof(struct fpreg));
  notes_.push_back(note_fpregs);
#endif
#endif

  return true;
}

} }
