// Copyright (c) 2015, Orlando Bassotto.
// Copyright (c) 2009, Google Inc.
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are
// met:
//
//     * Redistributions of source code must retain the above copyright
// notice, this list of conditions and the following disclaimer.
//     * Redistributions in binary form must reproduce the above
// copyright notice, this list of conditions and the following disclaimer
// in the documentation and/or other materials provided with the
// distribution.
//     * Neither the name of Google Inc. nor the names of its
// contributors may be used to endorse or promote products derived from
// this software without specific prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
// "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
// LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
// A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
// OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
// SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
// LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
// DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
// THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
// (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
// OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

#include "tools/bsd/md2core/core_dump_writer.h"

using google_breakpad::md2core::CoreDumpWriter;

namespace {

// Write all of the given buffer, handling short writes and EINTR. Return true
// iff successful.
static bool
writea(int fd, const void* idata, size_t length) {
  const uint8_t* data = (const uint8_t*) idata;

  size_t done = 0;
  while (done < length) {
    ssize_t r;
    do {
      r = write(fd, data + done, length - done);
    } while (r == -1 && errno == EINTR);

    if (r < 1)
      return false;
    done += r;
  }

  return true;
}

static bool
pwritea(int fd, const void* idata, size_t length, off_t offset) {
  const uint8_t* data = (const uint8_t*) idata;

  size_t done = 0;
  while (done < length) {
    ssize_t r;
    do {
      r = pwrite(fd, data + done, length - done, offset);
    } while (r == -1 && errno == EINTR);

    if (r < 1)
      return false;
    done += r;
  }

  return true;
}

}

CoreDumpWriter::CoreDumpWriter()
    : CoreDump() {
  memset(&ehdr_, 0, sizeof(Ehdr));
}

bool CoreDumpWriter::write(int fd) {
  if (fd < 0)
    return false;

  memset(&ehdr_, 0, sizeof(Ehdr));
  phdrs_.clear();
  notes_.clear();

  size_t note_index = 0;

  if (!prepare_notes())
    return false;
  
  if (write_note_first()) {
    note_index = 0;
    if (!prepare_note_program_header())
      return false;
  }
  
  if (!prepare_program_headers())
    return false;

  if (!write_note_first()) {
    note_index = phdrs_.size();
    if (!prepare_note_program_header())
      return false;
  }

  if (!prepare_elf_header())
    return false;

  if (!write_headers(fd))
    return false;

  if (!write_notes(fd, note_index))
    return false;
  if (!write_contents(fd))
    return false;

  return true;
}

bool CoreDumpWriter::write_headers(int fd) {
  // Write ELF header
  if (!writea(fd, &ehdr_, sizeof(Ehdr)))
    return false;
  // Write program headers
  if (!writea(fd, &phdrs_[0], sizeof(Phdr) * phdrs_.size()))
    return false;

  return true;
}

bool CoreDumpWriter::write_notes(int fd, size_t index) {
  if (notes_.empty())
    return true;

  // Use a temporary storage.
  std::vector <uint8_t> bytes;

  for (size_t n = 0; n < notes_.size(); n++) {
    const Note &note = notes_[n];
    bytes.insert(bytes.end(),
                 reinterpret_cast <const uint8_t*> (&note.nhdr_),
                 reinterpret_cast <const uint8_t*> (&note.nhdr_) +
                 sizeof(note.nhdr_));
    size_t namesz = note.nhdr_.n_namesz;
    bytes.insert(bytes.end(),
                 reinterpret_cast <const uint8_t*> (&note.name_[0]),
                 reinterpret_cast <const uint8_t*> (&note.name_[namesz]));
    while (namesz & 3) {
      bytes.push_back(0);
      namesz++;
    }
    bytes.insert(bytes.end(), note.contents_.begin(), note.contents_.end());
    // Align to 4 bytes
    while (bytes.size() & 3) {
      bytes.push_back(0);
    }
  }

  if (!pwritea(fd, &bytes[0], bytes.size(), phdrs_[index].p_offset))
    return false;

  return true;
}

bool CoreDumpWriter::write_contents(int fd) {
  size_t nphdr = !write_note_first() || notes_.empty() ? 0 : 1;

  for (std::map<uint64_t, CrashedProcess::Mapping>::const_iterator iter =
         crashinfo_.mappings.begin();
       iter != crashinfo_.mappings.end(); ++iter) {
    const CrashedProcess::Mapping& mapping = iter->second;
    if (mapping.data.size()) {
      if (!pwritea(fd, mapping.data.c_str(), mapping.data.size(),
                   phdrs_[nphdr].p_offset))
        return false;
    }
    nphdr++;
  }
  return true;
}

bool CoreDumpWriter::prepare_elf_header() {
  size_t offset = sizeof(Ehdr) + phdrs_.size() * sizeof(Phdr);

  // Recompute the offsets of the program headers.
  for (size_t n = 0; n < phdrs_.size(); n++) {
    phdrs_[n].p_offset = offset;
    offset += phdrs_[n].p_filesz;
  }

  // Prepare the ELF header. The file will look like:
  //   ELF header
  //   Phdr for each of the thread stacks
  //   Phdr for the PT_NOTE
  //   <Phdr contents>
  //   PT_NOTE
  //   each of the thread stacks
  ehdr_.e_ident[EI_MAG0]    = ELFMAG0;
  ehdr_.e_ident[EI_MAG1]    = ELFMAG1;
  ehdr_.e_ident[EI_MAG2]    = ELFMAG2;
  ehdr_.e_ident[EI_MAG3]    = ELFMAG3;
  ehdr_.e_ident[EI_CLASS]   = ELF_CLASS;
  ehdr_.e_ident[EI_DATA]    = ELF_DATA;
#if defined(__FreeBSD__)
  ehdr_.e_ident[EI_OSABI]   = ELFOSABI_DEFAULT;
#else
  ehdr_.e_ident[EI_OSABI]   = ELFOSABI_SYSV;
#endif
  ehdr_.e_ident[EI_VERSION] = EV_CURRENT;
  ehdr_.e_type              = ET_CORE;
  ehdr_.e_machine           = ELF_ARCH;
  ehdr_.e_version           = EV_CURRENT;
  ehdr_.e_phoff             = sizeof(Ehdr);
  ehdr_.e_ehsize            = sizeof(Ehdr);
  ehdr_.e_phentsize         = sizeof(Phdr);
  ehdr_.e_phnum             = phdrs_.size();
  ehdr_.e_shentsize         = sizeof(Shdr);

  return true;
}

bool CoreDumpWriter::prepare_program_headers() {
  size_t filesz = 0;

  Phdr phdr;
  memset(&phdr, 0, sizeof(phdr));

  phdr.p_type = PT_LOAD;
  phdr.p_align = getpagesize();

  for (std::map<uint64_t, CrashedProcess::Mapping>::const_iterator iter =
         crashinfo_.mappings.begin();
       iter != crashinfo_.mappings.end(); ++iter) {
    const CrashedProcess::Mapping& mapping = iter->second;
    if (mapping.permissions == 0xFFFFFFFF) {
      // This is a map that we found in MD_MODULE_LIST_STREAM (as opposed to
      // MD_LINUX_MAPS). It lacks some of the information that we would like
      // to include.
      phdr.p_flags = PF_R;
    } else {
      phdr.p_flags = mapping.permissions;
    }
    phdr.p_vaddr = mapping.start_address;
    phdr.p_memsz = mapping.end_address - mapping.start_address;
    phdr.p_filesz = mapping.data.size();
    phdrs_.push_back(phdr);
  }
  return true;
}

bool CoreDumpWriter::prepare_note_program_header() {
  if (notes_.empty())
    return true;

  size_t notesize = 0;

  // Fix all notes size, and compute grand total size.
  for (size_t n = 0; n < notes_.size(); n++) {
    Note &note = notes_[n];
    if (note.nhdr_.n_descsz != note.contents_.size()) {
      note.nhdr_.n_descsz = note.contents_.size();
    }
    size_t namesz = (note.nhdr_.n_namesz + 3) & -4;
    notesize += sizeof(Nhdr) + note.nhdr_.n_descsz + namesz;
    // Align to 4 bytes
    notesize = (notesize + 3) & -4;
  }

  Phdr phdr;
  memset(&phdr, 0, sizeof(phdr));

  phdr.p_type = PT_NOTE;
  phdr.p_filesz = notesize;

  phdrs_.push_back(phdr);

  return true;
}

#if defined(__DragonFly__) || defined(__FreeBSD__)
#include "tools/bsd/md2core/core_dump_writer_freebsd_dragonfly_shared.inc.cc"
#elif defined(__NetBSD__)
#include "tools/bsd/md2core/core_dump_writer_netbsd.inc.cc"
#elif defined(__OpenBSD__)
#include "tools/bsd/md2core/core_dump_writer_openbsd.inc.cc"
#else
#error "I don't know this BSD!"
#endif
