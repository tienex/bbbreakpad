/* Copyright (c) 2015, Orlando Bassotto.
 * Copyright (c) 2006, Google Inc.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 *
 *     * Redistributions of source code must retain the above copyright
 * notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above
 * copyright notice, this list of conditions and the following disclaimer
 * in the documentation and/or other materials provided with the
 * distribution.
 *     * Neither the name of Google Inc. nor the names of its
 * contributors may be used to endorse or promote products derived from
 * this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 * A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE. */

/* minidump_exception_bsd.h: A definition of exception codes for
 * BSD
 *
 * (This is C99 source, please don't corrupt it with C++.)
 */
 

#ifndef GOOGLE_BREAKPAD_COMMON_MINIDUMP_EXCEPTION_BSD_H__
#define GOOGLE_BREAKPAD_COMMON_MINIDUMP_EXCEPTION_BSD_H__

#include <stddef.h>

#include "google_breakpad/common/breakpad_types.h"


/* For (MDException).exception_code.  These values come from sys/signal.h.
 */
typedef enum {
  MD_EXCEPTION_CODE_BSD_SIGHUP = 1,      /* Hangup (POSIX) */
  MD_EXCEPTION_CODE_BSD_SIGINT = 2,      /* Interrupt (ANSI) */
  MD_EXCEPTION_CODE_BSD_SIGQUIT = 3,     /* Quit (POSIX) */
  MD_EXCEPTION_CODE_BSD_SIGILL = 4,      /* Illegal instruction (ANSI) */
  MD_EXCEPTION_CODE_BSD_SIGTRAP = 5,     /* Trace trap (POSIX) */
  MD_EXCEPTION_CODE_BSD_SIGABRT = 6,     /* Abort (ANSI) */
  MD_EXCEPTION_CODE_BSD_SIGEMT = 7,      /* Emulate instruction (BSD) */
  MD_EXCEPTION_CODE_BSD_SIGFPE = 8,      /* Floating-point exception (ANSI) */
  MD_EXCEPTION_CODE_BSD_SIGKILL = 9,     /* Kill, unblockable (POSIX) */
  MD_EXCEPTION_CODE_BSD_SIGBUS  = 10,    /* BUS error (BSD) */
  MD_EXCEPTION_CODE_BSD_SIGSEGV = 11,    /* Segmentation violation (ANSI) */
  MD_EXCEPTION_CODE_BSD_SIGSYS  = 12,    /* Bad argument to system call */
  MD_EXCEPTION_CODE_BSD_SIGPIPE = 13,    /* Broken pipe (POSIX) */
  MD_EXCEPTION_CODE_BSD_SIGALRM = 14,    /* Alarm clock (POSIX) */
  MD_EXCEPTION_CODE_BSD_SIGTERM = 15,    /* Termination (ANSI) */
  MD_EXCEPTION_CODE_BSD_SIGURG = 16,
    /* Urgent condition on socket (4.2 BSD) */
  MD_EXCEPTION_CODE_BSD_SIGSTOP = 17,    /* Stop, unblockable (POSIX) */
  MD_EXCEPTION_CODE_BSD_SIGTSTP = 18,    /* Keyboard stop (POSIX) */
  MD_EXCEPTION_CODE_BSD_SIGCONT = 19,    /* Continue (POSIX) */
  MD_EXCEPTION_CODE_BSD_SIGCHLD = 20,    /* Child status has changed (POSIX) */
  MD_EXCEPTION_CODE_BSD_SIGTTIN = 21,    /* Background read from tty (POSIX) */
  MD_EXCEPTION_CODE_BSD_SIGTTOU = 22,    /* Background write to tty (POSIX) */
  MD_EXCEPTION_CODE_BSD_SIGIO = 23,      /* I/O now possible (4.2 BSD) */
  MD_EXCEPTION_CODE_BSD_SIGXCPU = 24,    /* CPU limit exceeded (4.2 BSD) */
  MD_EXCEPTION_CODE_BSD_SIGXFSZ = 25,
    /* File size limit exceeded (4.2 BSD) */
  MD_EXCEPTION_CODE_BSD_SIGVTALRM = 26,  /* Virtual alarm clock (4.2 BSD) */
  MD_EXCEPTION_CODE_BSD_SIGPROF = 27,    /* Profiling alarm clock (4.2 BSD) */
  MD_EXCEPTION_CODE_BSD_SIGWINCH = 28,   /* Window size change (4.3 BSD, Sun) */
  MD_EXCEPTION_CODE_BSD_SIGINFO = 29,    /* Information request (BSD) */
  MD_EXCEPTION_CODE_BSD_SIGUSR1 = 30,    /* User-defined signal 1 (POSIX) */
  MD_EXCEPTION_CODE_BSD_SIGUSR2 = 31,    /* User-defined signal 2 (POSIX) */
  MD_EXCEPTION_CODE_BSD_SIGPWR = 32,
      /* Power failure restart (System V, NetBSD-specific) */
  MD_EXCEPTION_CODE_BSD_SIGTHR = 32,     /* Thread AST (OpenBSD, FreeBSD) */
  MD_EXCEPTION_CODE_BSD_SIGLIBRT = 33,   /* Realtime AST (FreeBSD) */
  MD_EXCEPTION_CODE_BSD_DUMP_REQUESTED = 0xFFFFFFFF /* No exception,
                                                       dump requested. */
} MDExceptionCodeBSD;

/* For (MDException).exception_information[0].
 * These values come from sys/siginfo.h.
 */
typedef enum {
  MD_EXCEPTION_SUBCODE_BSD_UNKNOWN = 0,
  /* SIGILL */
  MD_EXCEPTION_SUBCODE_BSD_ILL_ILLOPC = 1,
  MD_EXCEPTION_SUBCODE_BSD_ILL_ILLOPN = 2,
  MD_EXCEPTION_SUBCODE_BSD_ILL_ILLADR = 3,
  MD_EXCEPTION_SUBCODE_BSD_ILL_ILLTRP = 4,
  MD_EXCEPTION_SUBCODE_BSD_ILL_PRVOPC = 5,
  MD_EXCEPTION_SUBCODE_BSD_ILL_PRVREG = 6,
  MD_EXCEPTION_SUBCODE_BSD_ILL_COPROC = 7,
  MD_EXCEPTION_SUBCODE_BSD_ILL_BADSTK = 8,
  /* SIGFPE */
  MD_EXCEPTION_SUBCODE_BSD_FPE_INTDIV = 1,
  MD_EXCEPTION_SUBCODE_BSD_FPE_INTOVF = 2,
  MD_EXCEPTION_SUBCODE_BSD_FPE_FLTDIV = 3,
  MD_EXCEPTION_SUBCODE_BSD_FPE_FLTOVF = 4,
  MD_EXCEPTION_SUBCODE_BSD_FPE_FLTUND = 5,
  MD_EXCEPTION_SUBCODE_BSD_FPE_FLTRES = 6,
  MD_EXCEPTION_SUBCODE_BSD_FPE_FLTINV = 7,
  MD_EXCEPTION_SUBCODE_BSD_FPE_FLTSUB = 8,
  /* SIGSEGV */
  MD_EXCEPTION_SUBCODE_BSD_SEGV_MAPERR = 1,
  MD_EXCEPTION_SUBCODE_BSD_SEGV_ACCERR = 2,
  /* SIGBUS */
  MD_EXCEPTION_SUBCODE_BSD_BUS_ADRALN = 1,
  MD_EXCEPTION_SUBCODE_BSD_BUS_ADRERR = 2,
  MD_EXCEPTION_SUBCODE_BSD_BUS_OBJERR = 3,
  /* SIGTRAP */
  MD_EXCEPTION_SUBCODE_BSD_TRAP_BRKPT = 1,
  MD_EXCEPTION_SUBCODE_BSD_TRAP_TRACE = 2,
  /* SIGCHLD */
  MD_EXCEPTION_SUBCODE_BSD_CHLD_EXITED = 1,
  MD_EXCEPTION_SUBCODE_BSD_CHLD_KILLED = 2,
  MD_EXCEPTION_SUBCODE_BSD_CHLD_DUMPED = 3,
  MD_EXCEPTION_SUBCODE_BSD_CHLD_TRAPPED = 4,
  MD_EXCEPTION_SUBCODE_BSD_CHLD_STOPPED = 5,
  MD_EXCEPTION_SUBCODE_BSD_CHLD_CONTINUED = 6,
  /* SIGPOLL */
  MD_EXCEPTION_SUBCODE_BSD_POLL_IN = 1,
  MD_EXCEPTION_SUBCODE_BSD_POLL_OUT = 2,
  MD_EXCEPTION_SUBCODE_BSD_POLL_MSG = 3,
  MD_EXCEPTION_SUBCODE_BSD_POLL_ERR = 4,
  MD_EXCEPTION_SUBCODE_BSD_POLL_PRI = 5,
  MD_EXCEPTION_SUBCODE_BSD_POLL_HUP = 6,
} MDExceptionSubcodeBSD;

/* For (MDException).exception_information[1].
 * These values come from errno.h.
 * TODO
 */

#endif  /* GOOGLE_BREAKPAD_COMMON_MINIDUMP_EXCEPTION_BSD_H__ */
