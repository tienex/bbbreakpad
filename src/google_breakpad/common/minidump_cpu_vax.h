/* Copyright (c) 2015, Orlando Bassotto.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 *
 *     * Redistributions of source code must retain the above copyright
 * notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above
 * copyright notice, this list of conditions and the following disclaimer
 * in the documentation and/or other materials provided with the
 * distribution.
 *     * Neither the name of Google Inc. nor the names of its
 * contributors may be used to endorse or promote products derived from
 * this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 * A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE. */

/*
 * VAX support
 */

#ifndef GOOGLE_BREAKPAD_COMMON_MINIDUMP_CPU_VAX_H__
#define GOOGLE_BREAKPAD_COMMON_MINIDUMP_CPU_VAX_H__

#define MD_CONTEXT_VAX_GPR_COUNT 16

typedef struct {
  /* The next field determines the layout of the structure, and which parts
   * of it are populated
   */
  uint32_t      context_flags;

  /* 16 32-bit integer registers, r0 .. r15
   * Note the following fixed uses:
   *   r12 is the argument pointer
   *   r13 is the stack pointer
   *   r14 is the frame register
   *   r15 is the program counter
   */
  uint32_t     iregs[MD_CONTEXT_VAX_GPR_COUNT];
  uint32_t     psl;

} MDRawContextVAX;

/* Indices into iregs for registers with a dedicated or conventional
 * purpose.
 */
enum MDVAXRegisterNumbers {
  MD_CONTEXT_VAX_REG_AP = 12,
  MD_CONTEXT_VAX_REG_SP = 13,
  MD_CONTEXT_VAX_REG_FP = 14,
  MD_CONTEXT_VAX_REG_PC = 15
};

/* For (MDRawContextVAX).context_flags.  These values indicate the type of
 * context stored in the structure. */
#define MD_CONTEXT_VAX                   0x0a000000
#define MD_CONTEXT_VAX_INTEGER           (MD_CONTEXT_VAX | 0x00000001)

#define MD_CONTEXT_VAX_FULL              (MD_CONTEXT_VAX_INTEGER)

#define MD_CONTEXT_VAX_ALL               (MD_CONTEXT_VAX_INTEGER)

#endif  /* GOOGLE_BREAKPAD_COMMON_MINIDUMP_CPU_VAX_H__ */
