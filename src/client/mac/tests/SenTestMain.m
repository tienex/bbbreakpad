#import <Foundation/Foundation.h>
#import <SenTestingKit/SenTestingKit.h>

int main(int argc,const char *argv[])
{
   @autoreleasepool {
       return SenSelfTestMain();
   }
}
