// Copyright (c) 2014, Google Inc.
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are
// met:
//
//     * Redistributions of source code must retain the above copyright
// notice, this list of conditions and the following disclaimer.
//     * Redistributions in binary form must reproduce the above
// copyright notice, this list of conditions and the following disclaimer
// in the documentation and/or other materials provided with the
// distribution.
//     * Neither the name of Google Inc. nor the names of its
// contributors may be used to endorse or promote products derived from
// this software without specific prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
// "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
// LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
// A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
// OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
// SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
// LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
// DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
// THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
// (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
// OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

#ifndef CLIENT_BSD_DUMP_WRITER_COMMON_THREAD_INFO_H_
#define CLIENT_BSD_DUMP_WRITER_COMMON_THREAD_INFO_H_

#include "client/bsd/dump_writer_common/raw_context_cpu.h"
#include "common/bsd/bsd_thread_context.h"
#include "common/memory.h"

namespace google_breakpad {

// We produce one of these structures for each thread in the crashed process.
struct ThreadInfo {
  lwpid_t lwpid; // lightweight process id
  pid_t   pid;   // process id
  pid_t   ppid;  // parent process id

  uintptr_t stack_pointer;  // thread stack pointer

  bsd::ThreadContext context;

  // Returns the instruction pointer (platform-dependent impl.).
  uintptr_t GetInstructionPointer() const;

  // Fills a RawContextCPU using the context in the ThreadInfo object.
  void FillCPUContext(RawContextCPU* out) const;
};

}  // namespace google_breakpad

#endif  // CLIENT_BSD_DUMP_WRITER_COMMON_THREAD_INFO_H_
