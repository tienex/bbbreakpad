// Copyright (c) 2010, Google Inc.
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are
// met:
//
//     * Redistributions of source code must retain the above copyright
// notice, this list of conditions and the following disclaimer.
//     * Redistributions in binary form must reproduce the above
// copyright notice, this list of conditions and the following disclaimer
// in the documentation and/or other materials provided with the
// distribution.
//     * Neither the name of Google Inc. nor the names of its
// contributors may be used to endorse or promote products derived from
// this software without specific prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
// "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
// LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
// A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
// OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
// SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
// LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
// DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
// THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
// (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
// OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

// bsd_dumper.cc: Implement google_breakpad::BSDDumper.
// See bsd_dumper.h for details.

// This code deals with the mechanics of getting information about a crashed
// process. Since this code may run in a compromised address space, the same
// rules apply as detailed at the top of minidump_writer.h: no libc calls and
// use the alternative allocator.

#include "client/bsd/minidump_writer/bsd_dumper.h"

#include <assert.h>
#include <errno.h>
#include <fcntl.h>
#include <limits.h>
#include <stddef.h>
#include <string.h>
#include <sys/stat.h>

#include <iomanip>
#include <sstream>

#include "common/bsd/bsd_utils.h"
#include "common/bsd/elfutils.h"
#include "common/bsd/file_id.h"
#include "common/bsd/bsd_libc_support.h"
#include "common/bsd/memory_mapped_file.h"

static const char kMappedFileUnsafePrefix[] = "/dev/";

inline static bool IsMappedFileOpenUnsafe(
    const google_breakpad::MappingInfo& mapping) {
  // It is unsafe to attempt to open a mapped file that lives under /dev,
  // because the semantics of the open may be driver-specific so we'd risk
  // hanging the crash dumper. And a file in /dev/ almost certainly has no
  // ELF file identifier anyways.
  return strncmp(mapping.name, kMappedFileUnsafePrefix,
                 sizeof(kMappedFileUnsafePrefix) - 1) == 0;
}

namespace google_breakpad {

BSDDumper::BSDDumper(pid_t pid)
    : pid_(pid),
      crash_address_(0),
      crash_signal_(0),
      crash_subcode_(0),
      crash_errno_(0),
      crash_thread_(0),
      threads_(&allocator_, 8),
      mappings_(&allocator_),
      auxv_(&allocator_, AT_COUNT),
      auxv_present_(&allocator_, AT_COUNT) {
  // The passed-in size to the constructor (above) is only a hint.
  // Must call .resize() to do actual initialization of the elements.
  auxv_.resize(AT_COUNT);
  auxv_present_.resize(AT_COUNT);
}

BSDDumper::~BSDDumper() {
}

bool BSDDumper::Init() {
  return ReadAuxv() && EnumerateThreads() && EnumerateMappings();
}

bool BSDDumper::CopyFromProcess(void* dest, lwpid_t lwpid, const void* src,
                                size_t length, bool filltop, uint8_t fillbyte) {
  return CopyFromProcess(dest, lwpid, src, length);
}

bool
BSDDumper::ElfFileIdentifierForMapping(const MappingInfo& mapping,
                                       bool member,
                                       unsigned int mapping_id,
                                       uint8_t identifier[sizeof(MDGUID)]) {
  assert(!member || mapping_id < mappings_.size());
  memset(identifier, 0, sizeof(MDGUID));
  if (IsMappedFileOpenUnsafe(mapping))
    return false;

  char filename[NAME_MAX];
  size_t filename_len = strlen(mapping.name);
  if (filename_len >= NAME_MAX) {
    assert(false);
    return false;
  }
  memcpy(filename, mapping.name, filename_len);
  filename[filename_len] = '\0';
  HandleDeletedFileInMapping(filename);

  MemoryMappedFile mapped_file(filename, mapping.offset);
  if (!mapped_file.data() || mapped_file.size() < SELFMAG)
    return false;

  return
      FileID::ElfFileIdentifierFromMappedFile(mapped_file.data(), identifier);
}

namespace {
bool ElfFileSoNameFromMappedFile(
    const void* elf_base, char* soname, size_t soname_size) {
  if (!IsValidElf(elf_base)) {
    // Not ELF
    return false;
  }

  const void* segment_start;
  size_t segment_size;
  int elf_class;
  if (!FindElfSection(elf_base, ".dynamic", SHT_DYNAMIC,
                      &segment_start, &segment_size, &elf_class)) {
    // No dynamic section
    return false;
  }

  const void* dynstr_start;
  size_t dynstr_size;
  if (!FindElfSection(elf_base, ".dynstr", SHT_STRTAB,
                      &dynstr_start, &dynstr_size, &elf_class)) {
    // No dynstr section
    return false;
  }

  const ElfW(Dyn)* dynamic = static_cast<const ElfW(Dyn)*>(segment_start);
  size_t dcount = segment_size / sizeof(ElfW(Dyn));
  for (const ElfW(Dyn)* dyn = dynamic; dyn < dynamic + dcount; ++dyn) {
    if (dyn->d_tag == DT_SONAME) {
      const char* dynstr = static_cast<const char*>(dynstr_start);
      if (dyn->d_un.d_val >= dynstr_size) {
        // Beyond the end of the dynstr section
        return false;
      }
      const char* str = dynstr + dyn->d_un.d_val;
      const size_t maxsize = dynstr_size - dyn->d_un.d_val;
      strlcpy(soname, str, maxsize < soname_size ? maxsize : soname_size);
      return true;
    }
  }

  // Did not find SONAME
  return false;
}

// Find the shared object name (SONAME) by examining the ELF information
// for |mapping|. If the SONAME is found copy it into the passed buffer
// |soname| and return true. The size of the buffer is |soname_size|.
// The SONAME will be truncated if it is too long to fit in the buffer.
bool ElfFileSoName(
    const MappingInfo& mapping, char* soname, size_t soname_size) {
  if (IsMappedFileOpenUnsafe(mapping)) {
    // Not safe
    return false;
  }

  char filename[NAME_MAX];
  size_t filename_len = strlen(mapping.name);
  if (filename_len >= NAME_MAX) {
    assert(false);
    // name too long
    return false;
  }

  memcpy(filename, mapping.name, filename_len);
  filename[filename_len] = '\0';

  MemoryMappedFile mapped_file(filename, mapping.offset);
  if (!mapped_file.data() || mapped_file.size() < SELFMAG) {
    // mmap failed
    return false;
  }

  return ElfFileSoNameFromMappedFile(mapped_file.data(), soname, soname_size);
}

}  // namespace


// static
void BSDDumper::GetMappingEffectiveNameAndPath(const MappingInfo& mapping,
                                                 char* file_path,
                                                 size_t file_path_size,
                                                 char* file_name,
                                                 size_t file_name_size) {
  strlcpy(file_path, mapping.name, file_path_size);

  // If an executable is mapped from a non-zero offset, this is likely because
  // the executable was loaded directly from inside an archive file (e.g., an
  // apk on Android). We try to find the name of the shared object (SONAME) by
  // looking in the file for ELF sections.
  bool mapped_from_archive = false;
  if (mapping.exec && mapping.offset != 0)
    mapped_from_archive = ElfFileSoName(mapping, file_name, file_name_size);

  if (mapped_from_archive) {
    // Some tools (e.g., stackwalk) extract the basename from the pathname. In
    // this case, we append the file_name to the mapped archive path as follows:
    //   file_name := libname.so
    //   file_path := /path/to/ARCHIVE.APK/libname.so
    if (strlen(file_path) + 1 + strlen(file_name) < file_path_size) {
      strlcat(file_path, "/", file_path_size);
      strlcat(file_path, file_name, file_path_size);
    }
  } else {
    // Common case:
    //   file_path := /path/to/libname.so
    //   file_name := libname.so
    const char* basename = strrchr(file_path, '/');
    basename = basename == NULL ? file_path : (basename + 1);
    strlcpy(file_name, basename, file_name_size);
  }
}

bool BSDDumper::CopyAuxv(void **auxv, size_t *size) {
  if (!ReadAuxv())
    return false;

  // Count the number of aux entries.
  size_t count = 0;
  for (size_t n = 0; n < AT_COUNT; n++) {
    if (auxv_present_[n])
      count++;
  }

  if (count == 0)
    return false;

  ElfW(Auxinfo) *entries = reinterpret_cast<ElfW(Auxinfo)*>
      (calloc(count + 1, sizeof(ElfW(Auxinfo))));
  if (entries == NULL)
    return false;

  for (size_t n = 0, m = 0; n < AT_COUNT; n++) {
    if (auxv_present_[n]) {
      ELF_AUX_TYPE(&entries[m]) = n;
      ELF_AUX_VALUE(&entries[m]) = auxv_[n];
      m++;
    }
  }
  ELF_AUX_TYPE(&entries[count]) = AT_NULL;

  *size = (count + 1) * sizeof(ElfW(Auxinfo));
  *auxv = entries;
  return true;
}

bool BSDDumper::ReadAuxv() {
  size_t count;
  ElfW(Auxinfo) *auxv = bsd::GetProcessElfAuxVector(pid_, &count);
  if (auxv == NULL)
    return false;

  bool res = false;
  for (size_t n = 0; n < count; n++) {
    if (ELF_AUX_TYPE(&auxv[n]) == AT_NULL)
      break;
    if (ELF_AUX_TYPE(&auxv[n]) < AT_COUNT) {
      auxv_[ELF_AUX_TYPE(&auxv[n])] = ELF_AUX_VALUE(&auxv[n]);
      auxv_present_[ELF_AUX_TYPE(&auxv[n])] = true;
      res = true;
    }
  }
  free(auxv);
  return res;
}

bool BSDDumper::ReadMappings(std::vector<bsd::Mapping> &mappings) {
  return bsd::GetProcessMappings(pid_, mappings);
}

bool BSDDumper::EnumerateMappings() {
  std::vector<bsd::Mapping> mappings;
  if (!ReadMappings(mappings))
    return false;

  const void* entry_point_loc = reinterpret_cast<void *>(auxv_[AT_ENTRY]);

  mappings_.clear();
  for (size_t n = 0; n < mappings.size(); n++) {
    // Merge adjacent mappings with the same name into one module,
    // assuming they're a single library mapped by the dynamic linker
    if (mappings[n].name[0] && !mappings_.empty()) {
      MappingInfo* module = mappings_.back();
      if ((mappings[n].start == module->start_addr + module->size) &&
          (strlen(mappings[n].name) == strlen(module->name)) &&
          (strncmp(mappings[n].name, module->name,
                   strlen(mappings[n].name)) == 0)) {
        module->size = mappings[n].end - module->start_addr;
        continue;
      }
    }
    // Also merge mappings that result from address ranges that the
    // linker reserved but which a loaded library did not use. These
    // appear as an anonymous private mapping with no access flags set
    // and which directly follow an executable mapping.
    if (!mappings[n].name[0] && !mappings_.empty()) {
      MappingInfo* module = mappings_.back();
      if ((mappings[n].start == module->start_addr + module->size) &&
          module->exec && module->name[0] &&
          mappings[n].protection == bsd::Mapping::PROT_PRIVATE) {
        module->size = mappings[n].end - module->start_addr;
        continue;
      }
    }

    MappingInfo* const module = new(allocator_) MappingInfo;
    memset(module, 0, sizeof(MappingInfo));
    module->start_addr = mappings[n].start;
    module->size = mappings[n].end - mappings[n].start;
    module->offset = mappings[n].offset;
    module->exec = (mappings[n].protection & PROT_EXEC) != 0;
    if (mappings[n].name[0]) {
      const unsigned l = strlen(mappings[n].name);
      if (l < sizeof(module->name))
        memcpy(module->name, mappings[n].name, l);
    }
    // If this is the entry-point mapping, and it's not already the
    // first one, then we need to make it be first.  This is because
    // the minidump format assumes the first module is the one that
    // corresponds to the main executable (as codified in
    // processor/minidump.cc:MinidumpModuleList::GetMainModule()).
    if (entry_point_loc &&
        (entry_point_loc >=
            reinterpret_cast<void*>(module->start_addr)) &&
        (entry_point_loc <
            reinterpret_cast<void*>(module->start_addr+module->size)) &&
        !mappings_.empty()) {
      // push the module onto the front of the list.
      mappings_.resize(mappings_.size() + 1);
      for (size_t idx = mappings_.size() - 1; idx > 0; idx--)
        mappings_[idx] = mappings_[idx - 1];
      mappings_[0] = module;
    } else {
      mappings_.push_back(module);
    }
  }

  return !mappings_.empty();
}

// Get information about the stack, given the stack pointer. We don't try to
// walk the stack since we might not have all the information needed to do
// unwind. So we just grab, up to, 32k of stack.
bool BSDDumper::GetStackInfo(const void** stack, size_t* stack_len,
                               uintptr_t int_stack_pointer) {
  // Move the stack pointer to the bottom of the page that it's in.
  const uintptr_t page_size = getpagesize();

  uint8_t* const stack_pointer =
      reinterpret_cast<uint8_t*>(int_stack_pointer & ~(page_size - 1));

  // The number of bytes of stack which we try to capture.
  static const ptrdiff_t kStackToCapture = 32 * 1024;

  const MappingInfo* mapping = FindMapping(stack_pointer);
  if (!mapping)
    return false;
  const ptrdiff_t offset = stack_pointer -
      reinterpret_cast<uint8_t*>(mapping->start_addr);
  const ptrdiff_t distance_to_end =
      static_cast<ptrdiff_t>(mapping->size) - offset;
  *stack_len = distance_to_end > kStackToCapture ?
      kStackToCapture : distance_to_end;
  *stack = stack_pointer;
  return true;
}

// Find the mapping which the given memory address falls in.
const MappingInfo* BSDDumper::FindMapping(const void* address) const {
  const uintptr_t addr = (uintptr_t) address;

  for (size_t i = 0; i < mappings_.size(); ++i) {
    const uintptr_t start = static_cast<uintptr_t>(mappings_[i]->start_addr);
    if (addr >= start && addr - start < mappings_[i]->size)
      return mappings_[i];
  }

  return NULL;
}

void BSDDumper::HandleDeletedFileInMapping(char* path) const {
  if (IsPostMortem())
    return;

  char exe_path[NAME_MAX];
  if (bsd::GetProcessExecutablePath(pid_, exe_path, sizeof(exe_path))) {
    if (access(exe_path, F_OK) != 0 && (errno == ENOENT || errno == ENOTDIR)) {
      bsd::MakeProcPath(pid_, "file", exe_path, sizeof(exe_path));
      if (access(exe_path, F_OK) == 0) {
        strcpy(path, exe_path);
      }
    }
  }
}

// Generates a fake maps file similar to linux /proc/pid/maps
bool BSDDumper::CopyLinuxMaps(void **buf, size_t *size) {
  std::stringstream ss;
  std::vector<bsd::Mapping> mappings;

  if (!ReadMappings(mappings))
    return false;

  for (size_t n = 0; n < mappings.size(); n++) {
    const bsd::Mapping &m = mappings[n];
    ss << "0x"
       << std::hex << std::setw(sizeof(uintptr_t) << 1) << std::setfill('0')
       << m.start
       << '-'
       << "0x"
       << std::hex << std::setw(sizeof(uintptr_t) << 1) << std::setfill('0')
       << m.end
       << std::dec
       << ' '
       << ((m.protection & PROT_READ) ? 'r' : '-')
       << ((m.protection & PROT_WRITE) ? 'w' : '-')
       << ((m.protection & PROT_EXEC) ? 'x' : '-')
       << ((m.protection & bsd::Mapping::PROT_PRIVATE) ? 'p' : 's')
       << ' '
       << std::hex << std::setw(sizeof(uintptr_t) << 1) << std::setfill('0')
       << m.offset
       << ' ';

    // Try to obtain the info for this file.
    if (m.name[0]) {
       struct stat st;
       if (stat(m.name, &st) == 0) {
         ss << std::hex << major(st.st_dev) << ':' << minor(st.st_dev) << ' '
            << std::dec << st.st_ino;
       } else {
         ss << std::hex << 0 << ':' << 0 << ' ' << std::dec << 0;
       }

       ss << '\t' << m.name;
    } else {
      ss << std::hex << 0 << ':' << 0 << ' ' << std::dec << 0;
    }
    ss << std::endl;
  }
 
  std::string str(ss.str());
  *buf = reinterpret_cast<char*>(calloc(1, str.size()+1));
  if (*buf == NULL)
    return false;

  memcpy(*buf, str.c_str(), str.size());
  *size = str.size();
  return true;
}

}  // namespace google_breakpad
