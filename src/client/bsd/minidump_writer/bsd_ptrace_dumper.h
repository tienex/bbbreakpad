// Copyright (c) 2012, Google Inc.
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are
// met:
//
//     * Redistributions of source code must retain the above copyright
// notice, this list of conditions and the following disclaimer.
//     * Redistributions in binary form must reproduce the above
// copyright notice, this list of conditions and the following disclaimer
// in the documentation and/or other materials provided with the
// distribution.
//     * Neither the name of Google Inc. nor the names of its
// contributors may be used to endorse or promote products derived from
// this software without specific prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
// "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
// LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
// A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
// OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
// SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
// LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
// DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
// THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
// (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
// OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

// bsd_ptrace_dumper.h: Define the google_breakpad::BSDPtraceDumper
// class, which is derived from google_breakpad::BSDDumper to extract
// information from a crashed process via ptrace.
// This class was originally splitted from google_breakpad::BSDDumper.

#ifndef CLIENT_BSD_MINIDUMP_WRITER_BSD_PTRACE_DUMPER_H_
#define CLIENT_BSD_MINIDUMP_WRITER_BSD_PTRACE_DUMPER_H_

#include "client/bsd/minidump_writer/bsd_dumper.h"

namespace google_breakpad {

class BSDPtraceDumper : public BSDDumper {
 public:
  // Constructs a dumper for extracting information of a given process
  // with a process ID of |pid|.
  explicit BSDPtraceDumper(pid_t pid);
  virtual ~BSDPtraceDumper();

  virtual bool Init();

  // Implements BSDDumper::CopyFromProcess().
  // Copies content of |length| bytes from a given thread |lwpid|,
  // starting from |src|, into |dest|. This method uses ptrace to extract
  // the content from the target process. Always returns true.
  virtual bool CopyFromProcess(void* dest, lwpid_t lwpid, const void* src,
                               size_t length);

  // Implements BSDDumper::GetThreadInfoByIndex().
  // Reads information about the |index|-th thread of |threads_|.
  // Returns true on success. One must have called |ThreadsSuspend| first.
  virtual bool GetThreadInfoByIndex(size_t index, ThreadInfo* info);

  // Implements BSDDumper::IsPostMortem().
  // Always returns false to indicate this dumper performs a dump of
  // a crashed process via ptrace.
  virtual bool IsPostMortem() const;

  // Implements BSDDumper::ThreadsSuspend().
  // Suspends all threads in the given process. Returns true on success.
  virtual bool ThreadsSuspend();

  // Implements BSDDumper::ThreadsResume().
  // Resumes all threads in the given process. Returns true on success.
  virtual bool ThreadsResume();

 protected:
  // Implements BSDDumper::EnumerateThreads().
  // Enumerates all threads of the given process into |threads_|.
  virtual bool EnumerateThreads();

  virtual bool ReadAuxv();

 public:
  virtual bool CopyCommandLine(void **buf, size_t *size);
  virtual bool CopyEnvironment(void **buf, size_t *size);
  virtual bool CopyLinuxMaps(void **buf, size_t *size);
  virtual bool GetTimes(bsd::ProcessTimes &times);

 private:
   bool Attach();
   void Detach();

 private:
  // Set to true if all threads of the crashed process are suspended.
  bool threads_suspended_;
  pid_t parent_pid_;
  int attached_;
};

}  // namespace google_breakpad

#endif  // CLIENT_BSD_HANDLER_BSD_PTRACE_DUMPER_H_
