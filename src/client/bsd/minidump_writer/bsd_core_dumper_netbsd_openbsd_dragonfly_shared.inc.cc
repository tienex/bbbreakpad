// Copyright (c) 2015, Orlando Bassotto.
// Copyright (c) 2012, Google Inc.
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are
// met:
//
//     * Redistributions of source code must retain the above copyright
// notice, this list of conditions and the following disclaimer.
//     * Redistributions in binary form must reproduce the above
// copyright notice, this list of conditions and the following disclaimer
// in the documentation and/or other materials provided with the
// distribution.
//     * Neither the name of Google Inc. nor the names of its
// contributors may be used to endorse or promote products derived from
// this software without specific prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
// "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
// LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
// A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
// OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
// SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
// LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
// DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
// THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
// (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
// OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

namespace google_breakpad {

bool BSDCoreDumper::ReadMappings(std::vector<bsd::Mapping> &mappings) {
  if (!cached_mappings_.empty()) {
    mappings = cached_mappings_;
    return true;
  }

  bool res = false;

  // Only analyzing the Phdrs we can reconstruct the mappings. We perform
  // a dirty hack in order to retrieve the maps, we extract the exe path
  // from the arguments and read the proc mapping.

  // First, collect all the mappings.
  const ElfW(Ehdr) *ehdr = core_.GetHeader();
  cached_mappings_.clear();
  for (unsigned n = 0; n < ehdr->e_phnum; n++) {
    const ElfW(Phdr) *phdr = core_.GetProgramHeader(n);
    if (phdr->p_type != PT_LOAD)
      continue;

    bsd::Mapping m;
    memset(&m, 0, sizeof(m));

    m.start = phdr->p_vaddr;
    m.end = phdr->p_vaddr + phdr->p_memsz;
    m.offset = 0;
    m.protection = bsd::Mapping::PROT_PRIVATE;
    if (phdr->p_flags & PF_X) {
      m.protection |= PROT_EXEC;
    }
    if (phdr->p_flags & PF_W) {
      m.protection |= PROT_WRITE;
    }
    if (phdr->p_flags & PF_R) {
      m.protection |= PROT_READ;
    }
    cached_mappings_.push_back(m);
    res = true;
  }

#if defined(BSD_CORE_DIRTY_HACK)
  // Second, extract the command line
  char *cmdline;
  size_t cmdlinesz;
  if (CopyCommandLine(reinterpret_cast<void**>(&cmdline), &cmdlinesz)) {
    // Now spawn the process.
    pid_t pid = fork();
    if (pid >= 0) {
      if (pid == 0) {
        char * const argv[2] = { basename(cmdline), NULL };
        if (ptrace(PT_TRACE_ME, 0, NULL, 0) != 0)
          _exit(1);
        execvp(cmdline, argv);
        _exit(1);
      }

      // Set the breakpoint to the entry and read the live mappings,
      // this is really an hack!
      int status = 0;
      if (waitpid(pid, &status, 0) == pid && WIFSTOPPED(status)) {
        // Set breakpoint on entry.
#if defined(__i386__) || defined(__x86_64__)
        const uint32_t bpword = 0xcccccccc;
#elif defined(__vax__)
        const uint32_t bpword = 0xcccccccc;
#else
#error "Architecture not supported."
#endif
        if (bsd::WriteProcess(pid, auxv_[AT_ENTRY], &bpword,
                              sizeof(bpword)) == sizeof(bpword)) {
          // Continue and wait.
          if (ptrace(PT_CONTINUE, pid, reinterpret_cast<caddr_t>(1), 0) == 0) {
            if (waitpid(pid, &status, 0) == pid && WIFSTOPPED(status)) {
              // Read in the proc map
              std::vector<bsd::Mapping> livemappings;
              if (bsd::GetProcessMappings(pid, livemappings)) {
                AugmentMappings(pid, cached_mappings_, livemappings, true);
              }
            }
          }
        }
        // Kill it.
        ptrace(PT_KILL, pid, reinterpret_cast<caddr_t>(1), 0);
        waitpid(pid, NULL, 0);
      }
    }

    free(cmdline);
  }
#endif

  mappings = cached_mappings_;
  return res;
}

bool BSDCoreDumper::ReadTopOfStack(bsd::TopOfStack &tos, uintptr_t *offset) {
  return google_breakpad::ReadTopOfStack(*this, tos, offset);
}

bool BSDCoreDumper::GetTimes(bsd::ProcessTimes &times) {
  return false;
}

}
