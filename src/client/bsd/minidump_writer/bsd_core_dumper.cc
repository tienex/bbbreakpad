// Copyright (c) 2015, Orlando Bassotto.
// Copyright (c) 2012, Google Inc.
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are
// met:
//
//     * Redistributions of source code must retain the above copyright
// notice, this list of conditions and the following disclaimer.
//     * Redistributions in binary form must reproduce the above
// copyright notice, this list of conditions and the following disclaimer
// in the documentation and/or other materials provided with the
// distribution.
//     * Neither the name of Google Inc. nor the names of its
// contributors may be used to endorse or promote products derived from
// this software without specific prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
// "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
// LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
// A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
// OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
// SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
// LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
// DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
// THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
// (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
// OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

// bsd_core_dumper.cc: Implement google_breakpad::BSDCoreDumper.
// See bsd_core_dumper.h for details.

#include "client/bsd/minidump_writer/bsd_core_dumper.h"

#include <sys/types.h>
#include <sys/ptrace.h>
#include <sys/wait.h>
#include <assert.h>
#include <errno.h>
#include <stdio.h>
#include <string.h>

#include "common/bsd/bsd_libc_support.h"
#include "common/bsd/bsd_utils.h"

namespace google_breakpad {

BSDCoreDumper::BSDCoreDumper(pid_t pid,
                             const char* core_path)
    : BSDDumper(pid),
      core_path_(core_path),
      thread_infos_(&allocator_, 8) {
  assert(core_path_);
}

bool BSDCoreDumper::Init() {
  if (!mapped_core_file_.Map(core_path_, 0)) {
    fprintf(stderr, "Could not map core dump file into memory\n");
    return false;
  }

  core_.SetContent(mapped_core_file_.content());
  if (!core_.IsValid()) {
    fprintf(stderr, "Invalid core dump file\n");
    return false;
  }

  if (!core_.GetFirstNote().IsValid()) {
    fprintf(stderr, "PT_NOTE section not found\n");
    return false;
  }

  return BSDDumper::Init();
}

bool BSDCoreDumper::CopyFromProcess(void* dest, lwpid_t,
                                    const void* src, size_t length) {
  ElfCoreDump::Addr virtual_address = reinterpret_cast<ElfCoreDump::Addr>(src);
  // TODO(benchan): Investigate whether the data to be copied could span
  // across multiple segments in the core dump file. ElfCoreDump::CopyData
  // and this method do not handle that case yet.
  if (!core_.CopyData(dest, virtual_address, length)) {
    // If the data segment is not found in the core dump, fill the result
    // with marker characters.
    memset(dest, 0xab, length);
    return false;
  }
  return true;
}

bool BSDCoreDumper::CopyFromProcess(void* dest, lwpid_t lwpid, const void* src,
                                    size_t length, bool filltop,
                                    uint8_t fillbyte) {
  ElfCoreDump::Addr virtual_address = reinterpret_cast<ElfCoreDump::Addr>(src);
  // TODO(benchan): Investigate whether the data to be copied could span
  // across multiple segments in the core dump file. ElfCoreDump::CopyData
  // and this method do not handle that case yet.
  if (!core_.CopyData(dest, virtual_address, length, filltop, fillbyte)) {
    // If the data segment is not found in the core dump, fill the result
    // with marker characters.
    memset(dest, 0xab, length);
    return false;
  }
  return true;
}

bool BSDCoreDumper::GetThreadInfoByIndex(size_t index, ThreadInfo* info) {
  if (index >= thread_infos_.size())
    return false;

  *info = thread_infos_[index];
  const uint8_t* stack_pointer;
  memcpy(&stack_pointer, info->context.get_sp_p(),
         sizeof(*info->context.get_sp_p()));
  info->stack_pointer = reinterpret_cast<uintptr_t>(stack_pointer);
  return true;
}

bool BSDCoreDumper::IsPostMortem() const {
  return true;
}

bool BSDCoreDumper::ThreadsSuspend() {
  return true;
}

bool BSDCoreDumper::ThreadsResume() {
  return true;
}

bool BSDCoreDumper::FindFirstNote(ElfCoreDump::Word type,
                                  ElfCoreDump::Note &note) {
  note = core_.GetFirstNote();
  do {
    ElfCoreDump::Word ntype = note.GetType();
    MemoryRange name = note.GetName();
    MemoryRange description = note.GetDescription();

    if (ntype == 0 || name.IsEmpty() || description.IsEmpty()) {
      fprintf(stderr, "Could not found a valid PT_NOTE.\n");
      return false;
    }

    if (type == ntype)
      return true;

    note = note.GetNextNote();
  } while (note.IsValid());
  return false;
}

bool BSDCoreDumper::FindNextNote(ElfCoreDump::Note &note) {
  ElfCoreDump::Word type = note.GetType();
  if (type == 0)
    return false;
  do {
    ElfCoreDump::Word ntype = note.GetType();
    MemoryRange name = note.GetName();
    MemoryRange description = note.GetDescription();

    if (ntype == 0 || name.IsEmpty() || description.IsEmpty()) {
      fprintf(stderr, "Could not found a valid PT_NOTE.\n");
      return false;
    }

    if (type == ntype)
      return true;

    note = note.GetNextNote();
  } while (note.IsValid());
  return false;
}

bool BSDCoreDumper::IsReady() const {
  return !thread_infos_.empty() && !mappings_.empty();
}

// Forward declaration.
static inline bool IsMainThread(const ThreadInfo &info);

#if defined(DEFAULT_TOP_OF_STACK_LOCATION)
// An utility function to extract TopOfStack.
static bool ReadTopOfStack(BSDCoreDumper &dumper, bsd::TopOfStack &tos,
                           uintptr_t *tos_pointer) {
  ThreadInfo info;
  if (!dumper.IsReady()) {
    // Threads or mapping haven't been read in yet, try first to the
    // default locationi, this case is only for NetBSD and DragonFly.
#if defined(DEFAULT_TOP_OF_STACK_LOCATION64)
    if (dumper.CopyFromProcess(&tos, 0,
                               reinterpret_cast<const void *>
                               (DEFAULT_TOP_OF_STACK_LOCATION64),
                               sizeof(tos)))
      return true;
#endif
    return dumper.CopyFromProcess(&tos, 0,
            reinterpret_cast<const void *>(DEFAULT_TOP_OF_STACK_LOCATION),
            sizeof(tos));
  }

  for (size_t n = 0;; n++) {
    if (!dumper.GetThreadInfoByIndex(n, &info))
      return false;

    if (IsMainThread(info))
      break;
  }

  const void *stack_pointer;
  size_t stack_length;
  if (!dumper.GetStackInfo(&stack_pointer, &stack_length, info.stack_pointer))
    return false;

  char *stack = reinterpret_cast<char*>(malloc(stack_length));
  if (stack == NULL)
    return false;

  if (!dumper.CopyFromProcess(stack, 0, stack_pointer, stack_length,
                              /*filltop=*/false)) {
    free(stack);
    return false;
  }

  size_t tos_off = stack_length - 1;
  for (; tos_off > 0; tos_off--) {
    if (stack[tos_off - 1] != 0)
      break;
  }

  tos_off = (tos_off + 15) & -16;
  tos_off -= sizeof(bsd::TopOfStack);

  tos = *reinterpret_cast<const bsd::TopOfStack*>(stack + tos_off);
  if (tos_pointer != NULL) {
    *tos_pointer = (reinterpret_cast<uintptr_t>(stack_pointer) + tos_off);
  }

  free(stack);
  return true;
}
#endif

bool BSDCoreDumper::CopyCommandLine(void **buf, size_t *size) {
  bsd::TopOfStack tos;
  if (!ReadTopOfStack(tos))
    return false;

  // The arguments spans from the lowest address to the beginning of
  // the environment strings.

  // Read in the first arg pointer.
  uintptr_t arg0;
  if (!CopyFromProcess(&arg0, 0, reinterpret_cast<const void *>(tos.argv),
                       sizeof(uintptr_t)))
    return false;

  // Read in the first env pointer.
  uintptr_t env0;
  if (!CopyFromProcess(&env0, 0, reinterpret_cast<const void *>(tos.envp),
                       sizeof(uintptr_t)))
    return false;

  // Compute the arguments size and copy in the strings.
  size_t argsize = (env0 - arg0);
  char *strings = reinterpret_cast<char*>(calloc(1, argsize + 1));
  if (strings == NULL)
    return false;

  if (!CopyFromProcess(strings, 0, reinterpret_cast<const void *>(arg0),
                       argsize)) {
    free(strings);
    return false;
  }

  // Scan argument strings to find the last.
  char const *arg = strings;
  while (*arg != 0) {
    arg += strlen(arg) + 1;
  }

  argsize = arg - strings;
  if (argsize == 0) {
    free(strings);
    return false;
  }

  *buf = strings;
  *size = argsize;
  return true;
}

bool BSDCoreDumper::CopyEnvironment(void **buf, size_t *size) {
  bsd::TopOfStack tos;
  uintptr_t tos_location;
  if (!ReadTopOfStack(tos, &tos_location))
    return false;

  // The environment spans from the lowest address to the beginning of
  // the TopOfStack structure.

  // Read in the first env pointer.
  uintptr_t env0;
  if (!CopyFromProcess(&env0, 0, reinterpret_cast<const void *>(tos.envp),
                       sizeof(uintptr_t)))
    return false;

  // Compute the environment size and copy in the strings.
  size_t envsize = (tos_location - env0);
  char *strings = reinterpret_cast<char*>(calloc(1, envsize + 1));
  if (strings == NULL)
    return false;

  if (!CopyFromProcess(strings, 0, reinterpret_cast<const void *>(env0),
                       envsize)) {
    free(strings);
    return false;
  }

  // Scan environment strings to find the last.
  char const *env = strings;
  while (*env != 0) {
    env += strlen(env) + 1;
  }

  envsize = env - strings;
  if (envsize == 0) {
    free(strings);
    return false;
  }

  *buf = strings;
  *size = envsize;
  return true;
}

}  // namespace google_breakpad

#if defined(__DragonFly__)
#include "client/bsd/minidump_writer/bsd_core_dumper_dragonfly.inc.cc"
#elif defined(__FreeBSD__)
#include "client/bsd/minidump_writer/bsd_core_dumper_freebsd.inc.cc"
#elif defined(__NetBSD__)
#include "client/bsd/minidump_writer/bsd_core_dumper_netbsd.inc.cc"
#elif defined(__OpenBSD__)
#include "client/bsd/minidump_writer/bsd_core_dumper_openbsd.inc.cc"
#else
#error "I don't know this BSD!"
#endif
