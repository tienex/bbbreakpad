// Copyright (c) 2012, Google Inc.
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are
// met:
//
//     * Redistributions of source code must retain the above copyright
// notice, this list of conditions and the following disclaimer.
//     * Redistributions in binary form must reproduce the above
// copyright notice, this list of conditions and the following disclaimer
// in the documentation and/or other materials provided with the
// distribution.
//     * Neither the name of Google Inc. nor the names of its
// contributors may be used to endorse or promote products derived from
// this software without specific prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
// "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
// LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
// A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
// OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
// SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
// LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
// DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
// THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
// (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
// OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

// bsd_core_dumper.h: Define the google_breakpad::BSDCoreDumper
// class, which is derived from google_breakpad::BSDDumper to extract
// information from a crashed process via its core dump and proc files.

#ifndef CLIENT_BSD_MINIDUMP_WRITER_BSD_CORE_DUMPER_H_
#define CLIENT_BSD_MINIDUMP_WRITER_BSD_CORE_DUMPER_H_

#include "client/bsd/minidump_writer/bsd_dumper.h"
#include "common/bsd/elf_core_dump.h"
#include "common/bsd/memory_mapped_file.h"

namespace google_breakpad {

class BSDCoreDumper : public BSDDumper {
 public:
  // Constructs a dumper for extracting information of a given process
  // with a process ID of |pid| via its core dump file at |core_path|.
  BSDCoreDumper(pid_t pid, const char* core_path);

  // Parse the data for |threads| and |mappings|.
  virtual bool Init();

  // Implements BSDDumper::CopyFromProcess().
  // Copies content of |length| bytes from a given process |child|,
  // starting from |src|, into |dest|. This method extracts the content
  // the core dump and fills |dest| with a sequence of marker bytes
  // if the expected data is not found in the core dump. Returns true if
  // the expected data is found in the core dump.
  virtual bool CopyFromProcess(void* dest, pid_t child, const void* src,
                               size_t length);
  virtual bool CopyFromProcess(void* dest, lwpid_t lwpid, const void* src,
                               size_t length, bool filltop,
                               uint8_t fillbyte = 0);

  // Implements BSDDumper::GetThreadInfoByIndex().
  // Reads information about the |index|-th thread of |threads_|.
  // Returns true on success. One must have called |ThreadsSuspend| first.
  virtual bool GetThreadInfoByIndex(size_t index, ThreadInfo* info);

  // Implements BSDDumper::IsPostMortem().
  // Always returns true to indicate that this dumper performs a
  // post-mortem dump of a crashed process via a core dump file.
  virtual bool IsPostMortem() const;

  // Implements BSDDumper::ThreadsSuspend().
  // As the dumper performs a post-mortem dump via a core dump file,
  // there is no threads to suspend. This method does nothing and
  // always returns true.
  virtual bool ThreadsSuspend();

  // Implements BSDDumper::ThreadsResume().
  // As the dumper performs a post-mortem dump via a core dump file,
  // there is no threads to resume. This method does nothing and
  // always returns true.
  virtual bool ThreadsResume();

 protected:
  virtual bool ReadAuxv();
  virtual bool ReadMappings(std::vector<bsd::Mapping> &mappings);

  // Implements BSDDumper::EnumerateThreads().
  // Enumerates all threads of the given process into |threads_|.
  virtual bool EnumerateThreads();

 public:
  virtual bool CopyCommandLine(void **buf, size_t *size);
  virtual bool CopyEnvironment(void **buf, size_t *size);
  virtual bool GetTimes(bsd::ProcessTimes &times);

 private:
  bool FindFirstNote(ElfCoreDump::Word type, ElfCoreDump::Note &note);
  bool FindNextNote(ElfCoreDump::Note &note);

 public:
  // Used internally by ReadTopOfStack to determine if threads and mappings
  // are available to scavenge the stack to extract process information.
  inline bool IsReady() const;

 private:
  bool ReadTopOfStack(bsd::TopOfStack &tos, uintptr_t *offset = NULL);

 private:
  // Path of the core dump file.
  const char* core_path_;

  // Memory-mapped core dump file at |core_path_|.
  MemoryMappedFile mapped_core_file_;

  // Content of the core dump file.
  ElfCoreDump core_;

  // Thread info found in the core dump file.
  wasteful_vector<ThreadInfo> thread_infos_;

  // A cache of BSD mappings, with resolve paths.
  std::vector<bsd::Mapping> cached_mappings_;
};

}  // namespace google_breakpad

#endif  // CLIENT_BSD_HANDLER_BSD_CORE_DUMPER_H_
