// Copyright (c) 2015, Orlando Bassotto.
// Copyright (c) 2012, Google Inc.
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are
// met:
//
//     * Redistributions of source code must retain the above copyright
// notice, this list of conditions and the following disclaimer.
//     * Redistributions in binary form must reproduce the above
// copyright notice, this list of conditions and the following disclaimer
// in the documentation and/or other materials provided with the
// distribution.
//     * Neither the name of Google Inc. nor the names of its
// contributors may be used to endorse or promote products derived from
// this software without specific prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
// "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
// LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
// A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
// OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
// SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
// LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
// DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
// THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
// (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
// OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

//
// Code specific to DragonFly.
//

namespace google_breakpad {

bool BSDCoreDumper::ReadAuxv() {
  ElfCoreDump::Note note;
  ElfW(Auxinfo) auxinfo[32];
  const ElfW(Auxinfo) *one_aux_entry;
  uint32_t count;
  if (FindFirstNote(NT_AUXV, note)) {
    MemoryRange description = note.GetDescription();
    one_aux_entry = reinterpret_cast<const ElfW(Auxinfo)*>(description.data());
    count = description.length() / sizeof(*one_aux_entry);

    if (description.length() % sizeof(*one_aux_entry)) {
      fprintf(stderr, "Found NT_AUXV descriptor of unexpected size\n");
      return false;
    }
  } else {
    bsd::TopOfStack tos;
    // Obtain top of the stack.
    if (!ReadTopOfStack(tos))
      return false;

    // The auxv is located after the last environment pointer.
    if (!CopyFromProcess(auxinfo, 0,
                         reinterpret_cast<const void *>
                             (tos.envp + (tos.nenvs + 1) * sizeof(uintptr_t)),
                         sizeof(auxinfo)))
      return false;

    one_aux_entry = auxinfo;
    count = sizeof(auxinfo) / sizeof(auxinfo[0]);
  }

  bool res = false;
  for (uint32_t n = 0; n < count; n++) {
    if (ELF_AUX_TYPE(&one_aux_entry[n]) == AT_NULL)
      break;
    if (ELF_AUX_TYPE(&one_aux_entry[n]) < AT_COUNT) {
      auxv_[ELF_AUX_TYPE(&one_aux_entry[n])] =
          ELF_AUX_VALUE(&one_aux_entry[n]);
      auxv_present_[ELF_AUX_TYPE(&one_aux_entry[n])] = true;
      res = true;
    }
  }

  return res;
}

bool BSDCoreDumper::EnumerateThreads() {
  ElfCoreDump::Note note = core_.GetFirstNote();
  bool first_thread = true;
  pid_t pid = 1234, ppid = 1;
  do {
    ElfCoreDump::Word type = note.GetType();
    MemoryRange name = note.GetName();
    MemoryRange description = note.GetDescription();

    if (type == 0 || name.IsEmpty() || description.IsEmpty()) {
      fprintf(stderr, "Could not found a valid PT_NOTE.\n");
      return false;
    }

    switch (type) {
      case NT_PRSTATUS: {
        if (description.length() != sizeof(prstatus_t)) {
          fprintf(stderr, "Found NT_PRSTATUS descriptor of unexpected size\n");
          return false;
        }

        const prstatus_t* status =
            reinterpret_cast<const prstatus_t*>(description.data());
        if (status->pr_version != PRSTATUS_VERSION) {
          fprintf(stderr, "Found NT_PRSTATUS descriptor of unknown version\n");
          return false;
        }
        lwpid_t lwpid = status->pr_pid;
        ThreadInfo info;
        memset(&info, 0, sizeof(ThreadInfo));
        info.lwpid = lwpid;
        info.pid = pid;
        info.ppid = ppid;
        memcpy(&info.context.gregs, &status->pr_reg,
               sizeof(info.context.gregs));
        if (first_thread) {
          crash_thread_ = lwpid;
          crash_signal_ = status->pr_cursig;
        }
        first_thread = false;
        threads_.push_back(lwpid);
        thread_infos_.push_back(info);
        break;
      }

      case NT_FPREGSET: {
        if (thread_infos_.empty())
          return false;

        ThreadInfo* info = &thread_infos_.back();
        if (description.length() > sizeof(info->context.fregs)) {
          fprintf(stderr, "Found NT_FPREGSET descriptor of unexpected size\n");
          return false;
        }

        memcpy(&info->context.fregs, description.data(),
               sizeof(info->context.fregs));
        break;
      }
    }
    note = note.GetNextNote();
  } while (note.IsValid());
  return true;
}

static inline bool IsMainThread(const ThreadInfo &info) {
  return info.lwpid == 0;
}

}

// Enable the dirty hack to retrieve the libraries from a core file.
#define BSD_CORE_DIRTY_HACK 1

//
// Include code shared across NetBSD, OpenBSD and DragonFly.
//
#include "client/bsd/minidump_writer/bsd_core_dumper_netbsd_openbsd_dragonfly_shared.inc.cc"
