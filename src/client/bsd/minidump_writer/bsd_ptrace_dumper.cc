// Copyright (c) 2012, Google Inc.
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are
// met:
//
//     * Redistributions of source code must retain the above copyright
// notice, this list of conditions and the following disclaimer.
//     * Redistributions in binary form must reproduce the above
// copyright notice, this list of conditions and the following disclaimer
// in the documentation and/or other materials provided with the
// distribution.
//     * Neither the name of Google Inc. nor the names of its
// contributors may be used to endorse or promote products derived from
// this software without specific prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
// "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
// LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
// A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
// OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
// SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
// LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
// DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
// THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
// (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
// OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

// bsd_ptrace_dumper.cc: Implement google_breakpad::BSDPtraceDumper.
// See bsd_ptrace_dumper.h for detals.
// This class was originally splitted from google_breakpad::BSDDumper.

// This code deals with the mechanics of getting information about a crashed
// process. Since this code may run in a compromised address space, the same
// rules apply as detailed at the top of minidump_writer.h: no libc calls and
// use the alternative allocator.

#include "client/bsd/minidump_writer/bsd_ptrace_dumper.h"

#include <sys/types.h>
#include <sys/ptrace.h>
#include <machine/reg.h>
#include <assert.h>
#include <errno.h>
#include <fcntl.h>
#include <limits.h>
#include <stddef.h>
#include <stdlib.h>
#include <string.h>
#include <sys/ptrace.h>
#include <sys/uio.h>
#include <sys/wait.h>

#include "common/bsd/bsd_libc_support.h"
#include "common/bsd/bsd_utils.h"

#if defined(__NetBSD__) || defined(__OpenBSD__) || defined(__DragonFly__)
// NetBSD, OpenBSD and DragonFly require ptrace to read auxv,
// assume success until we attach.
#define BSD_LATE_AUXV 1
#endif

#if defined(__OpenBSD__)
// OpenBSD has no way to perfom vnode->path, reloading will read in
// the DSO debug structure and extract library names.
#define BSD_RELOAD_MAPPINGS 1
#endif

namespace google_breakpad {

BSDPtraceDumper::BSDPtraceDumper(pid_t pid)
    : BSDDumper(pid),
      threads_suspended_(false),
      parent_pid_(0),
      attached_(0) {
}

BSDPtraceDumper::~BSDPtraceDumper() {
}

bool BSDPtraceDumper::Init() {
  if (!BSDDumper::Init())
    return false;

  parent_pid_ = bsd::GetParentProcessId(pid_);
  if (parent_pid_ < 0) {
    parent_pid_ = 0;
  }
  return true;
}

static bool SysDetach(pid_t pid) {
  return !(ptrace(PT_DETACH, pid, reinterpret_cast<caddr_t>(1), 0) < 0);
}

static inline bool SysAttach(pid_t pid) {
  // This may fail if the thread has just died or debugged.
  errno = 0;
  if (ptrace(PT_ATTACH, pid, NULL, 0) != 0 && errno != 0)
    return false;

  while (waitpid(pid, NULL, WUNTRACED) < 0) {
    if (errno != EINTR) {
      SysDetach(pid);
      return false;
    }
  }

  return true;
}

bool BSDPtraceDumper::Attach() {
  if (attached_++ != 0)
    return true;

  if (!SysAttach(pid_))
    return false;

  attached_ = true;
  return true;
}

void BSDPtraceDumper::Detach() {
   if (!attached_)
     return;

  if (--attached_ == 0) {
    SysDetach(pid_);
#if defined(__OpenBSD__)
    // Apparently, when detaching with ptrace(2) in a multithread scenario,
    // signals are lost (this can also be seen with GDB); the only workaround
    // I could find is to sent A LOT of SIGCONT and hope for the best.
    if (threads_.size() > 1) {
      for (size_t n = 0; n < 10000; n++) {
        kill(pid_, SIGCONT);
      }
    }
#endif
  }
}

bool BSDPtraceDumper::CopyFromProcess(void* dest, lwpid_t lwpid,
                                      const void* src, size_t length) {
  if (bsd::ReadProcess(pid_, dest, reinterpret_cast<uintptr_t>(src),
                       length) < 0) {
    memset(dest, 0, length);
  }
  return true;
}

// Read thread info
// Fill out the |tgid|, |ppid| and |pid| members of |info|. If unavailable,
// these members are set to -1. Returns true iff all three members are
// available.
bool BSDPtraceDumper::GetThreadInfoByIndex(size_t index, ThreadInfo* info) {
  if (index >= threads_.size())
    return false;

  lwpid_t lwpid = threads_[index];

  assert(info != NULL);

  info->lwpid = lwpid;
  info->pid = pid_;
  info->ppid = parent_pid_;

  if (!bsd::GetThreadContext(pid_, lwpid, info->context))
    return false;

  const uint8_t* stack_pointer;
  memcpy(&stack_pointer, info->context.get_sp_p(),
         sizeof(*info->context.get_sp_p()));
  info->stack_pointer = reinterpret_cast<uintptr_t>(stack_pointer);

  return true;
}

bool BSDPtraceDumper::IsPostMortem() const {
  return false;
}

bool BSDPtraceDumper::ReadAuxv() {
#if defined(BSD_LATE_AUXV)
  // NetBSD, OpenBSD and DragonFly require ptrace to read auxv,
  // assume success until we attach.
  if (!attached_)
    return true;
#endif
  return BSDDumper::ReadAuxv();
}

bool BSDPtraceDumper::ThreadsSuspend() {
  if (threads_suspended_)
    return true;

  // Attaching is enough to stop the world in the target process.
  if (!Attach())
    return false;

#if defined(BSD_LATE_AUXV)
  if (!ReadAuxv()) {
    Detach();
    return false;
  }
#endif

#if defined(BSD_RELOAD_MAPPINGS)
  if (!EnumerateMappings()) {
    Detach();
    return false;
  }
#endif

  threads_suspended_ = true;
  return threads_.size() > 0;
}

bool BSDPtraceDumper::ThreadsResume() {
  if (!threads_suspended_)
    return false;

  threads_suspended_ = false;
  Detach();
  return true;
}

bool BSDPtraceDumper::EnumerateThreads() {
  return bsd::GetProcessThreads(pid_, threads_);
}

bool BSDPtraceDumper::CopyCommandLine(void **buf, size_t *size) {
  *buf = bsd::GetProcessArguments(pid_, size);
  return (*buf != NULL);
}

bool BSDPtraceDumper::CopyEnvironment(void **buf, size_t *size) {
  *buf = bsd::GetProcessEnvironment(pid_, size);
  return (*buf != NULL);
}

bool BSDPtraceDumper::CopyLinuxMaps(void **buf, size_t *size) {
  *buf = bsd::ReadProcFile(pid_, "maps", size);
  if (*buf != NULL)
    return true;

  return BSDDumper::CopyLinuxMaps(buf, size);
}

bool BSDPtraceDumper::GetTimes(bsd::ProcessTimes &times) {
  return bsd::GetProcessTimes(pid_, times);
}

}  // namespace google_breakpad
