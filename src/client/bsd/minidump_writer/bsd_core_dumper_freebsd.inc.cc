// Copyright (c) 2015, Orlando Bassotto.
// Copyright (c) 2012, Google Inc.
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are
// met:
//
//     * Redistributions of source code must retain the above copyright
// notice, this list of conditions and the following disclaimer.
//     * Redistributions in binary form must reproduce the above
// copyright notice, this list of conditions and the following disclaimer
// in the documentation and/or other materials provided with the
// distribution.
//     * Neither the name of Google Inc. nor the names of its
// contributors may be used to endorse or promote products derived from
// this software without specific prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
// "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
// LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
// A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
// OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
// SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
// LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
// DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
// THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
// (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
// OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

//
// Code specific to FreeBSD.
//

namespace google_breakpad {

bool BSDCoreDumper::ReadAuxv() {
  ElfCoreDump::Note note;
  if (!FindFirstNote(NT_PROCSTAT_AUXV, note))
    return false;

  MemoryRange description = note.GetDescription();

  const uint32_t *countp =
     reinterpret_cast<const uint32_t *>(description.data());
  const ElfW(Auxinfo) *auxinfo =
     reinterpret_cast<const ElfW(Auxinfo)*>(countp + 1);

  if ((description.length() - sizeof(uint32_t)) % sizeof(*auxinfo)) {
    fprintf(stderr, "Found NT_PROCSTAT_AUXV descriptor of unexpected size\n");
    return false;
  }

  const uint32_t count = *countp;
  bool res = false;
  for (uint32_t n = 0; n < count; n++) {
    if (ELF_AUX_TYPE(&auxinfo[n]) < AT_COUNT) {
      auxv_[ELF_AUX_TYPE(&auxinfo[n])] = ELF_AUX_VALUE(&auxinfo[n]);
      auxv_present_[ELF_AUX_TYPE(&auxinfo[n])] = true;
      res = true;
    }
  }

  return res;
}

bool BSDCoreDumper::ReadMappings(std::vector<bsd::Mapping> &mappings) {
  bool res = false;
  ElfCoreDump::Note note = core_.GetFirstNote();
  do {
    ElfCoreDump::Word type = note.GetType();
    MemoryRange name = note.GetName();
    MemoryRange description = note.GetDescription();

    if (type == 0 || name.IsEmpty() || description.IsEmpty()) {
      fprintf(stderr, "Could not found a valid PT_NOTE.\n");
      return false;
    }

    if (type == NT_PROCSTAT_VMMAP) {
      const uint32_t *structsize =
         reinterpret_cast<const uint32_t *>(description.data());

      if (description.length() < sizeof(uint32_t) ||
          *structsize != sizeof(struct kinfo_vmentry)) {
        fprintf(stderr, "Found NT_PROCSTAT_VMMAP descriptor of "
                "unexpected size\n");
        return false;
      }

      const struct kinfo_vmentry *vmmap_entry =
         reinterpret_cast<const struct kinfo_vmentry *>(structsize + 1);
      const struct kinfo_vmentry *vmmap_last_entry =
         reinterpret_cast<const struct kinfo_vmentry *>
         (reinterpret_cast<uintptr_t>(vmmap_entry) + description.length() -
          sizeof(uint32_t));
      
      mappings.clear();
      for (uint32_t n = 0; vmmap_entry < vmmap_last_entry; n++) {
         bsd::Mapping m;
         memset(&m, 0, sizeof(m));

         m.start = vmmap_entry->kve_start;
         m.end = vmmap_entry->kve_end;
         m.offset = vmmap_entry->kve_offset;
         if (isascii(vmmap_entry->kve_path[0])) {
           strlcpy(m.name, vmmap_entry->kve_path, sizeof(m.name));
         }

         if ((vmmap_entry->kve_flags & KVME_FLAG_COW) != 0) {
             m.protection |= bsd::Mapping::PROT_PRIVATE;
         }

         if (vmmap_entry->kve_protection & KVME_PROT_EXEC) {
             m.protection |= PROT_EXEC;
         }
         if (vmmap_entry->kve_protection & KVME_PROT_WRITE) {
             m.protection |= PROT_WRITE;
         }
         if (vmmap_entry->kve_protection & KVME_PROT_READ) {
             m.protection |= PROT_READ;
         }

         mappings.push_back(m);

         vmmap_entry = reinterpret_cast<const struct kinfo_vmentry *>
            (reinterpret_cast<uintptr_t>(vmmap_entry) + vmmap_entry->kve_structsize);

         res = true;
      }

      break;
    }
    note = note.GetNextNote();
  } while (note.IsValid());
  return res;
}

bool BSDCoreDumper::EnumerateThreads() {
  bool first_thread = true;
  pid_t pid, ppid;

  ElfCoreDump::Note note;

  // We need first the NT_PROCSTAT_PROC to obtain pid and ppid.
  if (FindFirstNote(NT_PROCSTAT_PROC, note)) {
    MemoryRange description = note.GetDescription();

    const uint32_t *structsize =
        reinterpret_cast<const uint32_t *>(description.data());
    const struct kinfo_proc *proc_entry =
        reinterpret_cast<const struct kinfo_proc *>(structsize + 1);

    if (description.length() < sizeof(uint32_t) ||
        *structsize != sizeof(struct kinfo_proc) ||
        (description.length() - sizeof(uint32_t)) % sizeof(struct kinfo_proc)) {
      fprintf(stderr, "Found NT_PROCSTAT_PROC descriptor of unexpected size\n");
      return false;
    }

    // The first process is the leader.
    pid = proc_entry[0].ki_pid;
    ppid = proc_entry[0].ki_ppid;
  }

  note = core_.GetFirstNote();
  do {
    ElfCoreDump::Word type = note.GetType();
    MemoryRange name = note.GetName();
    MemoryRange description = note.GetDescription();

    if (type == 0 || name.IsEmpty() || description.IsEmpty()) {
      fprintf(stderr, "Could not found a valid PT_NOTE.\n");
      return false;
    }

    switch (type) {
      case NT_PRSTATUS: {
        if (description.length() != sizeof(prstatus_t)) {
          fprintf(stderr, "Found NT_PRSTATUS descriptor of unexpected size\n");
          return false;
        }

        const prstatus_t* status =
            reinterpret_cast<const prstatus_t*>(description.data());
        if (status->pr_version != PRSTATUS_VERSION) {
          fprintf(stderr, "Found NT_PRSTATUS descriptor of unknown version\n");
          return false;
        }
        lwpid_t lwpid = status->pr_pid;
        ThreadInfo info;
        memset(&info, 0, sizeof(ThreadInfo));
        info.lwpid = lwpid;
        info.pid = pid;
        info.ppid = ppid;
        memcpy(&info.context.gregs, &status->pr_reg,
               sizeof(info.context.gregs));
        if (first_thread) {
          crash_thread_ = lwpid;
          crash_signal_ = status->pr_cursig;
        }
        first_thread = false;
        threads_.push_back(lwpid);
        thread_infos_.push_back(info);
        break;
      }

      case NT_FPREGSET: {
        if (thread_infos_.empty())
          return false;

        ThreadInfo* info = &thread_infos_.back();
#if defined(__i386__)
        const size_t size = sizeof(struct save87);
#else
        const size_t size = sizeof(info->context.fregs);
#endif
        if (description.length() != size) {
          fprintf(stderr, "Found NT_FPREGSET descriptor of unexpected size\n");
          return false;
        }

        memcpy(&info->context.fregs, description.data(), size);
#if defined(__i386__)
        info->context.is_x87 = true;
#endif
        break;
      }

#if defined(__i386__) || defined(__x86_64__)

#if defined(__i386__)
#define savefpu_ymm savexmm_ymm
#endif

      case NT_X86_XSTATE: {
        if (thread_infos_.empty())
          return false;

        ThreadInfo* info = &thread_infos_.back();
        const size_t size = sizeof(info->context.fregs);
        if (description.length() != sizeof(struct savefpu_ymm)) {
          fprintf(stderr, "Found NT_X86_XSTATE descriptor of "
                  "unexpected size\n");
          return false;
        }

        memcpy(&info->context.fregs, description.data(),
               sizeof(info->context.fregs));
#if defined(__i386__)
        info->context.is_x87 = false;
#endif
        break;
      }
#endif
    }
    note = note.GetNextNote();
  } while (note.IsValid());
  return true;
}

bool BSDCoreDumper::ReadTopOfStack(bsd::TopOfStack &tos, uintptr_t *offset) {
  ElfCoreDump::Note note;
  if (!FindFirstNote(NT_PROCSTAT_PSSTRINGS, note))
    return false;

  MemoryRange description = note.GetDescription();

  const uint32_t *structsize =
      reinterpret_cast<const uint32_t *>(description.data());
  const uintptr_t *pssoff =
      reinterpret_cast<const uintptr_t *>(structsize + 1);

  if (description.length() != (sizeof(uint32_t) + sizeof(uintptr_t)) ||
      *structsize != sizeof(uintptr_t)) {
    fprintf(stderr, "Found NT_PROCSTAT_PSSTRINGS descriptor of "
            "unexpected size\n");
    return false;
  }

  if (offset != NULL) {
    *offset = *pssoff;
  }

  struct ps_strings ps_strings;
  if (!CopyFromProcess(&ps_strings, 0, reinterpret_cast<const void *>(*pssoff),
                       sizeof(ps_strings)))
    return false;

  tos.argv = reinterpret_cast<uintptr_t>(ps_strings.ps_argvstr);
  tos.argc = ps_strings.ps_nargvstr;
  tos.envp = reinterpret_cast<uintptr_t>(ps_strings.ps_envstr);
  tos.nenvs = ps_strings.ps_nenvstr;
  return true;
}

bool BSDCoreDumper::GetTimes(bsd::ProcessTimes &times) {
  ElfCoreDump::Note note;
  if (!FindFirstNote(NT_PROCSTAT_PROC, note))
    return false;

  MemoryRange description = note.GetDescription();

  const uint32_t *structsize =
      reinterpret_cast<const uint32_t *>(description.data());
  const struct kinfo_proc *proc_entry =
      reinterpret_cast<const struct kinfo_proc *>(structsize + 1);

  if (description.length() < sizeof(uint32_t) ||
      *structsize != sizeof(struct kinfo_proc) ||
      (description.length() - sizeof(uint32_t)) % sizeof(struct kinfo_proc)) {
    fprintf(stderr, "Found NT_PROCSTAT_PROC descriptor of unexpected size\n");
    return false;
  }

  // The first process is the leader.
  times.start.tv_sec   = proc_entry->ki_start.tv_sec;
  times.start.tv_nsec  = proc_entry->ki_start.tv_usec * 1000;
  times.user.tv_sec    = proc_entry->ki_rusage.ru_utime.tv_sec;
  times.user.tv_nsec   = proc_entry->ki_rusage.ru_utime.tv_usec * 1000;
  times.system.tv_sec  = proc_entry->ki_rusage.ru_stime.tv_sec;
  times.system.tv_nsec = proc_entry->ki_rusage.ru_stime.tv_usec * 1000;
  return true;
}

}
