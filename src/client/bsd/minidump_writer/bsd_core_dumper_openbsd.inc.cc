// Copyright (c) 2015, Orlando Bassotto.
// Copyright (c) 2012, Google Inc.
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are
// met:
//
//     * Redistributions of source code must retain the above copyright
// notice, this list of conditions and the following disclaimer.
//     * Redistributions in binary form must reproduce the above
// copyright notice, this list of conditions and the following disclaimer
// in the documentation and/or other materials provided with the
// distribution.
//     * Neither the name of Google Inc. nor the names of its
// contributors may be used to endorse or promote products derived from
// this software without specific prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
// "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
// LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
// A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
// OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
// SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
// LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
// DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
// THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
// (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
// OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

//
// Code specific to OpenBSD.
//

namespace google_breakpad {

bool BSDCoreDumper::ReadAuxv() {
  ElfCoreDump::Note note;
  if (!FindFirstNote(NT_OPENBSD_AUXV, note))
    return false;

  MemoryRange description = note.GetDescription();

  const ElfW(Auxinfo) *one_aux_entry =
     reinterpret_cast<const ElfW(Auxinfo)*>(description.data());
  const uint32_t count = description.length() / sizeof(*one_aux_entry);

  if (description.length() % sizeof(*one_aux_entry)) {
    fprintf(stderr, "Found NT_OPENBSD_AUXV descriptor of unexpected size\n");
    return false;
  }

  bool res = false;
  for (uint32_t n = 0; n < count; n++) {
    if (ELF_AUX_TYPE(&one_aux_entry[n]) < AT_COUNT) {
      auxv_[ELF_AUX_TYPE(&one_aux_entry[n])] =
          ELF_AUX_VALUE(&one_aux_entry[n]);
      auxv_present_[ELF_AUX_TYPE(&one_aux_entry[n])] = true;
      res = true;
    }
  }

  return res;
}

bool BSDCoreDumper::EnumerateThreads() {
  ElfCoreDump::Note note = core_.GetFirstNote();
  bool first_thread = true;
  pid_t pid, ppid;
  do {
    ElfCoreDump::Word type = note.GetType();
    MemoryRange name = note.GetName();
    MemoryRange description = note.GetDescription();

    if (type == 0 || name.IsEmpty() || description.IsEmpty()) {
      fprintf(stderr, "Could not found a valid PT_NOTE.\n");
      return false;
    }

    switch (type) {
      case NT_OPENBSD_PROCINFO: {
        if (description.length() != sizeof(struct elfcore_procinfo)) {
          fprintf(stderr, "Found NT_OPENBSD_PROCINFO descriptor "
                  "of unexpected size\n");
          return false;
        }

        const struct elfcore_procinfo *procinfo =
            reinterpret_cast<const struct elfcore_procinfo*>
                (description.data());

        pid = procinfo->cpi_pid;
        ppid = procinfo->cpi_ppid;
        crash_signal_ = procinfo->cpi_signo;
        crash_subcode_ = procinfo->cpi_sigcode;
        crash_errno_ = 0;
        break;
      }

      case NT_OPENBSD_REGS: {
        if (description.length() != sizeof(struct reg)) {
          fprintf(stderr, "Found NT_OPENBSD_REGS descriptor of "
                  "unexpected size\n");
          return false;
        }

        const char *name_str = reinterpret_cast<const char*>(name.data());
        const char *lwpid_str = strchr(name_str, '@');
        lwpid_t lwpid = pid;
        if (lwpid_str != NULL) {
            lwpid = atoi(lwpid_str + 1);
        }
        lwpid += 1000000;
        ThreadInfo info;
        memset(&info, 0, sizeof(ThreadInfo));
        info.lwpid = lwpid;
        info.pid = pid;
        info.ppid = ppid;
        memcpy(&info.context.gregs, description.data(), sizeof(struct reg));
        if (first_thread) {
          crash_thread_ = lwpid;
        }
        first_thread = false;
        threads_.push_back(lwpid);
        thread_infos_.push_back(info);
        break;
      }

#if defined(NT_OPENBSD_FPREGS)
      case NT_OPENBSD_FPREGS: {
        ThreadInfo* info = &thread_infos_.back();
        if (description.length() != sizeof(struct fpreg)) {
          fprintf(stderr, "Found NT_OPENBSD_FPREGS descriptor of "
                  "unexpected size\n");
          return false;
        }

        memcpy(&info->context.fregs, description.data(),
               sizeof(struct fpreg));
#if defined(__i386__)
        info->context.is_x87 = true;
#endif
        break;
      }
#endif

#if defined(__i386__) && defined(NT_OPENBSD_XFPREGS)
      case NT_OPENBSD_XFPREGS: {
        ThreadInfo* info = &thread_infos_.back();
        if (description.length() != sizeof(struct savexmm)) {
          fprintf(stderr, "Found NT_OPENBSD_XFPREGS descriptor of "
                  "unexpected size\n");
          return false;
        }

        memcpy(&info->context.fregs, description.data(),
               sizeof(struct savexmm));
        info->context.is_x87 = false;
        break;
      }
#endif
    }
    note = note.GetNextNote();
  } while (note.IsValid());
  return true;
}

static inline bool IsMainThread(const ThreadInfo &ti) {
  return (ti.pid + 1000000) == ti.lwpid;
}

}

//
// Include code shared across NetBSD, OpenBSD and DragonFly.
//
#include "client/bsd/minidump_writer/bsd_core_dumper_netbsd_openbsd_dragonfly_shared.inc.cc"
