// Copyright (c) 2015, Orlando Bassotto.
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are
// met:
//
//     * Redistributions of source code must retain the above copyright
// notice, this list of conditions and the following disclaimer.
//     * Redistributions in binary form must reproduce the above
// copyright notice, this list of conditions and the following disclaimer
// in the documentation and/or other materials provided with the
// distribution.
//     * Neither the name of Google Inc. nor the names of its
// contributors may be used to endorse or promote products derived from
// this software without specific prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
// "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
// LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
// A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
// OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
// SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
// LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
// DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
// THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
// (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
// OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

#include "google_breakpad/common/minidump_cpu_amd64.h"
#include "google_breakpad/common/minidump_cpu_x86.h"

namespace google_breakpad { namespace bsd {

#if defined(__i386__)

namespace {

static inline uint32_t rU32(const void* data, size_t offset) {
  uint32_t v;
  memcpy(&v, reinterpret_cast<const uint8_t*>(data) + offset, sizeof(v));
  return v;
}

static inline uint16_t rU16(const void* data, size_t offset) {
  uint16_t v;
  memcpy(&v, reinterpret_cast<const uint8_t*>(data) + offset, sizeof(v));
  return v;
}

// Write a uint16_t to memory
//   out: memory location to write to
//   v: value to write.
static inline void wU16(void* out, uint16_t v) {
  memcpy(out, &v, sizeof(v));
}

// Write a uint32_t to memory
//   out: memory location to write to
//   v: value to write.
static inline void wU32(void* out, uint32_t v) {
  memcpy(out, &v, sizeof(v));
}

}

ThreadContext::ThreadContext(const MDRawContextX86 &context) {
   clear();

   set_greg(GREG_EAX, context.eax);
   set_greg(GREG_EBX, context.ebx);
   set_greg(GREG_ECX, context.ecx);
   set_greg(GREG_EDX, context.edx);
   set_greg(GREG_ESI, context.esi);
   set_greg(GREG_EDI, context.edi);
   set_greg(GREG_EBP, context.ebp);
   set_greg(GREG_ESP, context.esp);
   set_greg(GREG_EIP, context.eip);
   set_greg(GREG_EFLAGS, context.eflags);
   set_greg(GREG_CS, context.cs);
   set_greg(GREG_DS, context.ds);
   set_greg(GREG_ES, context.es);
   set_greg(GREG_FS, context.fs);
   set_greg(GREG_GS, context.gs);
   set_greg(GREG_SS, context.ss);

   // It's always the SSE version.
   is_x87 = false;

   set_freg_ctl(FREG_CTL_CW, context.float_save.control_word);
   set_freg_ctl(FREG_CTL_SW, context.float_save.status_word);
   set_freg_ctl(FREG_CTL_TW, context.float_save.tag_word);
   set_freg_ctl(FREG_CTL_FOP, 0);
   set_freg_ctl(FREG_CTL_FCS, context.float_save.error_selector);
   set_freg_ctl(FREG_CTL_FIP, context.float_save.error_offset);
   set_freg_ctl(FREG_CTL_FOS, context.float_save.data_selector);
   set_freg_ctl(FREG_CTL_FOO, context.float_save.data_offset);
   set_freg_ctl(FREG_CTL_MXCSR, rU32(context.extended_registers, 24));

   for (size_t n = 0; n < 8; n++) {
     set_freg(static_cast<freg_name_t>(FREG_ST0 + n),
              context.float_save.register_area + n * 10);
     set_freg(static_cast<freg_name_t>(FREG_XMM0 + n),
              context.extended_registers + 160 + n * 16);
   }
}

void ThreadContext::FillRawContext(MDRawContextX86 &context) const {
  memset(&context, 0, sizeof(context));

  context.context_flags = MD_CONTEXT_X86_ALL;

  context.dr0 = get_dreg(0);
  context.dr1 = get_dreg(1);
  context.dr2 = get_dreg(2);
  context.dr3 = get_dreg(3);
  // 4 and 5 deliberatly omitted because they aren't included in the minidump
  // format.
  context.dr6 = get_dreg(6);
  context.dr7 = get_dreg(7);

  context.gs = get_greg(GREG_GS);
  context.fs = get_greg(GREG_FS);
  context.es = get_greg(GREG_ES);
  context.ds = get_greg(GREG_DS);

  context.edi = get_greg(GREG_EDI);
  context.esi = get_greg(GREG_ESI);
  context.ebx = get_greg(GREG_EBX);
  context.edx = get_greg(GREG_EDX);
  context.ecx = get_greg(GREG_ECX);
  context.eax = get_greg(GREG_EAX);

  context.ebp = get_greg(GREG_EBP);
  context.eip = get_greg(GREG_EIP);
  context.cs = get_greg(GREG_CS);
  context.eflags = get_greg(GREG_EFLAGS);
  context.esp = get_greg(GREG_ESP);
  context.ss = get_greg(GREG_SS);

  context.float_save.control_word = get_freg_ctl(FREG_CTL_CW);
  context.float_save.status_word = get_freg_ctl(FREG_CTL_SW);
  context.float_save.tag_word = get_freg_ctl(FREG_CTL_TW);
  context.float_save.error_offset = get_freg_ctl(FREG_CTL_FIP);
  context.float_save.error_selector = get_freg_ctl(FREG_CTL_FCS);
  context.float_save.data_offset = get_freg_ctl(FREG_CTL_FOO);
  context.float_save.data_selector = get_freg_ctl(FREG_CTL_FOS);

  // 8 registers * 10 bytes per register.
  if (get_freg(FREG_ST0) != NULL) {
    for (size_t n = 0; n < 8; n++) {
      const void *st = get_freg(static_cast<freg_name_t>
                                (FREG_ST0 + n));
      memcpy(context.float_save.register_area + 10 * n, st, 10);
    }
  }

  // This matches the Intel fpsave format.
  wU16(context.extended_registers + 0, get_freg_ctl(FREG_CTL_CW));
  wU16(context.extended_registers + 2, get_freg_ctl(FREG_CTL_SW));
  wU16(context.extended_registers + 4, get_freg_ctl(FREG_CTL_TW));
  wU16(context.extended_registers + 6, get_freg_ctl(FREG_CTL_FOP));
  wU32(context.extended_registers + 8, get_freg_ctl(FREG_CTL_FIP));
  wU16(context.extended_registers + 12, get_freg_ctl(FREG_CTL_FCS));
  wU32(context.extended_registers + 16, get_freg_ctl(FREG_CTL_FOO));
  wU16(context.extended_registers + 20, get_freg_ctl(FREG_CTL_FOS));
  wU32(context.extended_registers + 24, get_freg_ctl(FREG_CTL_MXCSR));

  for (size_t n = 0; n < 8; n++) {
    const void *st = get_freg(static_cast<freg_name_t>(FREG_ST0 + n));
    if (st != NULL) {
      memcpy(context.extended_registers + 32 + 16 * n, st, 10);
    }
  }

  for (size_t n = 0; n < 8; n++) {
    const void *xmm = get_freg(static_cast<freg_name_t>(FREG_XMM0 + n));
    if (xmm != NULL) {
      memcpy(context.extended_registers + 160 + 16 * n, xmm, 16);
    }
  }
}

void ThreadContext::getx87(struct save87 &s87) const {
  if (is_x87) {
    memcpy(&s87, &fregs, sizeof(struct save87));
    return;
  }
  memset(&s87, 0, sizeof(s87));
  s87.sv_env.en_cw = get_freg_ctl(FREG_CTL_CW);
  s87.sv_env.en_sw = get_freg_ctl(FREG_CTL_SW);
  s87.sv_env.en_tw = get_freg_ctl(FREG_CTL_TW);
  s87.sv_env.en_fip = get_freg_ctl(FREG_CTL_FIP);
  s87.sv_env.en_fcs = get_freg_ctl(FREG_CTL_FCS);
  s87.sv_env.en_opcode = get_freg_ctl(FREG_CTL_FOP);
  s87.sv_env.en_foo = get_freg_ctl(FREG_CTL_FOO);
  s87.sv_env.en_fos = get_freg_ctl(FREG_CTL_FOS);
  for (size_t n = 0; n < 8; n++) {
    memcpy(&s87.sv_ac[n],
           get_freg(static_cast<freg_name_t>(FREG_ST0 + n)),
           sizeof(s87.sv_ac[n]));
  }
}

#elif defined(__x86_64__)
ThreadContext::ThreadContext(const MDRawContextAMD64 &context) {
   clear();

   set_greg(GREG_RAX, context.rax);
   set_greg(GREG_RBX, context.rbx);
   set_greg(GREG_RCX, context.rcx);
   set_greg(GREG_RDX, context.rdx);
   set_greg(GREG_RSI, context.rsi);
   set_greg(GREG_RDI, context.rdi);
   set_greg(GREG_RBP, context.rbp);
   set_greg(GREG_RSP, context.rsp);
   set_greg(GREG_R8, context.r8);
   set_greg(GREG_R9, context.r9);
   set_greg(GREG_R10, context.r10);
   set_greg(GREG_R11, context.r11);
   set_greg(GREG_R12, context.r12);
   set_greg(GREG_R13, context.r13);
   set_greg(GREG_R14, context.r14);
   set_greg(GREG_R15, context.r15);
   set_greg(GREG_RIP, context.rip);
   set_greg(GREG_RFLAGS, context.eflags);
   set_greg(GREG_CS, context.cs);
   set_greg(GREG_DS, context.ds);
   set_greg(GREG_ES, context.es);
   set_greg(GREG_FS, context.fs);
   set_greg(GREG_GS, context.gs);
   set_greg(GREG_SS, context.ss);

#if defined(__DragonFly__)
   // It's always the SSE version.
   is_x87 = false;
#endif

   set_freg_ctl(FREG_CTL_CW, context.flt_save.control_word);
   set_freg_ctl(FREG_CTL_SW, context.flt_save.status_word);
   set_freg_ctl(FREG_CTL_TW, context.flt_save.tag_word);
   set_freg_ctl(FREG_CTL_FOP, context.flt_save.error_opcode);
   set_freg_ctl(FREG_CTL_RIP,
                *reinterpret_cast<const uint64_t*>
                (&context.flt_save.error_offset));
   set_freg_ctl(FREG_CTL_RDP,
                *reinterpret_cast<const uint64_t*>
                (&context.flt_save.data_offset));
   set_freg_ctl(FREG_CTL_MXCSR, context.flt_save.mx_csr);
   set_freg_ctl(FREG_CTL_MXCSR_MASK, context.flt_save.mx_csr_mask);

   for (size_t n = 0; n < 8; n++) {
     set_freg(static_cast<freg_name_t>(FREG_ST0 + n),
              &context.flt_save.float_registers[n]);
   }
   for (size_t n = 0; n < 16; n++) {
     set_freg(static_cast<freg_name_t>(FREG_XMM0 + n),
              &context.flt_save.xmm_registers[n]);
   }
}

void ThreadContext::FillRawContext(MDRawContextAMD64 &context) const {
  memset(&context, 0, sizeof(context));

  context.context_flags = MD_CONTEXT_AMD64_FULL | MD_CONTEXT_AMD64_SEGMENTS;

  context.cs = get_greg(GREG_CS);

  context.ds = get_greg(GREG_DS);
  context.es = get_greg(GREG_ES);
  context.fs = get_greg(GREG_FS);
  context.gs = get_greg(GREG_GS);

  context.ss = get_greg(GREG_SS);
  context.eflags = get_greg(GREG_RFLAGS);

  context.dr0 = get_dreg(0);
  context.dr1 = get_dreg(1);
  context.dr2 = get_dreg(2);
  context.dr3 = get_dreg(3);
  // 4 and 5 deliberatly omitted because they aren't included in the minidump
  // format.
  context.dr6 = get_dreg(6);
  context.dr7 = get_dreg(7);

  context.rax = get_greg(GREG_RAX);
  context.rcx = get_greg(GREG_RCX);
  context.rdx = get_greg(GREG_RDX);
  context.rbx = get_greg(GREG_RBX);

  context.rsp = get_greg(GREG_RSP);
  context.rbp = get_greg(GREG_RBP);
  context.rsi = get_greg(GREG_RSI);
  context.rdi = get_greg(GREG_RDI);
  context.r8 = get_greg(GREG_R8);
  context.r9 = get_greg(GREG_R9);
  context.r10 = get_greg(GREG_R10);
  context.r11 = get_greg(GREG_R11);
  context.r12 = get_greg(GREG_R12);
  context.r13 = get_greg(GREG_R13);
  context.r14 = get_greg(GREG_R14);
  context.r15 = get_greg(GREG_R15);

  context.rip = get_greg(GREG_RIP);

  context.flt_save.control_word = get_freg_ctl(FREG_CTL_CW);
  context.flt_save.status_word = get_freg_ctl(FREG_CTL_SW);
  context.flt_save.tag_word = get_freg_ctl(FREG_CTL_TW);
  // ???
  *reinterpret_cast<uint64_t*>(&context.flt_save.error_offset) =
      get_freg_ctl(FREG_CTL_RIP);
  // ???
  *reinterpret_cast<uint64_t*>(&context.flt_save.data_offset) =
      get_freg_ctl(FREG_CTL_RDP);
  context.flt_save.mx_csr = get_freg_ctl(FREG_CTL_MXCSR);
  context.flt_save.mx_csr_mask = get_freg_ctl(FREG_CTL_MXCSR_MASK);

  for (size_t n = 0; n < 8; n++) {
    const void *st = get_freg(static_cast<freg_name_t>(FREG_ST0 + n));
    if (st != NULL) {
      memcpy(&context.flt_save.float_registers[n], st, 10);
    }
  }

  for (size_t n = 0; n < 16; n++) {
    const void *xmm = get_freg(static_cast<freg_name_t>(FREG_XMM0 + n));
    if (xmm != NULL) {
      memcpy(&context.flt_save.xmm_registers[n], xmm, 16);
    }
  }
}
#else
#error "Architecture not supported."
#endif

void ThreadContext::clear() {
  memset(&gregs, 0, sizeof(gregs));
  memset(&fregs, 0, sizeof(fregs));
#if defined(__FreeBSD__) || defined(__DragonFly__)
  memset(&dregs, 0, sizeof(dregs));
#endif
#if defined(__i386__)
  is_x87 = false;
#endif
}
unsigned long ThreadContext::get_greg(greg_name_t name) const {
  switch (name) {
#if defined(__i386__)
    GET_GREG_CASE(EAX, eax);
    GET_GREG_CASE(EBX, ebx);
    GET_GREG_CASE(ECX, ecx);
    GET_GREG_CASE(EDX, edx);
    GET_GREG_CASE(ESI, esi);
    GET_GREG_CASE(EDI, edi);
    GET_GREG_CASE(EBP, ebp);
    GET_GREG_CASE(ESP, esp);
    GET_GREG_CASE(EIP, eip);
    GET_GREG_CASE(EFLAGS, eflags);
#elif defined(__x86_64__)
    GET_GREG_CASE(RAX, rax);
    GET_GREG_CASE(RBX, rbx);
    GET_GREG_CASE(RCX, rcx);
    GET_GREG_CASE(RDX, rdx);
    GET_GREG_CASE(RSI, rsi);
    GET_GREG_CASE(RDI, rdi);
    GET_GREG_CASE(RBP, rbp);
    GET_GREG_CASE(RSP, rsp);
    GET_GREG_CASE(R8, r8);
    GET_GREG_CASE(R9, r9);
    GET_GREG_CASE(R10, r10);
    GET_GREG_CASE(R11, r11);
    GET_GREG_CASE(R12, r12);
    GET_GREG_CASE(R13, r13);
    GET_GREG_CASE(R14, r14);
    GET_GREG_CASE(R15, r15);
    GET_GREG_CASE(RIP, rip);
    GET_GREG_CASE(RFLAGS, rflags);
#else
#error "This file is only for x86 and x86-64 architectures."
#endif
    GET_GREG_CASE(CS, cs);
    GET_GREG_CASE(SS, ss);
#if defined(BSD_NO_DS_SEGMENT)
    GET_GREG_CASE(DS, ss);
#else
    GET_GREG_CASE(DS, ds);
#endif
#if defined(BSD_NO_ES_SEGMENT)
    GET_GREG_CASE(ES, ss);
#else
    GET_GREG_CASE(ES, es);
#endif
#if defined(BSD_NO_FS_SEGMENT)
    GET_GREG_CASE_FIXED(FS, 0);
#else
    GET_GREG_CASE(FS, fs);
#endif
#if defined(BSD_NO_GS_SEGMENT)
    GET_GREG_CASE_FIXED(GS, 0);
#else
    GET_GREG_CASE(GS, gs);
#endif
  }
  return 0;
}

const unsigned long *ThreadContext::get_greg_p(greg_name_t name) const {
  switch (name) {
#if defined(__i386__)
    GETP_GREG(EAX, eax);
    GETP_GREG(EBX, ebx);
    GETP_GREG(ECX, ecx);
    GETP_GREG(EDX, edx);
    GETP_GREG(ESI, esi);
    GETP_GREG(EDI, edi);
    GETP_GREG(EBP, ebp);
    GETP_GREG(ESP, esp);
    GETP_GREG(EIP, eip);
    GETP_GREG(EFLAGS, eflags);
#elif defined(__x86_64__)
    GETP_GREG(RAX, rax);
    GETP_GREG(RBX, rbx);
    GETP_GREG(RCX, rcx);
    GETP_GREG(RDX, rdx);
    GETP_GREG(RSI, rsi);
    GETP_GREG(RDI, rdi);
    GETP_GREG(RBP, rbp);
    GETP_GREG(RSP, rsp);
    GETP_GREG(R8, r8);
    GETP_GREG(R9, r9);
    GETP_GREG(R10, r10);
    GETP_GREG(R11, r11);
    GETP_GREG(R12, r12);
    GETP_GREG(R13, r13);
    GETP_GREG(R14, r14);
    GETP_GREG(R15, r15);
    GETP_GREG(RIP, rip);
    GETP_GREG(RFLAGS, rflags);
#else
#error "This file is only for x86 and x86-64 architectures."
#endif
    GETP_GREG(CS, cs);
    GETP_GREG(SS, ss);
#if defined(BSD_NO_DS_SEGMENT)
    GETP_GREG(DS, ss);
#else
    GETP_GREG(DS, ds);
#endif
#if defined(BSD_NO_ES_SEGMENT)
    GETP_GREG(ES, ss);
#else
    GETP_GREG(ES, es);
#endif
#if defined(BSD_NO_FS_SEGMENT)
    GETP_CASE_FIXED(FS, 0);
#else
    GETP_GREG(FS, fs);
#endif
#if defined(BSD_NO_GS_SEGMENT)
    GETP_CASE_FIXED(GS, 0);
#else
    GETP_GREG(GS, gs);
#endif
  }
  return 0;
}

bool ThreadContext::set_greg(greg_name_t name, unsigned long value) {
  switch (name) {
#if defined(__i386__)
    SET_GREG_CASE(EAX, eax, value);
    SET_GREG_CASE(EBX, ebx, value);
    SET_GREG_CASE(ECX, ecx, value);
    SET_GREG_CASE(EDX, edx, value);
    SET_GREG_CASE(ESI, esi, value);
    SET_GREG_CASE(EDI, edi, value);
    SET_GREG_CASE(EBP, ebp, value);
    SET_GREG_CASE(ESP, esp, value);
    SET_GREG_CASE(EIP, eip, value);
    SET_GREG_CASE(EFLAGS, eflags, value);
#elif defined(__x86_64__)
    SET_GREG_CASE(RAX, rax, value);
    SET_GREG_CASE(RBX, rbx, value);
    SET_GREG_CASE(RCX, rcx, value);
    SET_GREG_CASE(RDX, rdx, value);
    SET_GREG_CASE(RSI, rsi, value);
    SET_GREG_CASE(RDI, rdi, value);
    SET_GREG_CASE(RBP, rbp, value);
    SET_GREG_CASE(RSP, rsp, value);
    SET_GREG_CASE(R8, r8, value);
    SET_GREG_CASE(R9, r9, value);
    SET_GREG_CASE(R10, r10, value);
    SET_GREG_CASE(R11, r11, value);
    SET_GREG_CASE(R12, r12, value);
    SET_GREG_CASE(R13, r13, value);
    SET_GREG_CASE(R14, r14, value);
    SET_GREG_CASE(R15, r15, value);
    SET_GREG_CASE(RIP, rip, value);
    SET_GREG_CASE(RFLAGS, rflags, value);
#else
#error "This file is only for x86 and x86-64 architectures."
#endif
    SET_GREG_CASE(CS, cs, value);
    SET_GREG_CASE(SS, ss, value);
#if defined(BSD_NO_DS_SEGMENT)
    SET_GREG_CASE_FIXED(DS);
#else
    SET_GREG_CASE(DS, ds, value);
#endif
#if defined(BSD_NO_ES_SEGMENT)
    SET_GREG_CASE_FIXED(ES);
#else
    SET_GREG_CASE(ES, es, value);
#endif
#if defined(BSD_NO_FS_SEGMENT)
    SET_GREG_CASE_FIXED(FS);
#else
    SET_GREG_CASE(FS, fs, value);
#endif
#if defined(BSD_NO_GS_SEGMENT)
    SET_GREG_CASE_FIXED(GS);
#else
    SET_GREG_CASE(GS, gs, value);
#endif
  }
  return false;
}

unsigned long ThreadContext::get_dreg(size_t index) const {
  switch (index) {
    GET_DREG_CASE(0);
    GET_DREG_CASE(1);
    GET_DREG_CASE(2);
    GET_DREG_CASE(3);
    GET_DREG_CASE(4);
    GET_DREG_CASE(5);
    GET_DREG_CASE(6);
    GET_DREG_CASE(7);
#if defined(__x86_64__)
    GET_DREG_CASE(8);
    GET_DREG_CASE(9);
    GET_DREG_CASE(10);
    GET_DREG_CASE(11);
    GET_DREG_CASE(12);
    GET_DREG_CASE(13);
    GET_DREG_CASE(14);
    GET_DREG_CASE(15);
#endif
  }
  return 0;
}

const unsigned long *ThreadContext::get_dreg_p(size_t index) const {
  switch (index) {
    GETP_DREG_CASE(0);
    GETP_DREG_CASE(1);
    GETP_DREG_CASE(2);
    GETP_DREG_CASE(3);
    GETP_DREG_CASE(4);
    GETP_DREG_CASE(5);
    GETP_DREG_CASE(6);
    GETP_DREG_CASE(7);
#if defined(__x86_64__)
    GETP_DREG_CASE(8);
    GETP_DREG_CASE(9);
    GETP_DREG_CASE(10);
    GETP_DREG_CASE(11);
    GETP_DREG_CASE(12);
    GETP_DREG_CASE(13);
    GETP_DREG_CASE(14);
    GETP_DREG_CASE(15);
#endif
  }
  return 0;
}

bool ThreadContext::set_dreg(size_t index, unsigned long value) {
  switch (index) {
    SET_DREG_CASE(0, value);
    SET_DREG_CASE(1, value);
    SET_DREG_CASE(2, value);
    SET_DREG_CASE(3, value);
    SET_DREG_CASE(4, value);
    SET_DREG_CASE(5, value);
    SET_DREG_CASE(6, value);
    SET_DREG_CASE(7, value);
#if defined(__x86_64__)
    SET_DREG_CASE(8, value);
    SET_DREG_CASE(9, value);
    SET_DREG_CASE(10, value);
    SET_DREG_CASE(11, value);
    SET_DREG_CASE(12, value);
    SET_DREG_CASE(13, value);
    SET_DREG_CASE(14, value);
    SET_DREG_CASE(15, value);
#endif
  }
  return false;
}

#if defined(FP_ENV_CW)
#define GET_FP_ENV_CW        FP_ENV_CW
#define SET_FP_ENV_CW(value) FP_ENV_CW = (value); return true
#endif

#if defined(FP_ENV_SW)
#define GET_FP_ENV_SW        FP_ENV_SW
#define SET_FP_ENV_SW(value) FP_ENV_SW = (value); return true
#endif

#if defined(FP_ENV_TW)
#define GET_FP_ENV_TW        FP_ENV_TW
#define SET_FP_ENV_TW(value) FP_ENV_TW = (value); return true
#endif

#if defined(FP_ENV_FOP)
#define GET_FP_ENV_FOP        FP_ENV_FOP
#define SET_FP_ENV_FOP(value) FP_ENV_FOP = (value); return true
#endif

#if defined(FP_ENV_MXCSR)
#define GET_FP_ENV_MXCSR        FP_ENV_MXCSR
#define SET_FP_ENV_MXCSR(value) FP_ENV_MXCSR = (value); return true
#endif

#if defined(__i386__)
#if defined(FP_ENV_FIP)
#define GET_FP_ENV_FIP        FP_ENV_FIP
#define SET_FP_ENV_FIP(value) FP_ENV_FIP = (value); return true
#endif

#if defined(FP_ENV_FCS)
#define GET_FP_ENV_FCS        FP_ENV_FCS
#define SET_FP_ENV_FCS(value) FP_ENV_FCS = (value); return true
#endif

#if defined(FP_ENV_FOO)
#define GET_FP_ENV_FOO        FP_ENV_FOO
#define SET_FP_ENV_FOO(value) FP_ENV_FOO = (value); return true
#endif

#if defined(FP_ENV_FOS)
#define GET_FP_ENV_FOS        FP_ENV_FOS
#define SET_FP_ENV_FOS(value) FP_ENV_FOS = (value); return true
#endif
#elif defined(__x86_64__)
#if defined(FP_ENV_RIP)
#define GET_FP_ENV_RIP        FP_ENV_RIP
#define SET_FP_ENV_RIP(value) FP_ENV_RIP = (value); return true
#endif

#if defined(FP_ENV_RDP)
#define GET_FP_ENV_RDP        FP_ENV_RDP
#define SET_FP_ENV_RDP(value) FP_ENV_RDP = (value); return true
#endif

#if defined(FP_ENV_MXCSR_MASK)
#define GET_FP_ENV_MXCSR_MASK        FP_ENV_MXCSR_MASK
#define SET_FP_ENV_MXCSR_MASK(value) FP_ENV_MXCSR_MASK = (value); return true
#endif
#else
#error "This file is only for x86 and x86-64 architectures."
#endif

uint64_t ThreadContext::get_freg_ctl(freg_ctl_name_t name) const {
  switch (name) {
    case FREG_CTL_CW:         return GET_FP_ENV_CW;
    case FREG_CTL_SW:         return GET_FP_ENV_SW;
    case FREG_CTL_TW:         return GET_FP_ENV_TW;
    case FREG_CTL_FOP:        return GET_FP_ENV_FOP;
    case FREG_CTL_MXCSR:      return GET_FP_ENV_MXCSR;
#if defined(__i386__)
    case FREG_CTL_FCS:        return GET_FP_ENV_FCS;
    case FREG_CTL_FIP:        return GET_FP_ENV_FIP;
    case FREG_CTL_FOS:        return GET_FP_ENV_FOS;
    case FREG_CTL_FOO:        return GET_FP_ENV_FOO;
#elif defined(__x86_64__)
    case FREG_CTL_RIP:        return GET_FP_ENV_RIP;
    case FREG_CTL_RDP:        return GET_FP_ENV_RDP;
    case FREG_CTL_MXCSR_MASK: return GET_FP_ENV_MXCSR_MASK;
#else
#error "This file is only for x86 and x86-64 architectures."
#endif
  }
  return 0;
}

bool ThreadContext::set_freg_ctl(freg_ctl_name_t name, uint64_t value) {
  switch (name) {
    case FREG_CTL_CW:         SET_FP_ENV_CW(value);
    case FREG_CTL_SW:         SET_FP_ENV_SW(value);
    case FREG_CTL_TW:         SET_FP_ENV_TW(value);
    case FREG_CTL_FOP:        SET_FP_ENV_FOP(value);
    case FREG_CTL_MXCSR:      SET_FP_ENV_MXCSR(value);
#if defined(__i386__)
    case FREG_CTL_FCS:        SET_FP_ENV_FCS(value);
    case FREG_CTL_FIP:        SET_FP_ENV_FIP(value);
    case FREG_CTL_FOS:        SET_FP_ENV_FOS(value);
    case FREG_CTL_FOO:        SET_FP_ENV_FOO(value);
#elif defined(__x86_64__)
    case FREG_CTL_RIP:        SET_FP_ENV_RIP(value);
    case FREG_CTL_RDP:        SET_FP_ENV_RDP(value);
    case FREG_CTL_MXCSR_MASK: SET_FP_ENV_MXCSR_MASK(value);
#else
#error "This file is only for x86 and x86-64 architectures."
#endif
  }
  return false;
}

const void *ThreadContext::get_freg(freg_name_t name) const {
#define DO_ST(n) \
   case FREG_ST##n: return FP_REG_ST(n)
#define DO_XMM(n) \
   case FREG_XMM##n: return FP_REG_XMM(n)
  switch (name) {
    DO_ST(0); DO_ST(1); DO_ST(2); DO_ST(3);
    DO_ST(4); DO_ST(5); DO_ST(6); DO_ST(7);
    DO_XMM(0); DO_XMM(1); DO_XMM(2); DO_XMM(3);
    DO_XMM(4); DO_XMM(5); DO_XMM(6); DO_XMM(7);
#if defined(__x86_64__)
    DO_XMM(8); DO_XMM(9); DO_XMM(10); DO_XMM(11);
    DO_XMM(12); DO_XMM(13); DO_XMM(14); DO_XMM(15);
#endif
  }
#undef DO_XMM
#undef DO_ST
  return NULL;
}

bool ThreadContext::set_freg(freg_name_t name, void const *value) {
#define DO_ST(n) \
   case FREG_ST##n: memcpy(FP_REG_ST(n), value, FP_REG_ST_LEN); return true
#define DO_XMM(n) \
   case FREG_XMM##n: memcpy(FP_REG_XMM(n), value, FP_REG_XMM_LEN); return true

  switch (name) {
    DO_ST(0); DO_ST(1); DO_ST(2); DO_ST(3);
    DO_ST(4); DO_ST(5); DO_ST(6); DO_ST(7);
    DO_XMM(0); DO_XMM(1); DO_XMM(2); DO_XMM(3);
    DO_XMM(4); DO_XMM(5); DO_XMM(6); DO_XMM(7);
#if defined(__x86_64__)
    DO_XMM(8); DO_XMM(9); DO_XMM(10); DO_XMM(11);
    DO_XMM(12); DO_XMM(13); DO_XMM(14); DO_XMM(15);
#endif
  }
#undef DO_XMM
#undef DO_ST
  return false;
}

unsigned long ThreadContext::get_pc() const {
#if defined(__i386__)
  return get_greg(GREG_EIP);
#elif defined(__x86_64__)
  return get_greg(GREG_RIP);
#else
#error "This file is only for x86 and x86-64 architectures."
#endif
}

const unsigned long *ThreadContext::get_pc_p() const {
#if defined(__i386__)
  return get_greg_p(GREG_EIP);
#elif defined(__x86_64__)
  return get_greg_p(GREG_RIP);
#else
#error "This file is only for x86 and x86-64 architectures."
#endif
}

void ThreadContext::set_pc(unsigned long value) {
#if defined(__i386__)
  set_greg(GREG_EIP, value);
#elif defined(__x86_64__)
  set_greg(GREG_RIP, value);
#else
#error "This file is only for x86 and x86-64 architectures."
#endif
}

unsigned long ThreadContext::get_sp() const {
#if defined(__i386__)
  return get_greg(GREG_ESP);
#elif defined(__x86_64__)
  return get_greg(GREG_RSP);
#else
#error "This file is only for x86 and x86-64 architectures."
#endif
}

const unsigned long *ThreadContext::get_sp_p() const {
#if defined(__i386__)
  return get_greg_p(GREG_ESP);
#elif defined(__x86_64__)
  return get_greg_p(GREG_RSP);
#else
#error "This file is only for x86 and x86-64 architectures."
#endif
}

void ThreadContext::set_sp(unsigned long value) {
#if defined(__i386__)
  set_greg(GREG_ESP, value);
#elif defined(__x86_64__)
  set_greg(GREG_RSP, value);
#else
#error "This file is only for x86 and x86-64 architectures."
#endif
}

} }
