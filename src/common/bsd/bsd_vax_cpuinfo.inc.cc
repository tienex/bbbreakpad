// Copyright (c) 2015, Orlando Bassotto.
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are
// met:
//
//     * Redistributions of source code must retain the above copyright
// notice, this list of conditions and the following disclaimer.
//     * Redistributions in binary form must reproduce the above
// copyright notice, this list of conditions and the following disclaimer
// in the documentation and/or other materials provided with the
// distribution.
//     * Neither the name of Google Inc. nor the names of its
// contributors may be used to endorse or promote products derived from
// this software without specific prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
// "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
// LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
// A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
// OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
// SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
// LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
// DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
// THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
// (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
// OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

// This source file provides replacements for libc functions that we need. If
// we call the libc functions directly we risk crashing in the dynamic linker
// as it tries to resolve uncached PLT entries.

#include <ctype.h>

#include <iomanip>
#include <sstream>

//
// vax:
//
// processor\t: 3
// ...
// model\t\t: MicroVAX 3800
//
namespace {

static char *GetHardwareModel() {
  int mib[] = { CTL_HW, HW_MODEL };
  size_t size;
  if (sysctl(mib, 2, NULL, &size, NULL, 0) < 0)
    return NULL;

  char *model = reinterpret_cast<char*>(calloc(1, size + 1));
  if (model == NULL)
    return NULL;

  if (sysctl(mib, 2, model, &size, NULL, 0) < 0) {
    free(model);
    model = NULL;
  }

  return model;
}

}

namespace google_breakpad { namespace bsd {

static char *MakeLinuxCpuInfo(size_t *size) {
  std::stringstream ss;

  size_t ncpus = GetProcessorCount();
  for (size_t n = 0; n < ncpus; n++) {
    ss << "processor\t: " << n << std::endl << std::endl;
  }

  ss << "model\t\t: ";

  char *model = GetHardwareModel();
  if (model == NULL) {
    ss << "Unknown";
  } else {
    ss << model;
    free(model);
  }
  ss << std::endl;

  std::string str(ss.str());

  char *buf = reinterpret_cast<char*>(calloc(1, str.size() + 1));
  if (buf == NULL)
    return NULL;

  strlcpy(buf, str.c_str(), str.size());
  *size = str.size();
  return buf;
}

} }
