// Copyright (c) 2015, Orlando Bassotto.
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are
// met:
//
//     * Redistributions of source code must retain the above copyright
// notice, this list of conditions and the following disclaimer.
//     * Redistributions in binary form must reproduce the above
// copyright notice, this list of conditions and the following disclaimer
// in the documentation and/or other materials provided with the
// distribution.
//     * Neither the name of Google Inc. nor the names of its
// contributors may be used to endorse or promote products derived from
// this software without specific prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
// "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
// LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
// A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
// OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
// SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
// LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
// DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
// THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
// (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
// OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

namespace google_breakpad { namespace bsd {

#define GET_GREG_CASE(name, field) \
  case GREG_##name: return gregs.field
#define GETP_GREG_CASE(name, field) \
  case GREG_##name: \
     return reinterpret_cast<const unsigned long*>(&gregs.field)
#define SET_GREG_CASE(name, field, value) \
  case GREG_##name: gregs.field = (value); return true

ThreadContext::ThreadContext(const MDRawContextVAX &context) {
  clear();

  set_greg(GREG_R0, context.iregs[0]);
  set_greg(GREG_R1, context.iregs[1]);
  set_greg(GREG_R2, context.iregs[2]);
  set_greg(GREG_R3, context.iregs[3]);
  set_greg(GREG_R4, context.iregs[4]);
  set_greg(GREG_R5, context.iregs[5]);
  set_greg(GREG_R6, context.iregs[6]);
  set_greg(GREG_R7, context.iregs[7]);
  set_greg(GREG_R8, context.iregs[8]);
  set_greg(GREG_R9, context.iregs[9]);
  set_greg(GREG_R10, context.iregs[10]);
  set_greg(GREG_R11, context.iregs[11]);
  set_greg(GREG_AP, context.iregs[MD_CONTEXT_VAX_REG_AP]);
  set_greg(GREG_FP, context.iregs[MD_CONTEXT_VAX_REG_SP]);
  set_greg(GREG_SP, context.iregs[MD_CONTEXT_VAX_REG_FP]);
  set_greg(GREG_PC, context.iregs[MD_CONTEXT_VAX_REG_PC]);
  set_greg(GREG_PSL, context.psl);
}

ThreadContext::ThreadContext(const ucontext_t &uc) {
  clear();

#define GREG_COPY(reg, in) gregs.reg = uc.uc_mcontext.__gregs[_REG_##in]
  GREG_COPY(r0, R0);
  GREG_COPY(r1, R1);
  GREG_COPY(r2, R2);
  GREG_COPY(r3, R3);
  GREG_COPY(r4, R4);
  GREG_COPY(r5, R5);
  GREG_COPY(r6, R6);
  GREG_COPY(r7, R7);
  GREG_COPY(r8, R8);
  GREG_COPY(r9, R9);
  GREG_COPY(r10, R10);
  GREG_COPY(r11, R11);
  GREG_COPY(ap, AP);
  GREG_COPY(sp, SP);
  GREG_COPY(fp, FP);
  GREG_COPY(pc, PC);
  GREG_COPY(psl, PSL);
}

void ThreadContext::FillRawContext(MDRawContextVAX &context) const {
  memset(&context, 0, sizeof(context));

  context.context_flags = MD_CONTEXT_VAX_FULL;

  for (size_t n = 0; n < MD_CONTEXT_VAX_GPR_COUNT; n++)
    context.iregs[n] = get_greg(static_cast<greg_name_t>(GREG_R0 + n));

  context.psl = get_greg(GREG_PSL);
}

void ThreadContext::clear() {
  memset(&gregs, 0, sizeof(gregs));
}

unsigned long ThreadContext::get_greg(greg_name_t name) const {
  switch (name) {
    GET_GREG_CASE(R0, r0);
    GET_GREG_CASE(R1, r1);
    GET_GREG_CASE(R2, r2);
    GET_GREG_CASE(R3, r3);
    GET_GREG_CASE(R4, r4);
    GET_GREG_CASE(R5, r5);
    GET_GREG_CASE(R6, r6);
    GET_GREG_CASE(R7, r7);
    GET_GREG_CASE(R8, r8);
    GET_GREG_CASE(R9, r9);
    GET_GREG_CASE(R10, r10);
    GET_GREG_CASE(R11, r11);
    GET_GREG_CASE(AP, ap);
    GET_GREG_CASE(FP, fp);
    GET_GREG_CASE(SP, sp);
    GET_GREG_CASE(PC, pc);
    GET_GREG_CASE(PSL, psl);
  }
  return 0;
}

const unsigned long *ThreadContext::get_greg_p(greg_name_t name) const {
  switch (name) {
    GETP_GREG_CASE(R0, r0);
    GETP_GREG_CASE(R1, r1);
    GETP_GREG_CASE(R2, r2);
    GETP_GREG_CASE(R3, r3);
    GETP_GREG_CASE(R4, r4);
    GETP_GREG_CASE(R5, r5);
    GETP_GREG_CASE(R6, r6);
    GETP_GREG_CASE(R7, r7);
    GETP_GREG_CASE(R8, r8);
    GETP_GREG_CASE(R9, r9);
    GETP_GREG_CASE(R10, r10);
    GETP_GREG_CASE(R11, r11);
    GETP_GREG_CASE(AP, ap);
    GETP_GREG_CASE(FP, fp);
    GETP_GREG_CASE(SP, sp);
    GETP_GREG_CASE(PC, pc);
    GETP_GREG_CASE(PSL, psl);
  }
  return 0;
}

bool ThreadContext::set_greg(greg_name_t name, unsigned long value) {
  switch (name) {
    SET_GREG_CASE(R0, r0, value);
    SET_GREG_CASE(R1, r1, value);
    SET_GREG_CASE(R2, r2, value);
    SET_GREG_CASE(R3, r3, value);
    SET_GREG_CASE(R4, r4, value);
    SET_GREG_CASE(R5, r5, value);
    SET_GREG_CASE(R6, r6, value);
    SET_GREG_CASE(R7, r7, value);
    SET_GREG_CASE(R8, r8, value);
    SET_GREG_CASE(R9, r9, value);
    SET_GREG_CASE(R10, r10, value);
    SET_GREG_CASE(R11, r11, value);
    SET_GREG_CASE(AP, ap, value);
    SET_GREG_CASE(FP, fp, value);
    SET_GREG_CASE(SP, sp, value);
    SET_GREG_CASE(PC, pc, value);
    SET_GREG_CASE(PSL, psl, value);
  }
  return false;
}

uint64_t ThreadContext::get_freg_ctl(freg_ctl_name_t) const {
  return 0;
}

bool ThreadContext::set_freg_ctl(freg_ctl_name_t, uint64_t) {
  return false;
}

const void *ThreadContext::get_freg(freg_name_t) const {
  return NULL;
}

bool ThreadContext::set_freg(freg_name_t, void const *) {
  return false;
}

unsigned long ThreadContext::get_pc() const {
  return get_greg(GREG_PC);
}

const unsigned long *ThreadContext::get_pc_p() const {
  return get_greg_p(GREG_PC);
}

void ThreadContext::set_pc(unsigned long value) {
  set_greg(GREG_PC, value);
}

unsigned long ThreadContext::get_sp() const {
  return get_greg(GREG_SP);
}

const unsigned long *ThreadContext::get_sp_p() const {
  return get_greg_p(GREG_SP);
}

void ThreadContext::set_sp(unsigned long value) {
  set_greg(GREG_SP, value);
}

} }
