// Copyright (c) 2015, Orlando Bassotto.
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are
// met:
//
//     * Redistributions of source code must retain the above copyright
// notice, this list of conditions and the following disclaimer.
//     * Redistributions in binary form must reproduce the above
// copyright notice, this list of conditions and the following disclaimer
// in the documentation and/or other materials provided with the
// distribution.
//     * Neither the name of Google Inc. nor the names of its
// contributors may be used to endorse or promote products derived from
// this software without specific prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
// "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
// LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
// A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
// OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
// SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
// LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
// DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
// THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
// (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
// OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

//
// Code shared across NetBSD and OpenBSD.
//

namespace google_breakpad { namespace bsd {

static const size_t kCOMMLEN = MAXCOMLEN;

//
// System Target helper functions.
//

static int GetCorePatternType() {
  char buf[32];
  if (!GetCorePattern(buf, sizeof(buf)))
    return BSD_CORE_PATTERN_CUSTOM;

  if (strlen(buf) == 7 && memcmp(buf, "%n.core", 7) == 0)
    return BSD_CORE_PATTERN_NAMED;
  else if (strlen(buf) == 4 && memcmp(buf, "core", 4) == 0)
    return BSD_CORE_PATTERN_SIMPLE;
  else
    return BSD_CORE_PATTERN_CUSTOM;
}

//
// Process helper functions.
//

static char *ReadProcessArguments(pid_t pid, size_t *bufsize) {
  int mib[] = { CTL_KERN, KERN_PROC_ARGS, MIB_PID(pid), KERN_PROC_ARGV };

  size_t size;
  if (sysctl(mib, sizeof(mib)/sizeof(int), NULL, &size, NULL, 0) < 0)
    return NULL;

  char *buf = reinterpret_cast<char*>(calloc(1, size));
  if (buf == NULL)
    return NULL;

  if (sysctl(mib, sizeof(mib)/sizeof(int), buf, &size, NULL, 0) < 0) {
    free(buf);
    return NULL;
  }

  *bufsize = size;
  return buf;
}

static char *ReadProcessEnvironment(pid_t pid, size_t *bufsize) {
  int mib[] = { CTL_KERN, KERN_PROC_ARGS, MIB_PID(pid), KERN_PROC_ENV };

  size_t size;
  if (sysctl(mib, sizeof(mib)/sizeof(int), NULL, &size, NULL, 0) < 0)
    return NULL;

  char *buf = reinterpret_cast<char*>(calloc(1, size));
  if (buf == NULL)
    return NULL;

  if (sysctl(mib, sizeof(mib)/sizeof(int), buf, &size, NULL, 0) < 0) {
    free(buf);
    return NULL;
  }

  *bufsize = size;
  return buf;
}

bool GetProcessThreads(pid_t pid, std::vector<lwpid_t> &lwpids) {
  int mib[6], *mibp = mib;

  *mibp++ = CTL_KERN;
  *mibp++ = kKERN_LWP;
  if (kKERN_LWP_FLAG != 0) {
    *mibp++ = kKERN_LWP_FLAG;
  }
  *mibp++ = MIB_PID(pid);
  *mibp++ = sizeof(kinfo_lwp_t);
  *mibp   = 0;

  size_t size;
  if (sysctl(mib, (mibp - mib) + 1, 0, &size, NULL, 0) < 0)
    return false;

  kinfo_lwp_t *kl = reinterpret_cast<kinfo_lwp_t*>(calloc(1, size));
  if (kl == NULL)
    return false;

  size_t count = size / sizeof(kinfo_lwp_t);
  *mibp = count;

  if (sysctl(mib, (mibp - mib) + 1, kl, &size, NULL, 0) < 0) {
    free(kl);
    return false;
  }
  
  lwpids.clear();
  count = size / sizeof(kinfo_lwp_t);
  for (size_t n = 0; n < count; n++) {
    lwpid_t lwpid = KinfoLwpGetId(&kl[n]);
    if (lwpid != -1) {
      lwpids.push_back(lwpid);
    }
  }
  free(kl);

  return true;
}

static bool GetProcessMappingsWithPmap(pid_t pid,
                                       std::vector<Mapping> &mappings) {
  char command_buf[128];
  if (pid == 0) {
    pid = getpid();
  }

  snprintf(command_buf, sizeof(command_buf), "%s -avp %d",
           kPMAP_EXECUTABLE_PATH, pid);
  FILE *fp = popen(command_buf, "r");
  if (fp == NULL)
    return false;

  // The format is
  // Start            End                 Size  Offset           rwxpc  RWX  I/W/A Dev     Inode - File
  // 0000000000001000-0000000000001000       0k 0000000000000000 ---s- (---) 0/0/0 00:00       0 -   [ anon ]

  char *line = NULL;
  size_t linesize = 0;
  ssize_t linelen;
  size_t lineno = 0;

  while ((linelen = getline(&line, &linesize, fp)) != -1) {
    // Skip the header
    if (lineno++ == 0)
      continue;

    unsigned long long start, end, offset;
    unsigned long size;
    char r, w, x, p;
    int namepos = 0;
    if (sscanf(line, "%llx-%llx %luk %llx %c%c%c%c%*c (%*c%*c%*c) %*u/%*u/%*u "
               "%*x:%*x %*u - %n", &start, &end, &size, &offset, &r, &w, &x, &p,
               &namepos) != 8)
      continue;

    Mapping m;
    memset(&m, 0, sizeof(m));

    m.start = start;
    m.end = (end <= start) ? start : (end + 1);
    m.offset = offset;
    m.protection = 0;
    if (x == 'x') {
      m.protection |= PROT_EXEC;
    }
    if (w == 'w') {
      m.protection |= PROT_WRITE;
    }
    if (r == 'r') {
      m.protection |= PROT_READ;
    }
    if (p != 's') {
      m.protection |= Mapping::PROT_PRIVATE;
    }
    if (line[namepos] == '/') {
      strlcpy(m.name, line + namepos, sizeof(m.name));
    }

    mappings.push_back(m);
  }

  if (line != NULL) {
    free(line);
  }

  pclose(fp);
  return true;
}

bool EnableProcessCoreDump(pid_t, bool) {
  return true;
}

bool GetProcessTimes(pid_t pid, ProcessTimes &times) {
  int mib[] = { CTL_KERN, kKERN_PROC, KERN_PROC_PID, MIB_PID(pid),
                sizeof(kinfo_proc_t), 1 };

  kinfo_proc_t kp;
  size_t size = sizeof(kp);
  if (sysctl(mib, sizeof(mib)/sizeof(mib[0]), &kp, &size, NULL, 0) < 0)
    return false;

  if (!kp.p_uvalid)
    return false;

  times.start.tv_sec   = kp.p_ustart_sec;
  times.start.tv_nsec  = kp.p_ustart_usec * 1000;
  times.user.tv_sec    = kp.p_uutime_sec;
  times.user.tv_nsec   = kp.p_uutime_usec * 1000;
  times.system.tv_sec  = kp.p_ustime_sec;
  times.system.tv_nsec = kp.p_ustime_usec * 1000;
  return true;
}

//
// Cloning thread helper functions.
//

bool NeedForkThreadStack() {
  return false;
}

pid_t ForkThread(int (*entry)(void *), void *arg, void *, size_t) {
  return ForkSplitThread(entry, arg);
}

pid_t WaitForkedThread(pid_t pid, int *status, int flags) {
  return WaitSplitForkedThread(pid, status, flags);
}

} }
