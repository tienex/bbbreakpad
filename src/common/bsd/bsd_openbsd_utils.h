// Copyright (c) 2015, Orlando Bassotto.
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are
// met:
//
//     * Redistributions of source code must retain the above copyright
// notice, this list of conditions and the following disclaimer.
//     * Redistributions in binary form must reproduce the above
// copyright notice, this list of conditions and the following disclaimer
// in the documentation and/or other materials provided with the
// distribution.
//     * Neither the name of Google Inc. nor the names of its
// contributors may be used to endorse or promote products derived from
// this software without specific prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
// "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
// LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
// A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
// OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
// SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
// LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
// DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
// THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
// (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
// OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

#ifndef CLIENT_BSD_BSD_OPENBSD_UTILS_H_
#define CLIENT_BSD_BSD_OPENBSD_UTILS_H_

extern "C" int getcontext(ucontext_t *);

namespace google_breakpad {

#define BSD_OSNAME "OpenBSD"
#define BSD_PLATFORM_ID MD_OS_OPENBSD

#define ELFOSABI_DEFAULT ELFOSABI_OPENBSD

#define AT_NULL AUX_null
#define AT_IGNORE AUX_ignore
#define AT_EXECFD AUX_execfd
#define AT_PHDR AUX_phdr
#define AT_PHENT AUX_phent
#define AT_PHNUM AUX_phnum
#define AT_PAGESZ AUX_pagesz
#define AT_BASE AUX_base
#define AT_FLAGS AUX_flags
#define AT_ENTRY AUX_entry
#define AT_EUID AUX_sun_uid
#define AT_RUID AUX_sun_ruid
#define AT_EGID AUX_sun_gid
#define AT_RGID AUX_sun_rgid

#define AT_COUNT ((AuxID)AT_RGID + 1)

#if defined(__i386__)
#define UCONTEXT_GPR_SP(uc)     ((uc)->sc_esp)
#define UCONTEXT_GPR_PC(uc)     ((uc)->sc_eip)
#elif defined(__x86_64__)
#define UCONTEXT_GPR_SP(uc)     ((uc)->sc_rsp)
#define UCONTEXT_GPR_PC(uc)     ((uc)->sc_rip)
#else
#error "Architecture not supported."
#endif

#define DEFAULT_TOP_OF_STACK_LOCATION \
    (VM_MAXUSER_ADDRESS - sizeof(google_breakpad::bsd::TopOfStack))

}

#endif  // CLIENT_BSD_BSD_OPENBSD_UTILS_H_
