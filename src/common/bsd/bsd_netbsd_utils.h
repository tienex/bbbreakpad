// Copyright (c) 2015, Orlando Bassotto.
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are
// met:
//
//     * Redistributions of source code must retain the above copyright
// notice, this list of conditions and the following disclaimer.
//     * Redistributions in binary form must reproduce the above
// copyright notice, this list of conditions and the following disclaimer
// in the documentation and/or other materials provided with the
// distribution.
//     * Neither the name of Google Inc. nor the names of its
// contributors may be used to endorse or promote products derived from
// this software without specific prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
// "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
// LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
// A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
// OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
// SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
// LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
// DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
// THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
// (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
// OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

#ifndef CLIENT_BSD_BSD_NETBSD_UTILS_H_
#define CLIENT_BSD_BSD_NETBSD_UTILS_H_

#include <sys/exec_elf.h>

namespace google_breakpad { namespace bsd {

#define BSD_OSNAME "NetBSD"
#define BSD_PLATFORM_ID MD_OS_NETBSD

#define ELFOSABI_DEFAULT ELFOSABI_NETBSD

#define AT_COUNT (AT_SUN_EXECNAME + 1)

#if defined(__i386__)
#define UCONTEXT_GPR_SP(uc) ((uc)->uc_mcontext.__gregs[_REG_ESP])
#define UCONTEXT_GPR_PC(uc) ((uc)->uc_mcontext.__gregs[_REG_EIP])
#elif defined(__x86_64__)
#define UCONTEXT_GPR_SP(uc) ((uc)->uc_mcontext.__gregs[_REG_RSP])
#define UCONTEXT_GPR_PC(uc) ((uc)->uc_mcontext.__gregs[_REG_RIP])
#elif defined(__vax__)
#define UCONTEXT_GPR_SP(uc) ((uc)->uc_mcontext.__gregs[_REG_SP])
#define UCONTEXT_GPR_PC(uc) ((uc)->uc_mcontext.__gregs[_REG_PC])
#else
#error "Architecture not supported."
#endif

#define DEFAULT_TOP_OF_STACK_LOCATION \
    (VM_MAXUSER_ADDRESS - sizeof(google_breakpad::bsd::TopOfStack))
#if defined(__i386__)
#define DEFAULT_TOP_OF_STACK_LOCATION64 \
    (0xffffefffU - sizeof(google_breakpad::bsd::TopOfStack))
#endif

} }

#endif  // CLIENT_BSD_BSD_NETBSD_UTILS_H_
