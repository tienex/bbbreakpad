// Copyright (c) 2015, Orlando Bassotto.
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are
// met:
//
//     * Redistributions of source code must retain the above copyright
// notice, this list of conditions and the following disclaimer.
//     * Redistributions in binary form must reproduce the above
// copyright notice, this list of conditions and the following disclaimer
// in the documentation and/or other materials provided with the
// distribution.
//     * Neither the name of Google Inc. nor the names of its
// contributors may be used to endorse or promote products derived from
// this software without specific prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
// "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
// LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
// A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
// OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
// SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
// LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
// DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
// THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
// (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
// OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

// This source file provides replacements for libc functions that we need. If
// we call the libc functions directly we risk crashing in the dynamic linker
// as it tries to resolve uncached PLT entries.

#ifndef CLIENT_BSD_BSD_THREAD_CONTEXT_H__
#define CLIENT_BSD_BSD_THREAD_CONTEXT_H__

#include <sys/types.h>
#include <sys/param.h>

#include "common/bsd/bsd_config.h"
#include "common/bsd/bsd_types.h"
#include "google_breakpad/common/minidump_format.h"

#if defined(BSD_HAVE_MACHINE_NPX_H)
#include <machine/npx.h>
#elif defined(BSD_HAVE_MACHINE_FPU_H)
#include <machine/fpu.h>
#endif
#include <machine/reg.h>
#if !defined(__OpenBSD__)
#include <ucontext.h>
#endif

namespace google_breakpad { namespace bsd {

struct ThreadContext {
  enum greg_name_t {
#if defined(__i386__) || defined(__x86_64__)
#if defined(__i386__)
    GREG_EAX,
    GREG_EBX,
    GREG_ECX,
    GREG_EDX,
    GREG_ESI,
    GREG_EDI,
    GREG_EBP,
    GREG_ESP,
    GREG_EIP,
    GREG_EFLAGS,
#else
    GREG_RAX,
    GREG_RBX,
    GREG_RCX,
    GREG_RDX,
    GREG_RSI,
    GREG_RDI,
    GREG_RBP,
    GREG_RSP,
    GREG_R8,
    GREG_R9,
    GREG_R10,
    GREG_R11,
    GREG_R12,
    GREG_R13,
    GREG_R14,
    GREG_R15,
    GREG_RIP,
    GREG_RFLAGS,
#endif
    GREG_CS,
    GREG_DS,
    GREG_ES,
    GREG_FS,
    GREG_GS,
    GREG_SS,
#elif defined(__vax__)
    GREG_R0,
    GREG_R1,
    GREG_R2,
    GREG_R3,
    GREG_R4,
    GREG_R5,
    GREG_R6,
    GREG_R7,
    GREG_R8,
    GREG_R9,
    GREG_R10,
    GREG_R11,
    GREG_AP,
    GREG_FP,
    GREG_SP,
    GREG_PC,
    GREG_PSL
#else
#error "Architecture not supported."
#endif
  };
  enum freg_ctl_name_t {
#if defined(__i386__) || defined(__x86_64__)
    FREG_CTL_CW,
    FREG_CTL_SW,
    FREG_CTL_TW,
    FREG_CTL_FOP,
#if defined(__i386__)
    FREG_CTL_FCS,
    FREG_CTL_FIP,
    FREG_CTL_FOS,
    FREG_CTL_FOO,
#else
    FREG_CTL_RIP,
    FREG_CTL_RDP,
#endif
    FREG_CTL_MXCSR,
#if defined(__x86_64__)
    FREG_CTL_MXCSR_MASK,
#endif
#else
    // Don't define any FREG_CTL!
#endif
  };
  enum freg_name_t {
#if defined(__i386__) || defined(__x86_64__)
    FREG_ST0,
    FREG_ST1,
    FREG_ST2,
    FREG_ST3,
    FREG_ST4,
    FREG_ST5,
    FREG_ST6,
    FREG_ST7,
    FREG_XMM0,
    FREG_XMM1,
    FREG_XMM2,
    FREG_XMM3,
    FREG_XMM4,
    FREG_XMM5,
    FREG_XMM6,
    FREG_XMM7,
#if defined(__x86_64__)
    FREG_XMM8,
    FREG_XMM9,
    FREG_XMM10,
    FREG_XMM11,
    FREG_XMM12,
    FREG_XMM13,
    FREG_XMM14,
    FREG_XMM15,
#endif
#else
    // Don't define any freg.
#endif
  };

  struct reg gregs;
#if defined(__i386__) || (defined(__x86_64__) && defined(__DragonFly__))
  union savefpu fregs;
  bool is_x87;
#elif defined(__x86_64__)
  struct savefpu fregs;
#elif defined(BSD_HAVE_MACHINE_FPU_H) || defined(BSD_HAVE_MACHINE_NPX_H)
  struct fpreg fregs;
#endif
#if (defined(__FreeBSD__) || defined(__DragonFly__)) && \
    (defined(__i386__) || defined(__x86_64__))
  struct dbreg dregs;
#endif

  ThreadContext();
#if defined(__i386__)
  ThreadContext(const MDRawContextX86 &context);
  void FillRawContext(MDRawContextX86 &context) const;
#elif defined(__x86_64__)
  ThreadContext(const MDRawContextAMD64 &context);
  void FillRawContext(MDRawContextAMD64 &context) const;
#elif defined(__vax__)
  ThreadContext(const MDRawContextVAX &context);
  void FillRawContext(MDRawContextVAX &context) const;
#else
#error "Architecture not supported."
#endif
  ThreadContext(const ucontext_t &uc);

  void clear();

  unsigned long get_greg(greg_name_t name) const;
  const unsigned long *get_greg_p(greg_name_t name) const;
  bool set_greg(greg_name_t name, unsigned long value);

#if defined(__i386__) || defined(__x86_64__)
  unsigned long get_dreg(size_t index) const;
  const unsigned long *get_dreg_p(size_t index) const;
  bool set_dreg(size_t index, unsigned long value);
#endif

  uint64_t get_freg_ctl(freg_ctl_name_t name) const;
  bool set_freg_ctl(freg_ctl_name_t name, uint64_t value);
  const void *get_freg(freg_name_t name) const;
  bool set_freg(freg_name_t name, const void *value);

#if defined(__i386__)
  void getx87(struct save87 &s87) const;
#endif

  unsigned long get_pc() const;
  const unsigned long *get_pc_p() const;
  void set_pc(unsigned long value);

  unsigned long get_sp() const;
  const unsigned long *get_sp_p() const;
  void set_sp(unsigned long value);
};

} }

#endif  // !CLIENT_BSD_BSD_THREAD_CONTEXT_H__
