// Copyright (c) 2015, Orlando Bassotto.
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are
// met:
//
//     * Redistributions of source code must retain the above copyright
// notice, this list of conditions and the following disclaimer.
//     * Redistributions in binary form must reproduce the above
// copyright notice, this list of conditions and the following disclaimer
// in the documentation and/or other materials provided with the
// distribution.
//     * Neither the name of Google Inc. nor the names of its
// contributors may be used to endorse or promote products derived from
// this software without specific prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
// "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
// LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
// A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
// OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
// SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
// LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
// DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
// THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
// (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
// OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

//
// Code specific to OpenBSD.
//

namespace google_breakpad { namespace bsd {

static const int kKERN_PROC = KERN_PROC;
static const int kKERN_LWP = KERN_PROC;
static const int kKERN_LWP_FLAG = KERN_PROC_PID | KERN_PROC_SHOW_THREADS;
static const char *kPMAP_EXECUTABLE_PATH = "/usr/sbin/procmap";
typedef struct kinfo_proc kinfo_proc_t;
typedef struct kinfo_proc kinfo_lwp_t;

//
// Forward declaration in bsd_netbsd_openbsd_utils_shared.inc.cc
//
static char *ReadProcessArguments(pid_t pid, size_t *size);
static char *ReadProcessEnvironment(pid_t pid, size_t *bufsize);
static bool GetProcessMappingsWithPmap(pid_t pid,
                                       std::vector<Mapping> &mappings);

//
// System Target helper functions.
//

bool GetSystemVersion(unsigned &major, unsigned &minor, unsigned &build) {
  struct utsname uts;
  if (uname(&uts))
    return false;

  build = 0;
  // MAJOR.MINOR
  if (sscanf(uts.release, "%u.%u", &major, &minor) != 2)
    return false;

  // RELEASETYPE#N
  const char *buildno = strchr(uts.version, '#');
  if (buildno == NULL)
    return true;

  build = atoi(buildno + 1);
  return true;
}

bool GetCorePattern(char *buf, size_t bufsize) {
  strlcpy(buf, "%n.core", bufsize);
  return true;
}

//
// Process helper functions.
//

static inline pid_t KinfoProcGetPpid(const kinfo_proc_t *kp) {
  return kp->p_ppid;
}

static inline lwpid_t KinfoLwpGetId(const kinfo_lwp_t *kl) {
  return kl->p_tid;
}

static bool ReadProcessExecutablePath(pid_t pid, char *buf, size_t bufsize) {
  size_t size;
  char *args = ReadProcessArguments(pid, &size);
  if (args == NULL)
    return false;

  const uintptr_t *arg0 = reinterpret_cast<const uintptr_t*>(args);
  strlcpy(buf, reinterpret_cast<const char*>(*arg0), bufsize);
  free(args);

  return true;
}

char *GetProcessArguments(pid_t pid, size_t *bufsize) {
  size_t size;
  char *buf = ReadProcessArguments(pid, &size);
  if (buf == NULL)
    return NULL;

  uintptr_t *argp = reinterpret_cast<uintptr_t*>(buf);
  while (*argp != 0) {
    argp++;
  }

  size_t argsize = size - (reinterpret_cast<char*>(argp + 1) - buf);
  char *args = reinterpret_cast<char*>(calloc(1, argsize + 1));
  if (args == NULL) {
    free(buf);
    return NULL;
  }

  memcpy(args, argp + 1, argsize);
  free(buf);

  *bufsize = argsize;
  return args;
}

char *GetProcessEnvironment(pid_t pid, size_t *bufsize) {
  size_t size;
  char *buf = ReadProcessEnvironment(pid, &size);
  if (buf == NULL)
    return NULL;

  uintptr_t *envp = reinterpret_cast<uintptr_t*>(buf);
  while (*envp != 0) {
    envp++;
  }

  size_t envsize = size - (reinterpret_cast<char*>(envp + 1) - buf);
  char *envs = reinterpret_cast<char*>(calloc(1, envsize + 1));
  if (envs == NULL) {
    free(buf);
    return NULL;
  }

  memcpy(envs, envp + 1, envsize);
  free(buf);

  *bufsize = envsize;
  return envs;
}

ElfW(Auxinfo) *GetProcessElfAuxVector(pid_t pid, size_t *count) {
  if (pid == 0 || pid == getpid())
    return NULL;

  ElfW(Auxinfo) *auxv =
      reinterpret_cast<ElfW(Auxinfo)*>(calloc(ELF_AUX_ENTRIES,
                                              sizeof(ElfW(Auxinfo))));
  if (auxv == NULL)
    return NULL;

  struct ptrace_io_desc piod;
  piod.piod_op   = PIOD_READ_AUXV;
  piod.piod_offs = NULL;
  piod.piod_addr = auxv;
  piod.piod_len  = ELF_AUX_ENTRIES * sizeof(ElfW(Auxinfo));
  if (ptrace(PT_IO, pid, reinterpret_cast<caddr_t>(&piod), 0) < 0)
    return NULL;

  *count = piod.piod_len / sizeof(ElfW(Auxinfo));
  return auxv;
}

//
// OpenBSD is really hard to work with, the KERN_PROC_VMMAP will work
// only for super-user as of 5.7; the fallback is not too much joy,
// the user must setuid /usr/sbin/procmap if she wants to have the
// process mappings as it will be used as fallback.
//
static bool GetProcessMappingsFromSysctl(pid_t pid,
                                         std::vector<Mapping> &mappings) {
  int mib[] = { CTL_KERN, KERN_PROC, KERN_PROC_VMMAP, MIB_PID(pid) };

  size_t size;
  if (sysctl(mib, sizeof(mib)/sizeof(mib[0]), NULL, &size, NULL, 0) < 0)
    return false;

  size_t count = size / sizeof(struct kinfo_vmentry);
  struct kinfo_vmentry *entries =
      reinterpret_cast<struct kinfo_vmentry*>(calloc(1, size));
  if (entries == NULL)
    return false;

  if (sysctl(mib, sizeof(mib)/sizeof(mib[0]), entries, &size, NULL, 0) < 0) {
    free(entries);
    return false;
  }

  for (int n = 0; n < count; n++) {
    Mapping m;
    memset(&m, 0, sizeof(m));

    m.start = entries[n].kve_start;
    m.end = entries[n].kve_end;
    m.offset = entries[n].kve_offset;

    if ((entries[n].kve_inheritance & KVE_INH_COPY) != 0 ||
        (entries[n].kve_inheritance & KVE_INH_SHARE) == 0) {
      m.protection |= Mapping::PROT_PRIVATE;
    }

    if (entries[n].kve_protection & KVE_PROT_EXEC) {
      m.protection |= PROT_EXEC;
    }
    if (entries[n].kve_protection & KVE_PROT_WRITE) {
      m.protection |= PROT_WRITE;
    }
    if (entries[n].kve_protection & KVE_PROT_READ) {
      m.protection |= PROT_READ;
    }

    mappings.push_back(m);
  }
  free(entries);

  return true;
}

bool GetProcessMappings(pid_t pid, std::vector<Mapping> &mappings) {
  if (!GetProcessMappingsFromSysctl(pid, mappings)) {
    if (!GetProcessMappingsWithPmap(pid, mappings))
      return false;
  }

  std::vector<Mapping> dymappings;
  if (!ReadProcessLinkerMap(pid, dymappings))
    return true;

  char exe_path[PATH_MAX + 1];
  const char *exe_name = NULL;
  if (!GetProcessExecutablePath(pid, exe_path, sizeof(exe_path), &exe_name)) {
    exe_path[0] = 0;
  }

  // Merge dynamic mappings.
  AugmentMappings(pid, mappings, dymappings, true);

  return true;
}

//
// Thread helper functions.
//

lwpid_t GetThreadId() {
  return getthrid();
}

bool KillThread(pid_t, lwpid_t lwpid, int sig) {
  return kill(lwpid, sig) == 0;
}

bool KillThread(lwpid_t lwpid, int sig) {
  return kill(lwpid, sig) == 0;
}

bool GetThreadContext(pid_t, lwpid_t lwpid, ThreadContext &context) {
  memset(&context, 0, sizeof(context));

  if (ptrace(PT_GETREGS, lwpid,
             reinterpret_cast<caddr_t>(&context.gregs), 0) < 0)
    return false;

  // Ignore results for these.
#if defined(PT_GETFPREGS)
#if defined(__i386__)
#if defined(PT_GEXMMREGS)
  if (ptrace(PT_GETXMMREGS, lwpid,
             reinterpret_cast<caddr_t>(&context.fregs), 0) == 0) {
    context.is_x87 = false;
  } else
#endif
  if (ptrace(PT_GETFPREGS, lwpid,
             reinterpret_cast<caddr_t>(&context.fregs), 0) == 0) {
    context.is_x87 = true;
  } else {
    memset(&context.fregs, 0, sizeof(context.fregs));
  }
#else
  if (ptrace(PT_GETFPREGS, lwpid,
             reinterpret_cast<caddr_t>(&context.fregs), 0) < 0) {
    memset(&context.fregs, 0, sizeof(context.fregs));
  }
#endif
#endif

  return true;
}

} }

//
// Include code shared across NetBSD and OpenBSD.
//
#include "common/bsd/bsd_netbsd_openbsd_utils_shared.inc.cc"
