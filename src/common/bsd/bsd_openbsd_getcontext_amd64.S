// Copyright (c) 2015, Orlando Bassotto.
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are
// met:
//
//     * Redistributions of source code must retain the above copyright
// notice, this list of conditions and the following disclaimer.
//     * Redistributions in binary form must reproduce the above
// copyright notice, this list of conditions and the following disclaimer
// in the documentation and/or other materials provided with the
// distribution.
//     * Neither the name of Google Inc. nor the names of its
// contributors may be used to endorse or promote products derived from
// this software without specific prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
// "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
// LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
// A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
// OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
// SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
// LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
// DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
// THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
// (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
// OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

.text

.globl getcontext
getcontext:
  movq %rdi, ( 0 * 8)(%rdi)
  movq %rsi, ( 1 * 8)(%rdi)
  movq %rdx, ( 2 * 8)(%rdi)
  movq %rcx, ( 3 * 8)(%rdi)
  movq %r8,  ( 4 * 8)(%rdi)
  movq %r9,  ( 5 * 8)(%rdi)
  movq %r10, ( 6 * 8)(%rdi)
  movq %r11, ( 7 * 8)(%rdi)
  movq %r12, ( 8 * 8)(%rdi)
  movq %r13, ( 9 * 8)(%rdi)
  movq %r14, (10 * 8)(%rdi)
  movq %r15, (11 * 8)(%rdi)
  movq %rbp, (12 * 8)(%rdi)
  movq %rbx, (13 * 8)(%rdi)
  movq %rax, (14 * 8)(%rdi)
  xorq %rax, %rax
  movl %gs, %eax
  movq %rax, (15 * 8)(%rdi)
  movl %fs, %eax
  movq %rax, (16 * 8)(%rdi)
  movl %es, %eax
  movq %rax, (17 * 8)(%rdi)
  movl %ds, %eax
  movq %rax, (18 * 8)(%rdi)
  movq $0,   (19 * 8)(%rdi)
  movq $0,   (20 * 8)(%rdi)
  movq (%rsp), %rax
  movq %rax, (21 * 8)(%rdi)
  xorq %rax, %rax
  movl %cs, %eax
  movq %rax, (22 * 8)(%rdi)
  pushfq
  popq %rax
  movq %rax, (23 * 8)(%rdi)
  movq %rsp, (24 * 8)(%rdi)
  xorq %rax, %rax
  movl %ss, %eax
  movq %rsp, (25 * 8)(%rdi)
  xorq %rax, %rax
  movq %rax, (26 * 8)(%rdi)
  movq %rax, (27 * 8)(%rdi)
  xorq %rax, %rax
  retq

