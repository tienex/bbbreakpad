// Copyright (c) 2015, Orlando Bassotto.
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are
// met:
//
//     * Redistributions of source code must retain the above copyright
// notice, this list of conditions and the following disclaimer.
//     * Redistributions in binary form must reproduce the above
// copyright notice, this list of conditions and the following disclaimer
// in the documentation and/or other materials provided with the
// distribution.
//     * Neither the name of Google Inc. nor the names of its
// contributors may be used to endorse or promote products derived from
// this software without specific prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
// "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
// LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
// A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
// OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
// SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
// LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
// DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
// THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
// (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
// OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

#ifndef CLIENT_BSD_BSD_UTILS_H__
#define CLIENT_BSD_BSD_UTILS_H__

#if defined(__OpenBSD__)
#define _DYN_LOADER
#endif
#if defined(BSD_HAVE_SYS_MUTEX_H)
#include <sys/mutex.h>
#endif
#if defined(BSD_HAVE_SYS_TREE_H)
#include <sys/tree.h>
#endif
#include <errno.h>
#include <fcntl.h>
#include <kvm.h>
#include <libgen.h>
#if defined(__FreeBSD__) || defined(__DragonFly__)
#include <libutil.h>
#endif
#include <limits.h>
#include <link.h>
#if defined(__NetBSD__)
#include <lwp.h>
#endif
#if defined(BSD_HAVE_MACHINE_PMAP_H) && !defined(__vax__)
#include <machine/pmap.h>
#endif
#if defined(BSD_HAVE_MACHINE_PTE_H)
#include <machine/pte.h>
#endif
#if !defined(__FreeBSD__)
#include <machine/vmparam.h>
#endif
#include <paths.h>
#include <signal.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/exec.h>
#if defined(__DragonFly__)
#include <sys/kinfo.h>
#endif
#if !defined(__NetBSD__)
#include <sys/limits.h>
#endif
#if defined(__FreeBSD__) || defined(__DragonFly__)
#include <sys/procctl.h>
#include <sys/procfs.h>
#endif
#include <sys/ptrace.h>
#include <sys/queue.h>
#include <sys/mman.h>
#include <sys/socket.h>
#include <sys/stat.h>
#include <sys/sysctl.h>
#if defined(__FreeBSD__)
#include <sys/thr.h>
#endif
#include <sys/user.h>
#include <sys/utsname.h>
#include <sys/wait.h>
#include <unistd.h>

#include <string>

#include "common/bsd/bsd_thread_context.h"
#include "common/memory.h"

//
// Include target-specific header.
//
#if defined(__DragonFly__)
#include "common/bsd/bsd_dragonfly_utils.h"
#elif defined(__FreeBSD__)
#include "common/bsd/bsd_freebsd_utils.h"
#elif defined(__NetBSD__)
#include "common/bsd/bsd_netbsd_utils.h"
#elif defined(__OpenBSD__)
#include "common/bsd/bsd_openbsd_utils.h"
#else
#error "I don't know this BSD!"
#endif

namespace google_breakpad { namespace bsd {

// The top of the stack contains the following words:
// - uint32_t  nenvs
// - void     *envp
// - uint32_t  argc
// - void     *argv
//
// The auxv is located after envp pointers for NetBSD 6.1.5.
struct TopOfStack {
  uintptr_t argv;
  uint32_t argc;
#ifdef __LP64__
  uint32_t padding0;
#endif
  uintptr_t envp;
  uint32_t nenvs;
#ifdef __LP64__
  uint32_t padding1;
#endif
};

//
// Minidump helper functions.
//

// Retrieve the OS name used in minidumps.
static inline const char *GetOSName() { return BSD_OSNAME; }

// Retrieve the minidump platform identifier.
static inline uint32_t GetPlatformID() { return BSD_PLATFORM_ID; }

//
// Processor helper functions.
//

// Returns the number of processors in the system.
size_t GetProcessorCount();

#if defined(__i386__) || defined(__x86_64__)
// Returns information about the x86 processor in the system, and
// fills MDCPUInformation |info| structure.
void GetX86ProcessorInfo(uint16_t &level, uint16_t &revision,
                         MDCPUInformation &info);
#endif

// Generate a clone of the output of Linux /proc/cpuinfo; on systems
// with a procfs, like NetBSD, the file in /proc is read instead.
// This function is currently supported only for x86 and x86-64 targets.
char *GenerateLinuxCpuInfo(size_t *size);

//
// System Target helper functions.
//

// Obtains the version of the current target, the |major|, |minor|
// and |build| arguments are interpreted as such:
//
// NetBSD 6.1.5                      | MAJOR[6] MINOR[1] BUILD[5]
// FreeBSD 10.1-RELEASE-p42          | MAJOR[10] MINOR[1] BUILD[42]
// OpenBSD 5.7-RELEASE GENERIC.MP#12 | MAJOR[5] MINOR[7] BUILD[12]
// DragonFly 4.0.5-RELEASE           | MINOR[4] MINOR[0] BUILD[5]
//
bool GetSystemVersion(unsigned &major, unsigned &minor, unsigned &build);

// Obtains the core pattern used by the target OS.
bool GetCorePattern(char *buf, size_t bufsize);

// Returns whether the core pattern used by the target OS is known
// and could be handled to make core file names via GetCoreFileName.
bool HasDefaultCorePattern();

// Given a process id in |pid| (or zero, for current process),
// returns the core file name.
std::string GetCoreFileName(pid_t pid = 0);

//
// Process helper functions.
//

// Returns the parent process id of the process |pid| specified; returns -1
// on failure.
pid_t GetParentProcessId(pid_t pid);

// Obtains the executable path of process |pid|. The argument |effective|
// contains what has really been extracted in the case the path has been
// recovered.
bool GetProcessExecutablePath(pid_t pid, char *buf, size_t bufsize,
                              const char **effective = NULL);

// Obtains the executable ELF aux vector of process |pid|.
// This function can be called any time on FreeBSD, but must be called
// after the process has been attached via ptrace(2) on all other targets.
// This function returns NULL if the target process is not an ELF and
// native to the target.
ElfW(Auxinfo) *GetProcessElfAuxVector(pid_t pid, size_t *count);

// Obtains the process |pid| arguments. The whole size is returned in |size|.
char *GetProcessArguments(pid_t pid, size_t *size);

// Obtains the process |pid| environment strings. The whole size is
// returned in |size|.
char *GetProcessEnvironment(pid_t pid, size_t *size);

// Obtains a list of thread identifiers in |lwpids| running in process |pid|.
bool GetProcessThreads(pid_t pid, std::vector<lwpid_t> &lwpids);
bool GetProcessThreads(pid_t pid, wasteful_vector<lwpid_t> &lwpids);

// Obtains a list of mappings in |mappings| active in process |pid|.
// Note for OpenBSD users: the support of breakpad is extremely incomplete
// and to be considered experimental, the OpenBSD kernel has no way to
// retrieve the path of an open vnode, and as such all mappings will result
// without a path attached. This in turn makes impossible to retrieve the
// list of shared objects loaded in process |pid|.
bool GetProcessMappings(pid_t pid, std::vector<Mapping> &mappings);

// Copy |length| bytes from |remote| address in process |pid| to local
// address |local|.
// The process must have been previously attached via ptrace(2).
ssize_t ReadProcess(pid_t pid, void *local, uintptr_t remote, size_t length);

// Copy |length| bytes to |remote| address in process |pid| from local
// address |local|.
// The process must have been previously attached via ptrace(2).
ssize_t WriteProcess(pid_t pid, uintptr_t remote, const void *local,
                     size_t length);

// Enables/disables the creation of core dumps via |enable| in the
// target process |pid|.
// This function is supported only by FreeBSD.
bool EnableProcessCoreDump(pid_t pid, bool enable = true);

// Obtains the times of a process |pid|.
bool GetProcessTimes(pid_t pid, ProcessTimes &times);

//
// Thread helper functions.
//

// Gets the current thread id.
lwpid_t GetThreadId();

// Delivers signal |sig| to thread |lwpid| to process |pid|.
// This function is not supported on NetBSD.
bool KillThread(pid_t pid, lwpid_t lwpid, int sig);

// Delivers signal |sig| to thread |lwpid| to the current process.
bool KillThread(lwpid_t lwpid, int sig);

// Read the context in |context| of thread |lwpid| from process |pid|.
bool GetThreadContext(pid_t pid, lwpid_t lwpid, ThreadContext &context);

//
// Cloning thread helper functions.
//

// This function will return true if ForkThread needs a stack.
bool NeedForkThreadStack();

// Forks a thread using a similar technique to Linux clone(2).
pid_t ForkThread(int (*entry)(void *), void *arg, void *stack,
                 size_t stacksize);

// Due to the nature of ForkThread, the implementation may need
// special handling to wait the process |pid|. The function arguments
// have the same meaning of waitpid.
pid_t WaitForkedThread(pid_t pid, int *status, int flags = 0);

//
// Miscellaneous helper functions.
//

// Builds the BSD /proc path for the specified |pid| and file |name|.
void MakeProcPath(pid_t pid, const char *name, char *buf, size_t bufsize);

// Reads a file in a BSD /proc.
char *ReadProcFile(pid_t pid, const char *name, size_t *size);
char *ReadProcFile(const char *filename, size_t *size);

// Given a process id |pid|, a set of mappings in |result| and 
// an other set in |input|, set the paths of the various addreses,
// eventually add also missing executable mappings. If |extract_exe_name|
// is set to true, then the complete executable path is used.
void AugmentMappings(pid_t pid, std::vector<Mapping> &result,
                     const std::vector<Mapping> &input,
                     bool extract_exe_name);

} }

#endif  // CLIENT_BSD_BSD_UTILS_H__
