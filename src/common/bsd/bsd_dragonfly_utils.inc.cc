// Copyright (c) 2015, Orlando Bassotto.
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are
// met:
//
//     * Redistributions of source code must retain the above copyright
// notice, this list of conditions and the following disclaimer.
//     * Redistributions in binary form must reproduce the above
// copyright notice, this list of conditions and the following disclaimer
// in the documentation and/or other materials provided with the
// distribution.
//     * Neither the name of Google Inc. nor the names of its
// contributors may be used to endorse or promote products derived from
// this software without specific prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
// "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
// LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
// A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
// OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
// SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
// LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
// DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
// THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
// (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
// OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

//
// Code specific to DragonFly.
//

namespace google_breakpad { namespace bsd {

static const size_t kELF_AUX_ENTRIES = 32; // large enough
static const size_t kCOMMLEN = MAXCOMLEN;
static const int kKERN_PROC = KERN_PROC;
static const int kKERN_PROC_INC_THREAD = KERN_PROC_FLAG_LWP;
static const char *kPROCFS_EXECUTABLE_FILE_LINK_NAME = "file";
typedef struct ::kinfo_proc kinfo_proc_t;

//
// System Target helper functions.
//

bool GetSystemVersion(unsigned &major, unsigned &minor, unsigned &build) {
  struct utsname uts;
  if (uname(&uts))
    return false;

  // DragonFly vMAJOR.MINOR.BUILD
  build = 0;
  return sscanf(uts.version, "%*s v%u.%u.%u", &major, &minor, &build) >= 2;
}

//
// Process helper functions.
//

static inline pid_t KinfoProcGetPpid(const kinfo_proc_t *kp) {
  return kp->kp_ppid;
}

static inline lwpid_t KinfoProcGetLWPid(const kinfo_proc_t *kp) {
  return kp->kp_lwp.kl_tid;
}

static inline struct timespec KinfoProcGetStartTime(const kinfo_proc_t *kp) {
  struct timespec ts;
  ts.tv_sec  = kp->kp_start.tv_sec;
  ts.tv_nsec = kp->kp_start.tv_usec * 1000;
  return ts;
}

static inline struct timespec KinfoProcGetUserTime(const kinfo_proc_t *kp) {
  struct timespec ts;
  ts.tv_sec  = kp->kp_ru.ru_utime.tv_sec;
  ts.tv_nsec = kp->kp_ru.ru_utime.tv_usec * 1000;
  return ts;
}

static inline struct timespec KinfoProcGetSystemTime(const kinfo_proc_t *kp) {
  struct timespec ts;
  ts.tv_sec  = kp->kp_ru.ru_stime.tv_sec;
  ts.tv_nsec = kp->kp_ru.ru_stime.tv_usec * 1000;
  return ts;
}

char *GetProcessArguments(pid_t pid, size_t *bufsize) {
  int mib[] = { CTL_KERN, KERN_PROC, KERN_PROC_ARGS, MIB_PID(pid) };

  size_t size;
  size_t capacity = getpagesize();
  char *buf = NULL;

  while (capacity != 0) {
    buf = reinterpret_cast<char*>(calloc(1, capacity));
    if (buf == NULL)
      return NULL;

    size = capacity;
    if (sysctl(mib, sizeof(mib)/sizeof(int), buf, &size, NULL, 0) < 0)
        return NULL;

    if (size < capacity) {
      char *newbuf = reinterpret_cast<char*>(realloc(buf, size));
      if (newbuf != NULL) {
        buf = newbuf;
      }
      break;
    }

    free(buf);
    capacity <<= 1;
  }

  *bufsize = size;
  return buf;
}

char *GetProcessEnvironment(pid_t pid, size_t *bufsize) {
  kvm_t *kd = kvm_openfiles(NULL, _PATH_DEVNULL, NULL, O_RDONLY, NULL);
  if (kd == NULL)
    return NULL;

  int cnt;
  kinfo_proc_t *ki = kvm_getprocs(kd, KERN_PROC_PID, MIB_PID(pid), &cnt);
  if (ki == NULL) {
    kvm_close(kd);
    return NULL;
  }

  char **env = kvm_getenvv(kd, ki, 0);
  if (env == NULL) {
    kvm_close(kd);
    return NULL;
  }

  size_t size = 0;
  char **envp = env;
  while (*envp != NULL) {
    size_t length = strlen(*envp) + 1;
    size += length, envp++;
  }

  char *renv = reinterpret_cast<char*>(calloc(1, size));
  if (renv == NULL) {
    kvm_close(kd);
    return NULL;
  }

  char *renvp = renv;
  envp = env;
  while (*envp != NULL) {
    size_t length = strlen(*envp) + 1;
    memcpy(renvp, *envp, length);
    renvp += length, envp++;
  }

  kvm_close(kd);

  *bufsize = size;
  return renv;
}

bool GetProcessMappings(pid_t pid, std::vector<Mapping> &mappings) {
  char exe_path[PATH_MAX + 1] = {0};
  uintptr_t exe_address = 0;
  bool deleted = false;

  size_t size;
  char *buf = ReadProcFile(pid, "map", &size);
  if (buf == NULL)
    return false;

  // The convoluted logic here is to deal with deleted binaries.
  if (GetProcessExecutablePath(pid, exe_path, sizeof(exe_path))) {
    if (access(exe_path, F_OK) != 0 &&
        (errno == ENOENT || errno == ENOTDIR)) {
      deleted = true;
      if (!GetProcessAddress(pid, exe_address)) {
        exe_address = 0;
      }
    }
  }

  char *line = buf;
  bool has_start_offset = false;
  uintptr_t start_offset = 0;
  std::string last_filename;
  while ((line - buf) < size) {
    unsigned long long start, end, offset;
    char r, w, x, C;
    int nfields, pos = 0;
    char *next = strchr(line, '\n');

    *next = 0;

    nfields = sscanf(line, "%llx %llx %*u %*u %*llx %c%c%c %*u %*u %*llx %c %n",
            &start, &end, &r, &w, &x, &C, &pos);
    if (nfields != 6)
      continue;

    const char *filename = strstr(line + pos, "vnode");
    if (filename != NULL) {
        filename += 6;
    }

    Mapping m;
    memset(&m, 0, sizeof(m));

    m.start = start;
    m.end = end;
    // Emulate offset *shrug*
    if (filename != NULL) {
      if (last_filename == filename && has_start_offset) {
          m.offset = start - start_offset;
      } else {
          m.offset = 0;
          start_offset = start;
          has_start_offset = true;
          last_filename = filename;
      }
    }
    if (deleted && exe_address >= m.start && exe_address < m.end) {
      strlcpy(m.name, exe_path, sizeof(m.name));
    } else if (filename != NULL) {
      strlcpy(m.name, filename, sizeof(m.name));
    }

    if (C == 'C') {
      m.protection |= Mapping::PROT_PRIVATE;
    }

    if (x == 'x') {
      m.protection |= PROT_EXEC;
    }
    if (w == 'w') {
      m.protection |= PROT_WRITE;
    }
    if (r == 'r') {
      m.protection |= PROT_READ;
    }

    mappings.push_back(m);

    line = next + 1;
  }

  free(buf);
  return true;
}

//
// Thread helper functions.
//

lwpid_t GetThreadId() {
  return lwp_gettid();
}

bool KillThread(pid_t pid, lwpid_t lwpid, int sig) {
  return lwp_kill(pid, lwpid, sig) == 0;
}

bool KillThread(lwpid_t lwpid, int sig) {
  return lwp_kill(-1, lwpid, sig) == 0;
}

bool GetThreadContext(pid_t pid, lwpid_t lwpid, ThreadContext &context) {
  memset(&context, 0, sizeof(context));

  if (ptrace(PT_GETREGS, pid,
             reinterpret_cast<caddr_t>(&context.gregs), lwpid) < 0)
    return false;

  // Ignore results for these.
  if (ptrace(PT_GETFPREGS, pid,
             reinterpret_cast<caddr_t>(&context.fregs), lwpid) == 0) {
    context.is_x87 = false;
  } else {
    memset(&context.fregs, 0, sizeof(context.fregs));
  }

  if (ptrace(PT_GETDBREGS, pid,
             reinterpret_cast<caddr_t>(&context.dregs), lwpid) < 0) {
    memset(&context.dregs, 0, sizeof(context.dregs));
  }

  return true;
}

//
// Cloning thread helper functions.
//

bool NeedForkThreadStack() {
  return false;
}

pid_t ForkThread(int (*entry)(void *), void *arg, void *, size_t) {
  return ForkSplitThread(entry, arg);
}

pid_t WaitForkedThread(pid_t pid, int *status, int flags) {
  return WaitSplitForkedThread(pid, status, flags);
}

} }

//
// Include code shared across FreeBSD and DragonFly.
//
#include "common/bsd/bsd_freebsd_dragonfly_utils_shared.inc.cc"

//
// Include code shared across NetBSD and DragonFly.
//
#include "common/bsd/bsd_netbsd_dragonfly_utils_shared.inc.cc"
