// Copyright (c) 2015, Orlando Bassotto.
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are
// met:
//
//     * Redistributions of source code must retain the above copyright
// notice, this list of conditions and the following disclaimer.
//     * Redistributions in binary form must reproduce the above
// copyright notice, this list of conditions and the following disclaimer
// in the documentation and/or other materials provided with the
// distribution.
//     * Neither the name of Google Inc. nor the names of its
// contributors may be used to endorse or promote products derived from
// this software without specific prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
// "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
// LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
// A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
// OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
// SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
// LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
// DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
// THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
// (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
// OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

// This source file provides replacements for libc functions that we need. If
// we call the libc functions directly we risk crashing in the dynamic linker
// as it tries to resolve uncached PLT entries.

#include <ctype.h>

#include <iomanip>
#include <sstream>

//
// x86:
//
// processor\t: 3
// vendor_id\t: GenuineIntel
// cpu family\t: 6
// model\t\t: 14
// model name\t: Intel(R) Xeon(R) CPU E5-2697 v2 @ 2.70GHz
// stepping\t: 4
// cpu MHz\t\t: 2968.79
// cpuid level\t: 13
// flags\t\t: fpu vme de pse tsc msr pae mce cx8 apic sep mtrr pge mca cmov pat pse36 clflush dts mmx fxsr sse sse2 ss
//
namespace {

static uint64_t GetProcessorFrequency() {
#if defined(__OpenBSD__)
  int mib[] = { CTL_HW, HW_CPUSPEED };
  int speed = 0;
  size_t size = sizeof(speed);
  if (sysctl(mib, 2, &speed, &size, NULL, 0) < 0) {
    speed = 100;
  }
  return static_cast<uint64_t>(speed) * 1000000;
#else
  uint64_t tsc_freq = 0;
  size_t size = sizeof(tsc_freq);
  if (sysctlbyname("machdep.tsc_freq", &tsc_freq, &size, NULL, 0) < 0) {
    size = sizeof(tsc_freq);
    if (sysctlbyname("hw.tsc_frequency", &tsc_freq, &size, NULL, 0) < 0) {
      tsc_freq = 100 * 1000000;
    }
  }
  return tsc_freq;
#endif
}

union x86_vendor {
  struct {
    uint32_t ebx;
    uint32_t edx;
    uint32_t ecx;
  };
  char string[12 + 1];
};

union x86_model {
  struct {
    uint32_t eax;
    uint32_t ebx;
    uint32_t ecx;
    uint32_t edx;
  } id[3];
  char string[48 + 1];
};

static uint32_t GetProcessorExtendedLevel() {
  uint32_t extlevel, unused1, unused2, unused3;
  cpuid(0x80000000, extlevel, unused1, unused2, unused3);
  return extlevel;
}

static void GetProcessorInfo(uint32_t &family, uint32_t &model,
                             uint32_t &stepping, uint32_t features[4]) {
  uint32_t version, unused1;
  uint32_t xfeatures1, xfeatures2;

  // clear out extended features
  features[2] = 0;
  features[3] = 0;

  cpuid(1, version, unused1, features[1], features[0]);

  family = (version >> 8) & 0xf;
  model = (version >> 4) & 0xf;
  stepping = (version >> 0) & 0xf;

  if (family == 0xf || family == 0x6) {
    model += (version >> 12) & 0xf;
  }

  if (family == 0xf) {
    family += (version >> 20) & 0xff;
  }

  if (GetProcessorExtendedLevel() >= 0x80000001) {
    cpuid(0x80000001, version, unused1, features[3], features[2]);
  }
}

static const char *GetProcessorVendor(uint32_t &cpuid_level, x86_vendor &v) {
  memset(&v, 0, sizeof(v));
  cpuid(0, cpuid_level, v.ebx, v.ecx, v.edx);
  return v.string;
}

static const char *GetProcessorModel(x86_model &m) {
  const char *model_name = NULL;
  memset(&m, 0, sizeof(m));
  if (GetProcessorExtendedLevel() >= 0x80000004) {
    cpuid(0x80000002, m.id[0].eax, m.id[0].ebx, m.id[0].ecx, m.id[0].edx);
    cpuid(0x80000003, m.id[1].eax, m.id[1].ebx, m.id[1].ecx, m.id[1].edx);
    cpuid(0x80000004, m.id[2].eax, m.id[2].ebx, m.id[2].ecx, m.id[2].edx);
    model_name = m.string;
    while (isspace(*model_name)) {
        model_name++;
    }
  }
  return model_name;
}

}

namespace google_breakpad { namespace bsd {

static char *MakeLinuxCpuInfo(size_t *size) {
  std::stringstream ss;
  uint32_t cpuid_level;
  uint32_t family;
  uint32_t cpumodel;
  uint32_t stepping;
  uint32_t features[4];
  x86_vendor vendor;
  x86_model model;
  const char *vendor_id;
  const char *model_name;

  ::GetProcessorInfo(family, cpumodel, stepping, features);
  vendor_id = GetProcessorVendor(cpuid_level, vendor);
  model_name = GetProcessorModel(model);

  size_t ncpus = GetProcessorCount();
  uint64_t freq = GetProcessorFrequency();

  for (size_t n = 0; n < ncpus; n++) {
    ss << "processor\t: " << n << std::endl
       << "vendor_id\t: " << vendor_id << std::endl
       << "cpu family\t: " << family << std::endl
       << "model\t\t: " << cpumodel << std::endl;
    if (model_name) {
      ss << "model name\t: " << model_name << std::endl;
    }
    ss << "stepping\t: " << stepping << std::endl
       << "cpu MHz\t\t: " << (freq / 1000000) << '.'
                          << std::setw(2) << std::setfill('0')
                          << ((freq / 10000) % 100) << std::endl
       << "cpuid level\t: " << cpuid_level << std::endl;
    ss << "flags\t\t:";
    if (features[0] & (1 << 0)) ss << " fpu";
    if (features[0] & (1 << 1)) ss << " vme";
    if (features[0] & (1 << 2)) ss << " de";
    if (features[0] & (1 << 3)) ss << " pse";
    if (features[0] & (1 << 4)) ss << " tsc";
    if (features[0] & (1 << 5)) ss << " msr";
    if (features[0] & (1 << 6)) ss << " pae";
    if (features[0] & (1 << 7)) ss << " mce";
    if (features[0] & (1 << 8)) ss << " cx8";
    if (features[0] & (1 << 9)) ss << " apic";
    if (features[0] & (1 << 11)) ss << " sep";
    if (features[0] & (1 << 12)) ss << " mtrr";
    if (features[0] & (1 << 13)) ss << " pge";
    if (features[0] & (1 << 14)) ss << " mca";
    if (features[0] & (1 << 15)) ss << " cmov";
    if (features[0] & (1 << 16)) ss << " pat";
    if (features[0] & (1 << 17)) ss << " pse36";
    if (features[0] & (1 << 18)) ss << " pn";
    if (features[0] & (1 << 19)) ss << " clflush";
    if (features[0] & (1 << 21)) ss << " dts";
    if (features[0] & (1 << 22)) ss << " acpi";
    if (features[0] & (1 << 23)) ss << " mmx";
    if (features[0] & (1 << 24)) ss << " fxsr";
    if (features[0] & (1 << 25)) ss << " sse";
    if (features[0] & (1 << 26)) ss << " sse2";
    if (features[0] & (1 << 27)) ss << " ss";
    if (features[0] & (1 << 28)) ss << " ht";
    if (features[0] & (1 << 29)) ss << " tm";
    if (features[0] & (1 << 30)) ss << " ia64";
    if (features[0] & (1 << 31)) ss << " pbe";
    if (features[2] & (1 << 11)) ss << " syscall";
    if (features[2] & (1 << 19)) ss << " mp";
    if (features[2] & (1 << 20)) ss << " nx";
    if (features[2] & (1 << 22)) ss << " mmxext";
    if (features[2] & (1 << 25)) ss << " fxsr_opt";
    if (features[2] & (1 << 26)) ss << " pdpe1gb";
    if (features[2] & (1 << 27)) ss << " rdtscp";
    if (features[2] & (1 << 29)) ss << " lm";
    if (features[2] & (1 << 30)) ss << " 3dnowext";
    if (features[2] & (1 << 31)) ss << " 3dnow";
    if (features[3] & (1 << 0)) ss << " lahf_lm";
    if (features[3] & (1 << 1)) ss << " cmp_legacy";
    if (features[3] & (1 << 2)) ss << " svm";
    if (features[3] & (1 << 3)) ss << " extapic";
    if (features[3] & (1 << 4)) ss << " cr8_legacy";
    if (features[3] & (1 << 5)) ss << " altmovcr8";
    if (features[3] & (1 << 6)) ss << " abm";
    if (features[3] & (1 << 7)) ss << " sse4a";
    if (features[3] & (1 << 8)) ss << " misalignsse";
    if (features[3] & (1 << 9)) ss << " 3dnowprefetch";
    if (features[3] & (1 << 10)) ss << " osvw";
    if (features[3] & (1 << 11)) ss << " ibs";
    if (features[3] & (1 << 12)) ss << " sse5";
    if (features[3] & (1 << 13)) ss << " skinit";
    if (features[3] & (1 << 14)) ss << " wdt";
    if (features[1] & (1 << 0)) ss << " pni";
    if (features[1] & (1 << 3)) ss << " monitor";
    if (features[1] & (1 << 4)) ss << " ds_cpl";
    if (features[1] & (1 << 5)) ss << " vmx";
    if (features[1] & (1 << 6)) ss << " smx";
    if (features[1] & (1 << 7)) ss << " est";
    if (features[1] & (1 << 8)) ss << " tm2";
    if (features[1] & (1 << 9)) ss << " ssse3";
    if (features[1] & (1 << 10)) ss << " cid";
    if (features[1] & (1 << 13)) ss << " cx16";
    if (features[1] & (1 << 14)) ss << " xtpr";
    if (features[1] & (1 << 18)) ss << " dca";
    if (features[1] & (1 << 19)) ss << " sse4_1";
    if (features[1] & (1 << 20)) ss << " sse4_2";
    if (features[1] & (1 << 21)) ss << " x2apic";
    if (features[1] & (1 << 22)) ss << " movbe";
    if (features[1] & (1 << 23)) ss << " popcnt";
    if (features[1] & (1 << 24)) ss << " tsc_deadline_timer";
    if (features[1] & (1 << 30)) ss << " rdrnd";
    if (features[1] & (1 << 31)) ss << " hypervisor";
    ss << std::endl << std::endl;
  }
  std::string str(ss.str());

  char *buf = reinterpret_cast<char*>(calloc(1, str.size() + 1));
  if (buf == NULL)
    return NULL;

  strlcpy(buf, str.c_str(), str.size());
  *size = str.size();
  return buf;
}

} }
