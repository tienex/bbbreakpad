// Copyright (c) 2015, Orlando Bassotto.
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are
// met:
//
//     * Redistributions of source code must retain the above copyright
// notice, this list of conditions and the following disclaimer.
//     * Redistributions in binary form must reproduce the above
// copyright notice, this list of conditions and the following disclaimer
// in the documentation and/or other materials provided with the
// distribution.
//     * Neither the name of Google Inc. nor the names of its
// contributors may be used to endorse or promote products derived from
// this software without specific prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
// "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
// LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
// A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
// OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
// SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
// LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
// DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
// THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
// (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
// OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

//
// Contains code shared across all BSD implementations.
//

#include <poll.h>

#include <map>

#include "common/bsd/bsd_utils.h"
#include "common/bsd/eintr_wrapper.h"

#define MIB_PID(pid) ((pid) <= 0 ? getpid() : (pid))

// Returns from bsd::GetCorePatternType()
#define BSD_CORE_PATTERN_CUSTOM 0
#define BSD_CORE_PATTERN_SIMPLE 1
#define BSD_CORE_PATTERN_NAMED  2

#if defined(__i386__) || defined(__x86_64__)
static inline void cpuid(uint32_t op, uint32_t &eax, uint32_t &ebx,
                         uint32_t &ecx, uint32_t &edx) {
  __asm__ __volatile__(
#if defined(__i386__)
  "pushl %%ebx\n\t"
#endif
  "cpuid\n\t"
#if defined(__i386__)
  "movl %%ebx, %[ebx]\n\t"
  "popl %%ebx"
#endif
  : "=a"(eax),
#if defined(__i386__)
    [ebx]"=r"(ebx),
#else
    "=b"(ebx),
#endif
    "=c"(ecx),
    "=d"(edx)
  : "a"(op));
}
#endif

// Forward declarations.
namespace google_breakpad { namespace bsd {

// Returns an address that lies inside the main executable
// mapping in process |pid|.
static bool GetProcessAddress(pid_t pid, uintptr_t &address);

} }

//
// Forward declarations, for the benefit of OpenBSD and DragonFly.
//
namespace google_breakpad { namespace bsd {
static bool ReadProcessLinkerMap(pid_t pid, std::vector <Mapping> &mappings);
static pid_t ForkSplitThread(int (*entry)(void *), void *arg);
static pid_t WaitSplitForkedThread(pid_t pid, int *status, int flags);
} }

//
// Include Linux /proc/cpuinfo generator.
//
#if defined(__i386__) || defined(__x86_64__)
#include "common/bsd/bsd_x86_cpuinfo.inc.cc"
#elif defined(__vax__)
#include "common/bsd/bsd_vax_cpuinfo.inc.cc"
#else
namespace google_breakpad { namespace bsd {
static char *MakeLinuxCpuInfo(size_t *) {
  return NULL;
}
} }
#endif

//
// Include target specific code.
//
#if defined(__DragonFly__)
#include "common/bsd/bsd_dragonfly_utils.inc.cc"
#elif defined(__FreeBSD__)
#include "common/bsd/bsd_freebsd_utils.inc.cc"
#elif defined(__NetBSD__)
#include "common/bsd/bsd_netbsd_utils.inc.cc"
#elif defined(__OpenBSD__)
#include "common/bsd/bsd_openbsd_utils.inc.cc"
#else
#error "I don't know this BSD!"
#endif

namespace google_breakpad { namespace bsd {

//
// Processor helper functions.
//

size_t GetProcessorCount() {
   int mib[] = { CTL_HW, HW_NCPU };

   int count;
   size_t size = sizeof(count);
   if (sysctl(mib, sizeof(mib)/sizeof(mib[0]), &count, &size, NULL, 0) < 0) {
     count = 1;
   }

   return count;
}

#if defined(__i386__) || defined(__x86_64__)
void GetX86ProcessorInfo(uint16_t &level, uint16_t &revision,
                         MDCPUInformation &info) {
  unsigned unused, unused2, unused3;
  unsigned features;

  // get vendor id
  cpuid(0, unused, info.x86_cpu_info.vendor_id[0],
        info.x86_cpu_info.vendor_id[2],
        info.x86_cpu_info.vendor_id[1]);
  // get version and feature info
  cpuid(1, info.x86_cpu_info.version_information, unused, unused2,
        info.x86_cpu_info.feature_information);
  // get highest extended function supported
  cpuid(0x80000000, features, unused, unused2, unused3);

  // family
  level = (info.x86_cpu_info.version_information & 0xF00) >> 8;
  // 0xMMSS (Model, Stepping)
  revision = static_cast<uint16_t>(
      (info.x86_cpu_info.version_information & 0xF) |
      ((info.x86_cpu_info.version_information & 0xF0) << 4));

  // decode extended model info
  if (level == 0xF || level == 0x6) {
    revision |= ((info.x86_cpu_info.version_information & 0xF0000) >> 4);
  }

  // decode extended family info
  if (level == 0xF) {
    level += ((info.x86_cpu_info.version_information & 0xFF00000) >> 20);
  }

  if (features >= 0x80000001) {
    cpuid(0x80000001, unused, features, unused2, unused3);
    info.x86_cpu_info.amd_extended_cpu_features = features;
  }
}
#endif

//
// System Target helper functions.
//

bool HasDefaultCorePattern() {
  return (GetCorePatternType() != 0);
}

std::string GetCoreFileName(pid_t pid) {
  int type = GetCorePatternType();
  if (type == BSD_CORE_PATTERN_CUSTOM)
    return std::string();

  std::string file_name;
  if (type == BSD_CORE_PATTERN_NAMED) {
      char exe_path[PATH_MAX + 1];
      if (!GetProcessExecutablePath(pid, exe_path, sizeof(exe_path)))
        return std::string();

      file_name = basename(exe_path);
      if (file_name.length() > kCOMMLEN) {
        file_name.erase(file_name.begin() + kCOMMLEN, file_name.end());
      }
      file_name += '.';
  }

  return file_name + "core";
}

char *GenerateLinuxCpuInfo(size_t *size) {
  char *info = ReadProcFile("/proc/cpuinfo", size);
  if (info == NULL || *size == 0) {
    if (info != NULL) {
      free(info);
    }
    info = ReadProcFile("/compat/linux/proc/cpuinfo", size);
    if (info == NULL || *size == 0) {
      if (info != NULL) {
        free(info);
      }
      info = MakeLinuxCpuInfo(size);
    }
  }
  return info;
}

//
// Process helper functions.
//

static bool GetProcessAddress(pid_t pid, uintptr_t &address) {
  size_t count;
  ElfW(Auxinfo) *auxv = GetProcessElfAuxVector(pid, &count);
  if (auxv == NULL)
    return false;

  bool success = false;
  address = 0;
  for (size_t n = 0; n < count; n++) {
    if (ELF_AUX_TYPE(&auxv[n]) == AT_ENTRY ||
        ELF_AUX_TYPE(&auxv[n]) == AT_PHDR) {
      address = ELF_AUX_VALUE(&auxv[n]);
      if (address != 0) {
        success = true;
        break;
      }
    }
  }
  free(auxv);

  return success && address != 0;
}

// When iterate mapping, the main executable may have been removed, like
// in the MinidumpWriter.DeletedBinary test, this function tries to recover
// the name reading the command line. This solution is far from being perfect,
// but it is enough to satisfy the requirements.

// This helper class stores the current path, to resolve relative executable
// paths.
struct StartingPath {
  char path[PATH_MAX+1];
  StartingPath() {
    if (getcwd(path, sizeof(path)) == NULL) {
      path[0] = 0;
    }
  }
} starting_path;

bool GetProcessExecutablePath(pid_t pid, char *buf, size_t bufsize,
                              const char **effective) {
  buf[0] = 0;

  // Try first the native approach, if valid ensure it is a regular
  // file and it is executable.
  struct stat st;
  if (ReadProcessExecutablePath(pid, buf, bufsize) && buf[0] &&
      stat(buf, &st) == 0 && (st.st_mode & S_IFMT) == S_IFREG &&
      (st.st_mode & (S_IXUSR|S_IXGRP|S_IXOTH)) != 0) {
    if (effective != NULL) {
      *effective = buf;
    }
    return true;
  }

  // Fallback retrieving the name from the process arguments :(
  size_t size;
  char *args = GetProcessArguments(pid, &size);
  if (args == NULL)
    return false;

  if (args[0] != '/') {
    // non-absolute path, use starting path.
    snprintf(buf, bufsize, "%s/%s", starting_path.path, args);
    if (effective != NULL) {
      *effective = buf + strlen(starting_path.path) + 1;
    }
  } else {
    strlcpy(buf, args, bufsize);
    if (effective != NULL) {
      *effective = buf;
    }
  }
  free(args);

  return true;
}

pid_t GetParentProcessId(pid_t pid) {
  if (pid == 0 || pid == getpid())
    return getppid();

  int mib[] = { CTL_KERN, kKERN_PROC, KERN_PROC_PID, MIB_PID(pid) };

  size_t size;
  if (sysctl(mib, sizeof(mib)/sizeof(int), 0, &size, NULL, 0) < 0)
    return -1;

  kinfo_proc_t *ki = reinterpret_cast<kinfo_proc_t*>(calloc(1, size));
  if (ki == NULL)
    return -1;

  if (sysctl(mib, sizeof(mib)/sizeof(int), ki, &size, NULL, 0) < 0) {
    free(ki);
    return -1;
  }

  pid_t ppid = KinfoProcGetPpid(ki);
  free(ki);

  return ppid;
}

ssize_t ReadProcess(pid_t pid, void *local, uintptr_t remote, size_t length) {
  struct ptrace_io_desc piod;
  // Use PIOD_READ_I to allow read text segments.
  piod.piod_op   = PIOD_READ_I;
  piod.piod_offs = reinterpret_cast<void*>(remote);
  piod.piod_addr = local;
  piod.piod_len  = length;
  if (ptrace(PT_IO, pid, reinterpret_cast<caddr_t>(&piod), 0) < 0)
    return -1;
  return piod.piod_len;
}

ssize_t WriteProcess(pid_t pid, uintptr_t remote, const void *local,
                     size_t length) {
  struct ptrace_io_desc piod;
  // Use PIOD_WRITE_I to allow writing text segments.
  piod.piod_op   = PIOD_WRITE_I;
  piod.piod_offs = reinterpret_cast<void*>(remote);
  piod.piod_addr = const_cast<void*>(local);
  piod.piod_len  = length;
  if (ptrace(PT_IO, pid, reinterpret_cast<caddr_t>(&piod), 0) < 0)
    return -1;
  return piod.piod_len;
}

ssize_t ReadProcessString(pid_t pid, char *dst, uintptr_t src, size_t maxlen) {
  ssize_t len = 0;
  while (maxlen > 0) {
    len = ReadProcess(pid, dst, src, maxlen);
    if (len < 0) {
      if (errno == EINVAL) {
          maxlen--;
          continue;
      }
      return -1;
    }
    maxlen = len;
    break;
  }
  dst[len] = '\0';
  return len;
}

bool GetProcessThreads(pid_t pid, wasteful_vector<lwpid_t> &lwpids) {
  std::vector<lwpid_t> vlwpids;
  if (!GetProcessThreads(pid, vlwpids))
    return false;

  for (size_t n = 0; n < vlwpids.size(); n++) {
    lwpids.push_back(vlwpids[n]);
  }
  return true;
}

//
// ELF Helper Functions
//

template <typename T>
static inline bool ElfAuxVectorFind(ElfW(Auxinfo) const *ai, ElfW(Word) id,
                                    T &value) {
  for (size_t n = 0; ELF_AUX_TYPE(&ai[n]) != AT_NULL; n++) {
    if (ELF_AUX_TYPE(&ai[n]) == id) {
      value = static_cast <T> (ELF_AUX_VALUE(&ai[n]));
      return true;
    }
  }
  return false;
}

static ElfW(Phdr) *ReadProcessProgramHeaders(pid_t pid, uintptr_t base,
                                             uintptr_t offset, size_t phnum,
                                             size_t phent) {
  Elf_Phdr *phdrs = (Elf_Phdr *)calloc(phnum, phent);
  if (phdrs == NULL)
    return NULL;

  if (ReadProcess(pid, phdrs, offset, phnum * phent) != phnum * phent) {
    free(phdrs);
    return NULL;
  }

  // Fix all virtual addresses
  for (size_t n = 0; n < phnum; n++) {
    switch (phdrs[n].p_type) {
      case PT_LOAD:
      case PT_DYNAMIC:
      case PT_INTERP:
      case PT_NOTE:
      case PT_SHLIB:
      case PT_PHDR:
      case PT_TLS:
        phdrs[n].p_vaddr += base;
        break;
    }
  }

  return phdrs;
}

static ElfW(Phdr) *ReadProcessProgramHeaders(pid_t pid, ElfW(Auxinfo) const *ai,
                                             size_t *count) {
  uintptr_t phoff;
  size_t pagesz;
  size_t phnum;
  size_t phent;

  if (!ElfAuxVectorFind(ai, AT_PHENT, phent) || phent != sizeof(ElfW(Phdr)))
    return NULL;
  if (!ElfAuxVectorFind(ai, AT_PHDR, phoff))
    return NULL;
  if (!ElfAuxVectorFind(ai, AT_PHNUM, phnum))
    return NULL;
  if (!ElfAuxVectorFind(ai, AT_PAGESZ, pagesz)) {
    pagesz = getpagesize();
  }

  uintptr_t base = phoff & -pagesz;
  ElfW(Phdr) *phdrs = ReadProcessProgramHeaders(pid, base, phoff, phnum, phent);
  if (phdrs == NULL)
    return NULL;

  *count = phnum;
  return phdrs;
}

static inline const ElfW(Phdr) *ElfPhdrFind(const Elf_Phdr *phdrs, size_t phnum,
                                            ElfW(Word) type) {
  for (size_t n = 0; n < phnum; n++) {
    if (phdrs[n].p_type == type)
      return &phdrs[n];
  }
  return NULL;
}

template <typename T>
static inline bool ElfDynFind(const ElfW(Dyn) *dyn, ElfW(Word) tag, T &value) {
  for (size_t n = 0; dyn[n].d_tag != AT_NULL; n++) {
    if (dyn[n].d_tag == tag) {
      value = static_cast <T> (dyn[n].d_un.d_val);
      return true;
    }
  }
  return false;
}

// This function is the helper function of ReadProcessLinkerMap, given a
// process id |pid|, a load base |address|, a file |path| and a vector
// of |mappings|, iterate all the program headers and insert all the
// PT_LOAD segments.
static void ReadProcessLoadHeaders(pid_t pid, uintptr_t base, const char *path,
                                   std::vector <Mapping> &mappings) {
  ElfW(Ehdr) ehdr;
  if (ReadProcess(pid, &ehdr, base, sizeof(ehdr)) != sizeof(ehdr))
    return;

  if (ehdr.e_phentsize != sizeof(Elf_Phdr))
    return;

  ElfW(Phdr) *phdrs = ReadProcessProgramHeaders(pid, base, base + ehdr.e_phoff,
                                                ehdr.e_phnum, ehdr.e_phentsize);
  if (phdrs == NULL)
      return;

  for (size_t n = 0; n < ehdr.e_phnum; n++) {
      switch (phdrs[n].p_type) {
        case PT_LOAD:
        case PT_DYNAMIC:
        case PT_INTERP:
        case PT_NOTE:
        case PT_SHLIB:
        case PT_PHDR:
        case PT_TLS:
          break;
        default:
          continue;
      }

      Mapping m;
      memset(&m, 0, sizeof(Mapping));
      m.start = phdrs[n].p_vaddr;
      m.end = phdrs[n].p_vaddr + phdrs[n].p_memsz;
      m.offset = phdrs[n].p_offset;
      strlcpy(m.name, path, sizeof(m.name));
      mappings.push_back(m);
  }
  free(phdrs);
}

// Given a process identifier |pid|, read the dynamic linker mappings, this is
// used on OpenBSD that doesn't provide any way to obtain a complete list of
// mapping with paths.
static bool ReadProcessLinkerMap(pid_t pid, std::vector <Mapping> &mappings) {
  // Read in auxv.
  size_t nauxv;
  ElfW(Auxinfo) *auxv = GetProcessElfAuxVector(pid, &nauxv);
  if (auxv == NULL)
    return false;

  // Read in program headers.
  size_t phnum;
  ElfW(Phdr) *phdrs = ReadProcessProgramHeaders(pid, auxv, &phnum);
  free(auxv);
  if (phdrs == NULL)
    return false;

  // Get the PT_DYNAMIC program header.
  const ElfW(Phdr) *phdyn = ElfPhdrFind(phdrs, phnum, PT_DYNAMIC);
  if (phdyn == NULL) {
    free(phdrs);
    return false;
  }

  // Read in the _DYNAMIC symbol.
  ElfW(Dyn) *dyn = (Elf_Dyn *)calloc(1, phdyn->p_memsz);
  if (ReadProcess(pid, dyn, phdyn->p_vaddr, phdyn->p_memsz) != phdyn->p_memsz) {
    free(phdrs);
    return false;
  }

  free(phdrs);

  // Find the DT_DEBUG for DSO debug information.
  uintptr_t debugoff;
  if (!ElfDynFind(dyn, DT_DEBUG, debugoff)) {
    free(dyn);
    return false;
  }

  free(dyn);

  // Bail out if it's zero.
  if (debugoff == 0)
    return false;

  // Read the debug structure.
  struct r_debug debug;
  if (ReadProcess(pid, &debug, debugoff, sizeof(debug)) != sizeof(debug))
    return false;

  mappings.clear();

  // Iterate all the entries and add mappings.
  uintptr_t lmapoff = reinterpret_cast<uintptr_t>(debug.r_map);
  while (lmapoff != 0) {
    struct link_map lmap;
    if (ReadProcess(pid, &lmap, lmapoff, sizeof(lmap)) != sizeof(lmap))
      break;

    if (lmap.l_name != NULL) {
        char path[PATH_MAX + 1];
        if (ReadProcessString(pid, path,
                              reinterpret_cast<uintptr_t>(lmap.l_name),
                              PATH_MAX) > 0) {
          ReadProcessLoadHeaders(pid, reinterpret_cast<uintptr_t>(lmap.l_addr),
                                 path, mappings);
        }
    }
    lmapoff = (uintptr_t)lmap.l_next;
  }

  return !mappings.empty();
}

//
// Cloning thread helper functions.
//

// These function exists for all the BSDs that do need to ptrace(2)
// attach the target process to extract information. So far, only
// FreeBSD can avoid to attach to obtain all the information required.

static pthread_mutex_t thread_pid_lock_ = PTHREAD_MUTEX_INITIALIZER;
static std::map<pid_t, int> thread_pids_;

static pid_t ForkSplitThread(int (*entry)(void *), void *arg) {
  // On OpenBSD we really return a file descriptor, because we need
  // the thread to be on a different session than the calling (crashing)
  // process.
  int fds[2];

  if (pipe(fds) < 0)
    return -1;

  pid_t pid = fork();
  if (pid == 0) {
    if (setsid() < 0) {
      _exit(1);
    }

    pid_t pid = fork();
    if (pid == 0) {
      pid_t pid = fork();
      if (pid < 0) {
        _exit(1);
      }

      if (pid == 0) {
        _exit((*entry)(arg));
      }

      close(fds[0]);

      // Signal process is ready.
      write(fds[1], &pid, sizeof(pid));

      // wait for it to complete.
      int status = 0;
      waitpid(pid, &status, 0);
      write(fds[1], &status, sizeof(status));
      _exit(0);
    }
#if defined(__DragonFly__)
    // Eek... Kernel panic without this!
    waitpid(pid, NULL, 0);
#endif
    _exit(0);
  }

  close(fds[1]);
  // Wait for process to start.
  pid_t pid2;
  if (read(fds[0], &pid2, sizeof(pid2)) != sizeof(pid2)) {
    close(fds[0]);
    kill(pid2, SIGKILL);
    kill(pid, SIGKILL);
    errno = EINVAL;
    return -1;
  }

  pthread_mutex_lock(&thread_pid_lock_);
  thread_pids_[pid] = fds[0];
  pthread_mutex_unlock(&thread_pid_lock_);

  return pid;
}

static pid_t WaitSplitForkedThread(pid_t pid, int *status, int flags) {
  pthread_mutex_lock(&thread_pid_lock_);
  int fd = thread_pids_[pid];
  if (fd == 0) {
    pthread_mutex_unlock(&thread_pid_lock_);
    errno = ESRCH;
    return -1;
  }

  struct pollfd pfd;
  pfd.fd = fd;
  pfd.events = POLLIN;

  if (flags & WNOHANG) {
    // If polling fails or receives an HUP, then the process died.
    if (HANDLE_EINTR(poll(&pfd, 1, 0)) < 0 ||
        (pfd.revents & POLLHUP) != 0) {
      // Remove zombie process
      waitpid(pid, NULL, 0);

      close(fd);
      thread_pids_.erase(pid);
      pthread_mutex_unlock(&thread_pid_lock_);
      errno = ESRCH;
      return -1;
    }
  }

  // Erase the process from the thread_pids_ map
  thread_pids_.erase(pid);
  pthread_mutex_unlock(&thread_pid_lock_);

  // If polling fails or receives an HUP, then the process died.
  if (HANDLE_EINTR(poll(&pfd, 1, -1)) < 0 ||
      (pfd.revents & POLLHUP) != 0) {
    // Remove zombie process
    waitpid(pid, NULL, 0);

    close(fd);
    errno = ESRCH;
    return -1;
  }

  int rstatus;
  bool success = (read(fd, &rstatus, sizeof(rstatus)) == sizeof(rstatus));
  close(fd);

  if (!success) {
    rstatus = 0;
    errno = ESRCH;
  } else if (status != NULL) {
    *status = rstatus;
  }

  // Remove zombie process
  waitpid(pid, NULL, 0);

  return success ? 0 : -1;
}

//
// Miscellaneous helper functions.
//

void MakeProcPath(pid_t pid, const char *name, char *buf, size_t bufsize) {
  if (pid == 0 || pid == getpid()) {
    snprintf(buf, bufsize, "/proc/curproc/%s", name);
  } else {
    snprintf(buf, bufsize, "/proc/%d/%s", pid, name);
  }
}

char *ReadProcFile(pid_t pid, const char *name, size_t *fsize) {
  char path[PATH_MAX + 1];
  MakeProcPath(pid, name, path, sizeof(path));
  return ReadProcFile(path, fsize);
}

char *ReadProcFile(const char *filename, size_t *size) {
  const int fd = open(filename, O_RDONLY, 0);
  if (fd < 0)
    return NULL;

  // We can't stat the files because several of the files that we want to
  // read are kernel seqfiles, which always have a length of zero. So we have
  // to read as much as we can into a buffer.
  // Contrary to Linux, the /proc on BSD behaves differently and
  // partial reads will result in truncated output, as such we
  // need to increase the buffer and re-read the whole file again
  // from the start.
  const ssize_t kInitialBufSize = getpagesize();
  char *buf = NULL;
  ssize_t bufsize = 0;
  ssize_t nread = 0;
  bool fail = false;

  for (;;) {
    if (bufsize == 0) {
      bufsize = kInitialBufSize;
    } else {
      bufsize <<= 1;
      if (bufsize < kInitialBufSize) {
        fail = true;
        break;
      }
    }

    char *newbuf = reinterpret_cast<char*>(realloc(buf, bufsize));
    if (newbuf == NULL) {
      fail = true;
      break;
    }
    buf = newbuf;

    errno = 0;
    do {
      nread = pread(fd, buf, bufsize, 0);
    } while (nread <= 0 && errno == EINTR);

    if (errno == EFBIG)
      continue;

    if (nread < bufsize)
      break;
  }
  close(fd);

  if (fail) {
    free(buf);
    buf = NULL;
    nread = 0;
  }

  *size = nread;
  return buf;
}

void AugmentMappings(pid_t pid, std::vector<Mapping> &result,
                     const std::vector<Mapping> &input,
                     bool extract_exe_name) {
  char exe_path[PATH_MAX + 1];
  const char *exe_name = NULL;
  if (extract_exe_name) {
    if (!GetProcessExecutablePath(pid, exe_path, sizeof(exe_path),
                                  &exe_name)) {
      exe_path[0] = 0;
      exe_name = NULL;
    }
  }

  // Augment the mappings with paths.
  for (size_t i = 0; i < input.size(); i++) {
    const Mapping &min = input[i];
    for (size_t j = 0; j < result.size(); j++) {
      Mapping &mout = result[j];
      if (min.start >= mout.start && min.end <= mout.end) {
        if (exe_name != NULL && strcmp(exe_name, min.name) == 0) {
          strlcpy(mout.name, exe_path, sizeof(mout.name));
        } else {
          strlcpy(mout.name, min.name, sizeof(mout.name));
        }
      }
    }
  }

  // Now do it again, but this time include the executable mappings
  // which may be missing.
  for (size_t i = 0; i < input.size(); i++) {
    const Mapping &min = input[i];
    // We're interested only in executable mappings.
    if ((min.protection & PROT_EXEC) == 0)
      continue;

    uintptr_t last_end = 0;
    for (size_t j = 0; j < result.size(); j++) {
      Mapping &mout = result[j];

      // Skip past entries already done.
      if (min.start >= mout.start && min.end <= mout.end) {
        last_end = mout.end;
        continue;
      }

      // We can't merge overlapping regions, as such we make sure
      // that we don't stomp over an existing region.
      if (min.start >= last_end && min.end < mout.start) {
        // We're right in the middle of a hole, insert at this point
        // the entry.
        result.insert(result.begin() + j, min);
        break;
      }

      last_end = mout.end;
    }
  }
}

} }
