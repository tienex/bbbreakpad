// Copyright (c) 2015, Orlando Bassotto.
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are
// met:
//
//     * Redistributions of source code must retain the above copyright
// notice, this list of conditions and the following disclaimer.
//     * Redistributions in binary form must reproduce the above
// copyright notice, this list of conditions and the following disclaimer
// in the documentation and/or other materials provided with the
// distribution.
//     * Neither the name of Google Inc. nor the names of its
// contributors may be used to endorse or promote products derived from
// this software without specific prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
// "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
// LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
// A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
// OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
// SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
// LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
// DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
// THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
// (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
// OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

#ifndef CLIENT_BSD_BSD_TYPES_H__
#define CLIENT_BSD_BSD_TYPES_H__

#include <sys/types.h>
#include <sys/time.h>

#if defined(__OpenBSD__)
// Define _DYN_LOADER to have AuxInfo
#define _DYN_LOADER 
#include <elf_abi.h>
#else
#include <elf.h>
#endif

#include <time.h>
#include <unistd.h>

//
// ELF specifics.
//
#if defined(__DragonFly__)

#define ElfW(x) __ElfN(x)

#define Elf32_Nhdr Elf_Note
#define Elf64_Nhdr Elf_Note

#define ELF_AUX_TYPE(x) ((x)->a_type)
#define ELF_AUX_VALUE(x) ((x)->a_un.a_val)

#elif defined(__FreeBSD__)

#define ElfW(x) __ElfN(x)

#define ELF_AUX_TYPE(x) ((x)->a_type)
#define ELF_AUX_VALUE(x) ((x)->a_un.a_val)

#elif defined(__NetBSD__)

#define __ELF_WORD_SIZE ARCH_ELFSIZE

#if ARCH_ELFSIZE == 32
#define ElfW(x) Elf32_##x
#define ELF_CLASS ELFCLASS32
#else
#define ElfW(x) Elf64_##x
#define ELF_CLASS ELFCLASS64
#endif

#define Elf32_Auxinfo Aux32Info
#define Elf64_Auxinfo Aux64Info

#define ELF_AUX_TYPE(x) ((x)->a_type)
#define ELF_AUX_VALUE(x) ((x)->a_v)

#elif defined(__OpenBSD__)

typedef pid_t lwpid_t;

#ifndef EM_X86_64
#define EM_X86_64 EM_AMD64
#endif
#ifndef EM_PPC64
#define EM_PPC64 21
#endif
#ifndef EM_S390
#define EM_S390 22
#endif

#define __ELF_WORD_SIZE ELFSIZE

#if ELFSIZE == 32
#define ElfW(x) Elf32_##x
#define ELF_CLASS ELFCLASS32
#else
#define ElfW(x) Elf64_##x
#define ELF_CLASS ELFCLASS64
#endif

typedef struct {
  Elf32_Word n_namesz;
  Elf32_Word n_descsz;
  Elf32_Word n_type;
} Elf32_Nhdr;

typedef struct {
  Elf64_Half n_namesz;
  Elf64_Half n_descsz;
  Elf64_Half n_type;
} Elf64_Nhdr;

#define Elf32_Auxinfo Aux32Info
#define Elf64_Auxinfo Aux64Info

#define ELF_AUX_TYPE(x) ((x)->au_id)
#define ELF_AUX_VALUE(x) ((x)->au_v)

#else
#error "I don't know this BSD!"
#endif

namespace google_breakpad { namespace bsd {

struct Mapping {
  enum {
    PROT_PRIVATE = 0x10000000
  };

  uintptr_t start, end;
  off_t offset;
  uint32_t protection;
  char name[PATH_MAX + 1];
};

struct ProcessTimes {
  struct timespec start;
  struct timespec user;
  struct timespec system;
};

} }

#endif  // !CLIENT_BSD_BSD_TYPES_H__
