// Copyright (c) 2015, Orlando Bassotto.
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are
// met:
//
//     * Redistributions of source code must retain the above copyright
// notice, this list of conditions and the following disclaimer.
//     * Redistributions in binary form must reproduce the above
// copyright notice, this list of conditions and the following disclaimer
// in the documentation and/or other materials provided with the
// distribution.
//     * Neither the name of Google Inc. nor the names of its
// contributors may be used to endorse or promote products derived from
// this software without specific prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
// "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
// LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
// A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
// OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
// SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
// LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
// DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
// THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
// (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
// OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

#if !defined(__x86_64__)
#error "DragonFly supports only x86-64 target."
#endif

// DragonFly doesn't have segment registers in x86-64 contexts.
#define BSD_NO_DS_SEGMENT
#define BSD_NO_ES_SEGMENT
#define BSD_NO_FS_SEGMENT
#define BSD_NO_GS_SEGMENT

#define GET_GREG_CASE(name, field) \
  case GREG_##name: return gregs.r_##field
#define GET_GREG_CASE_FIXED(name, value) \
  case GREG_##name: return (value)
#define GETP_GREG(name, field) \
  case GREG_##name: return \
      reinterpret_cast<const unsigned long*>(&gregs.r_##field)
#define GETP_CASE_FIXED(name, value) \
  case GREG_##name: { \
    static const unsigned long constant_value = (value); \
    return &constant_value; \
  }
#define SET_GREG_CASE(name, field, value) \
  case GREG_##name: gregs.r_##field = (value); return true
#define SET_GREG_CASE_FIXED(name) \
  case GREG_##name: return true

#define GET_DREG_CASE(index) \
  case index: return dregs.dr[index]
#define GETP_DREG_CASE(index) \
  case index: return reinterpret_cast<const unsigned long*>(&dregs.dr[index])
#define SET_DREG_CASE(index, value) \
  case index: dregs.dr[index] = (value); return true

#define _GET_FP_ENV1(field, cast) _GET_FP_ENV2(field, field, cast)
#define _GET_FP_ENV2(field87, fieldxmm, cast) \
  (is_x87 ? *reinterpret_cast<const cast*>(&fregs.sv_87.sv_env.en_##field87) : \
   *reinterpret_cast<const cast*>(&fregs.sv_xmm.sv_env.en_##fieldxmm))

#define _SET_FP_ENV1(field, cast, value) _SET_FP_ENV2(field, field, cast, value)
#define _SET_FP_ENV2(field87, fieldxmm, cast, value) \
  if (is_x87) { \
    *reinterpret_cast<cast*>(&fregs.sv_87.sv_env.en_##field87) = (value); \
  } else { \
    *reinterpret_cast<cast*>(&fregs.sv_xmm.sv_env.en_##fieldxmm) = (value); \
  } \
  return true

#define GET_FP_ENV_CW _GET_FP_ENV1(cw, uint32_t)
#define SET_FP_ENV_CW(value) _SET_FP_ENV1(cw, uint32_t, value)
#define GET_FP_ENV_SW _GET_FP_ENV1(sw, uint32_t)
#define SET_FP_ENV_SW(value) _SET_FP_ENV1(sw, uint32_t, value)
#define GET_FP_ENV_TW _GET_FP_ENV1(tw, uint32_t)
#define SET_FP_ENV_TW(value) _SET_FP_ENV1(tw, uint32_t, value)
#define GET_FP_ENV_FOP _GET_FP_ENV1(opcode, uint16_t)
#define SET_FP_ENV_FOP(value) _SET_FP_ENV1(opcode, uint16_t, value)
#define GET_FP_ENV_RIP _GET_FP_ENV1(fip, uint64_t)
#define SET_FP_ENV_RIP(value) _SET_FP_ENV1(fip, uint64_t, value)
#define GET_FP_ENV_RDP _GET_FP_ENV1(foo, uint64_t)
#define SET_FP_ENV_RDP(value) _SET_FP_ENV1(foo, uint64_t, value)
#define GET_FP_ENV_MXCSR_MASK (is_x87 ? 0 : fregs.sv_xmm.sv_env.en_pad2)
#define SET_FP_ENV_MXCSR_MASK(value) \
  if (!is_x87) { \
    fregs.sv_xmm.sv_env.en_pad2 = value; \
    return true; \
  } \
  break
#define GET_FP_ENV_MXCSR (is_x87 ? 0 : fregs.sv_xmm.sv_env.en_mxcsr)
#define SET_FP_ENV_MXCSR(value) \
  if (!is_x87) { \
    fregs.sv_xmm.sv_env.en_mxcsr = value; \
    return true; \
  } \
  break

#define FP_REG_ST(n) \
  (is_x87 ? (void*)(&fregs.sv_87.sv_ac[n]) : (void*)(&fregs.sv_xmm.sv_fp[n]))
#define FP_REG_ST_LEN \
  (is_x87 ? sizeof(fregs.sv_87.sv_ac[0]) : sizeof(fregs.sv_xmm.sv_fp[0]))
#define FP_REG_XMM(n) \
  (is_x87 ? NULL : &fregs.sv_xmm.sv_xmm[n])
#define FP_REG_XMM_LEN \
  (is_x87 ? 0 : sizeof(fregs.sv_xmm.sv_xmm[0]))

#include "common/bsd/bsd_x86_threadcontext.inc.cc"

namespace google_breakpad { namespace bsd {

ThreadContext::ThreadContext(const ucontext_t &uc) {
#define GREG_COPY(reg) gregs.r_##reg = uc.uc_mcontext.mc_##reg
  GREG_COPY(rax);
  GREG_COPY(rbx);
  GREG_COPY(rcx);
  GREG_COPY(rdx);
  GREG_COPY(rsi);
  GREG_COPY(rdi);
  GREG_COPY(rbp);
  GREG_COPY(rsp);
  GREG_COPY(r8);
  GREG_COPY(r9);
  GREG_COPY(r10);
  GREG_COPY(r11);
  GREG_COPY(r12);
  GREG_COPY(r13);
  GREG_COPY(r14);
  GREG_COPY(r15);
  GREG_COPY(rip);
  GREG_COPY(rflags);
  GREG_COPY(cs);
  GREG_COPY(ss);

  if (uc.uc_mcontext.mc_ownedfp == _MC_FPOWNED_NONE) {
    memset(&fregs, 0, sizeof(fregs));
  } else {
    if (uc.uc_mcontext.mc_fpformat == _MC_FPFMT_387) {
      memcpy(&fregs, uc.uc_mcontext.mc_fpregs, sizeof(struct save87));
      is_x87 = true;
    } else if (uc.uc_mcontext.mc_fpformat == _MC_FPFMT_XMM) {
      memcpy(&fregs, uc.uc_mcontext.mc_fpregs, sizeof(struct savexmm));
      is_x87 = false;
    } else if (uc.uc_mcontext.mc_fpformat == _MC_FPFMT_YMM) {
      memcpy(&fregs, uc.uc_mcontext.mc_fpregs, sizeof(struct saveymm));
      is_x87 = false;
    }
  }

  memset(&dregs, 0, sizeof(dregs));
}

} }
