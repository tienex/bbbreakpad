// Copyright (c) 2015, Orlando Bassotto.
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are
// met:
//
//     * Redistributions of source code must retain the above copyright
// notice, this list of conditions and the following disclaimer.
//     * Redistributions in binary form must reproduce the above
// copyright notice, this list of conditions and the following disclaimer
// in the documentation and/or other materials provided with the
// distribution.
//     * Neither the name of Google Inc. nor the names of its
// contributors may be used to endorse or promote products derived from
// this software without specific prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
// "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
// LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
// A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
// OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
// SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
// LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
// DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
// THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
// (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
// OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

namespace google_breakpad { namespace bsd {

static const size_t kELF_AUX_ENTRIES = 16;
static const int kKERN_PROC = KERN_PROC2;
static const int kKERN_LWP = KERN_LWP;
static const int kKERN_LWP_FLAG = 0;
static const char *kPMAP_EXECUTABLE_PATH = "/usr/bin/pmap";
static const char *kPROCFS_EXECUTABLE_FILE_LINK_NAME = "exe";
typedef struct kinfo_proc2 kinfo_proc_t;
typedef struct kinfo_lwp kinfo_lwp_t;

//
// Forward declaration in bsd_netbsd_openbsd_utils_shared.inc.cc
//
static char *ReadProcessArguments(pid_t pid, size_t *bufsize);
static char *ReadProcessEnvironment(pid_t pid, size_t *bufsize);

//
// System Target helper functions.
//

bool GetSystemVersion(unsigned &major, unsigned &minor, unsigned &build) {
  struct utsname uts;
  if (uname(&uts))
    return false;

  // NetBSD MAJOR.MINOR.PATCH
  return (sscanf(uts.version, "%*s %u.%u.%u", &major, &minor, &build) == 3);
}

bool GetCorePattern(char *buf, size_t bufsize) {
  if (sysctlbyname("kern.defcorename", buf, &bufsize, NULL, 0) < 0)
    return false;

  buf[bufsize] = '\0';
  return true;
}

//
// Process helper functions.
//

static inline pid_t KinfoProcGetPpid(const kinfo_proc_t *kp) {
  return kp->p_ppid;
}

static inline lwpid_t KinfoLwpGetId(const kinfo_lwp_t *kl) {
  return kl->l_lid;
}

char *GetProcessArguments(pid_t pid, size_t *bufsize) {
  return ReadProcessArguments(pid, bufsize);
}

char *GetProcessEnvironment(pid_t pid, size_t *bufsize) {
  return ReadProcessEnvironment(pid, bufsize);
}

bool GetProcessMappings(pid_t pid, std::vector<Mapping> &mappings) {
  char exe_path[PATH_MAX + 1] = {0};
  uintptr_t exe_address = 0;
  bool deleted = false;

  size_t size;
  char *buf = ReadProcFile(pid, "maps", &size);
  if (buf == NULL)
    return false;

  // The convoluted logic here is to deal with deleted binaries.
  if (GetProcessExecutablePath(pid, exe_path, sizeof(exe_path))) {
    if (access(exe_path, F_OK) != 0 &&
        (errno == ENOENT || errno == ENOTDIR)) {
      deleted = true;
      if (!GetProcessAddress(pid, exe_address)) {
        exe_address = 0;
      }
    }
  }

  char *line = buf;
  while ((line - buf) < size) {
    unsigned long long start, end, offset;
    char r, w, x, p;
    int nfields, pos = 0;
    char *next = strchr(line, '\n');

    *next = 0;

    nfields = sscanf(line, "%llx-%llx %c%c%c%c %llx %*x:%*x %*u %n",
            &start, &end, &r, &w, &x, &p, &offset, &pos);
    if (nfields != 7)
      continue;

    Mapping m;
    memset(&m, 0, sizeof(m));

    m.start = start;
    m.end = end;
    m.offset = offset;
    if (deleted && exe_address >= m.start && exe_address < m.end) {
      strlcpy(m.name, exe_path, sizeof(m.name));
    } else {
      strlcpy(m.name, line + pos, sizeof(m.name));
    }

    if (p == 'p') {
      m.protection |= Mapping::PROT_PRIVATE;
    }

    if (x == 'x') {
      m.protection |= PROT_EXEC;
    }
    if (w == 'w') {
      m.protection |= PROT_WRITE;
    }
    if (r == 'r') {
      m.protection |= PROT_READ;
    }

    mappings.push_back(m);

    line = next + 1;
  }

  free(buf);
  return true;
}

//
// Thread helper functions.
//

lwpid_t GetThreadId() {
  return _lwp_self();
}

bool KillThread(pid_t pid, lwpid_t lwpid, int sig) {
  if (pid == 0 || pid == getpid())
    return KillThread(lwpid, sig);
  else
    return false;
}

bool KillThread(lwpid_t lwpid, int sig) {
  return _lwp_kill(lwpid, sig);
}

bool GetThreadContext(pid_t pid, lwpid_t lwpid, ThreadContext &context) {
  memset(&context, 0, sizeof(context));

  if (ptrace(PT_GETREGS, pid,
             reinterpret_cast<caddr_t>(&context.gregs), lwpid) < 0)
    return false;

  // Ignore results for these.
#if defined(PT_GETFPREGS)
#if defined(__i386__)
#if defined(PT_GEXMMREGS)
  if (ptrace(PT_GETXMMREGS, pid,
             reinterpret_cast<caddr_t>(&context.fregs), lwpid) == 0) {
    context.is_x87 = false;
  } else
#endif
  if (ptrace(PT_GETFPREGS, pid,
             reinterpret_cast<caddr_t>(&context.fregs), lwpid) == 0) {
    context.is_x87 = true;
  } else {
    memset(&context.fregs, 0, sizeof(context.fregs));
  }
#else
  if (ptrace(PT_GETFPREGS, pid,
             reinterpret_cast<caddr_t>(&context.fregs), lwpid) < 0) {
    memset(&context.fregs, 0, sizeof(context.fregs));
  }
#endif
#endif

  return true;
}

} }

//
// Include code shared across NetBSD and OpenBSD.
//
#include "common/bsd/bsd_netbsd_openbsd_utils_shared.inc.cc"

//
// Include code shared across NetBSD and DragonFly.
//
#include "common/bsd/bsd_netbsd_dragonfly_utils_shared.inc.cc"
