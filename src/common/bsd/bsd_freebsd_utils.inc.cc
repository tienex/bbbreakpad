// Copyright (c) 2015, Orlando Bassotto.
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are
// met:
//
//     * Redistributions of source code must retain the above copyright
// notice, this list of conditions and the following disclaimer.
//     * Redistributions in binary form must reproduce the above
// copyright notice, this list of conditions and the following disclaimer
// in the documentation and/or other materials provided with the
// distribution.
//     * Neither the name of Google Inc. nor the names of its
// contributors may be used to endorse or promote products derived from
// this software without specific prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
// "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
// LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
// A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
// OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
// SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
// LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
// DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
// THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
// (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
// OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

//
// Code specific to FreeBSD.
//

namespace google_breakpad { namespace bsd {

static const size_t kCOMMLEN = COMMLEN;
static const int kKERN_PROC = KERN_PROC;
static const int kKERN_PROC_INC_THREAD = KERN_PROC_INC_THREAD;
typedef struct ::kinfo_proc kinfo_proc_t;

//
// System Target helper functions.
//

bool GetSystemVersion(unsigned &major, unsigned &minor, unsigned &build) {
  struct utsname uts;
  if (uname(&uts))
    return false;

  // FreeBSD MAJOR.MINOR-RELEASETYPE-pN
  build = 0;
  return sscanf(uts.version, "%*s %u.%u-%*[^-]s-p%u",
                &major, &minor, &build) >= 2;
}

//
// Process helper functions.
//

static inline pid_t KinfoProcGetPpid(const kinfo_proc_t *kp) {
  return kp->ki_ppid;
}

static inline lwpid_t KinfoProcGetLWPid(const kinfo_proc_t *kp) {
  return kp->ki_tid;
}

static inline struct timespec KinfoProcGetStartTime(const kinfo_proc_t *kp) {
  struct timespec ts;
  ts.tv_sec  = kp->ki_start.tv_sec;
  ts.tv_nsec = kp->ki_start.tv_usec * 1000;
  return ts;
}

static inline struct timespec KinfoProcGetUserTime(const kinfo_proc_t *kp) {
  struct timespec ts;
  ts.tv_sec  = kp->ki_rusage.ru_utime.tv_sec;
  ts.tv_nsec = kp->ki_rusage.ru_utime.tv_usec * 1000;
  return ts;
}

static inline struct timespec KinfoProcGetSystemTime(const kinfo_proc_t *kp) {
  struct timespec ts;
  ts.tv_sec  = kp->ki_rusage.ru_stime.tv_sec;
  ts.tv_nsec = kp->ki_rusage.ru_stime.tv_usec * 1000;
  return ts;
}

static bool ReadProcessExecutablePath(pid_t pid, char *buf, size_t bufsize) {
  int mib[] = { CTL_KERN, KERN_PROC, KERN_PROC_PATHNAME, MIB_PID(pid) };

  if (sysctl(mib, sizeof(mib)/sizeof(int), buf, &bufsize, NULL, 0) < 0)
    return false;

  buf[bufsize] = 0;
  return true;
}

ElfW(Auxinfo) *GetProcessElfAuxVector(pid_t pid, size_t *count) {
  int mib[] = { CTL_KERN, KERN_PROC, KERN_PROC_AUXV, MIB_PID(pid) };

  size_t size;
  if (sysctl(mib, sizeof(mib)/sizeof(int), 0, &size, NULL, 0) < 0)
    return NULL;

  char *buf = reinterpret_cast<char*>(calloc(1, size));
  if (buf == NULL)
    return NULL;

  if (sysctl(mib, sizeof(mib)/sizeof(int), buf, &size, NULL, 0) < 0) {
    free(buf);
    return NULL;
  }

  *count = size / sizeof(ElfW(Auxinfo));
  return reinterpret_cast<ElfW(Auxinfo)*>(buf);
}

char *GetProcessArguments(pid_t pid, size_t *bufsize) {
  int mib[] = { CTL_KERN, KERN_PROC, KERN_PROC_ARGS, MIB_PID(pid) };

  size_t size;
  if (sysctl(mib, sizeof(mib)/sizeof(int), 0, &size, NULL, 0) < 0)
    return NULL;

  char *buf = reinterpret_cast<char*>(calloc(1, size));
  if (buf == NULL)
    return NULL;

  if (sysctl(mib, sizeof(mib)/sizeof(int), buf, &size, NULL, 0) < 0) {
    free(buf);
    return NULL;
  }

  *bufsize = size;
  return buf;
}

char *GetProcessEnvironment(pid_t pid, size_t *bufsize) {
  int mib[] = { CTL_KERN, KERN_PROC, KERN_PROC_ENV, MIB_PID(pid) };

  size_t size;
  if (sysctl(mib, sizeof(mib)/sizeof(int), 0, &size, NULL, 0) < 0)
    return NULL;

  char *buf = reinterpret_cast<char*>(calloc(1, size));
  if (buf == NULL)
    return NULL;

  if (sysctl(mib, sizeof(mib)/sizeof(int), buf, &size, NULL, 0) < 0) {
    free(buf);
    return NULL;
  }

  *bufsize = size;
  return buf;
}

bool GetProcessMappings(pid_t pid, std::vector<Mapping> &mappings) {
  int count;
  struct kinfo_vmentry *entries;
  char exe_path[PATH_MAX + 1];
  uintptr_t exe_address;
  bool deleted = false;

  entries = kinfo_getvmmap(pid, &count);
  if (entries == NULL)
    return false;

  // The convoluted logic here is to deal with deleted binaries.
  if (GetProcessExecutablePath(pid, exe_path, sizeof(exe_path))) {
    if (access(exe_path, F_OK) != 0 &&
        (errno == ENOENT || errno == ENOTDIR)) {
      deleted = true;
      if (!GetProcessAddress(pid, exe_address)) {
        exe_address = 0;
      }
    }
  }

  for (int n = 0; n < count; n++) {
    Mapping m;
    memset(&m, 0, sizeof(m));

    m.start = entries[n].kve_start;
    m.end = entries[n].kve_end;
    m.offset = entries[n].kve_offset;
    if (deleted && entries[n].kve_type == KVME_TYPE_VNODE &&
        exe_address && exe_address >= m.start && exe_address < m.end) {
      strlcpy(m.name, exe_path, sizeof(m.name));
    } else if (isascii(entries[n].kve_path[0])) {
      strlcpy(m.name, entries[n].kve_path, sizeof(m.name));
    }

    if ((entries[n].kve_flags & KVME_FLAG_COW) != 0) {
      m.protection |= Mapping::PROT_PRIVATE;
    }

    if (entries[n].kve_protection & KVME_PROT_EXEC) {
      m.protection |= PROT_EXEC;
    }
    if (entries[n].kve_protection & KVME_PROT_WRITE) {
      m.protection |= PROT_WRITE;
    }
    if (entries[n].kve_protection & KVME_PROT_READ) {
      m.protection |= PROT_READ;
    }

    mappings.push_back(m);
  }

  free(entries);

  return true;
}

//
// Thread helper functions.
//

lwpid_t GetThreadId() {
  long tid;
  if (thr_self(&tid) < 0) {
    tid = -1;
  }
  return static_cast<lwpid_t>(tid);
}

bool KillThread(pid_t pid, lwpid_t lwpid, int sig) {
  return thr_kill2(pid, lwpid, sig) == 0;
}

bool KillThread(lwpid_t lwpid, int sig) {
  return thr_kill(lwpid, sig) == 0;
}

bool GetThreadContext(pid_t, lwpid_t lwpid, ThreadContext &context) {
  memset(&context, 0, sizeof(context));

  if (ptrace(PT_GETREGS, lwpid,
             reinterpret_cast<caddr_t>(&context.gregs), 0) < 0)
    return false;

  // Ignore results for these.
#if defined(PT_GETFPREGS)
#if defined(__i386__)
#if defined(PT_GEXMMREGS)
  if (ptrace(PT_GETXMMREGS, lwpid,
             reinterpret_cast<caddr_t>(&context.fregs), 0) == 0) {
    context.is_x87 = false;
  } else
#endif
  if (ptrace(PT_GETFPREGS, lwpid,
             reinterpret_cast<caddr_t>(&context.fregs), 0) == 0) {
    context.is_x87 = true;
  } else {
    memset(&context.fregs, 0, sizeof(context.fregs));
  }
#else
  if (ptrace(PT_GETFPREGS, lwpid,
             reinterpret_cast<caddr_t>(&context.fregs), 0) < 0) {
    memset(&context.fregs, 0, sizeof(context.fregs));
  }
#endif
#endif

#if defined(__i386__) || defined(__x86_64__)
  if (ptrace(PT_GETDBREGS, lwpid,
             reinterpret_cast<caddr_t>(&context.dregs), 0) < 0) {
    memset(&context.dregs, 0, sizeof(context.dregs));
  }
#endif

  return true;
}

//
// Cloning thread helper functions.
//

bool NeedForkThreadStack() {
  return true;
}

pid_t ForkThread(int (*entry)(void *), void *arg, void *stack,
                 size_t stacksize) {
  void *stacktop =
    reinterpret_cast<void*>(reinterpret_cast<uintptr_t>(stack) + stacksize);
  return rfork_thread(RFPROC|RFMEM|RFTHREAD, stacktop, entry, arg);
}

pid_t WaitForkedThread(pid_t pid, int *status, int flags) {
  return waitpid(pid, status, flags);
}

} }

//
// Include code shared across FreeBSD and DragonFly.
//
#include "common/bsd/bsd_freebsd_dragonfly_utils_shared.inc.cc"
