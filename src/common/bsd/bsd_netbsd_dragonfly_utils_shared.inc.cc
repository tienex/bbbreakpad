// Copyright (c) 2015, Orlando Bassotto.
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are
// met:
//
//     * Redistributions of source code must retain the above copyright
// notice, this list of conditions and the following disclaimer.
//     * Redistributions in binary form must reproduce the above
// copyright notice, this list of conditions and the following disclaimer
// in the documentation and/or other materials provided with the
// distribution.
//     * Neither the name of Google Inc. nor the names of its
// contributors may be used to endorse or promote products derived from
// this software without specific prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
// "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
// LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
// A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
// OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
// SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
// LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
// DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
// THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
// (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
// OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

//
// Code shared across NetBSD and DragonFly.
//

namespace google_breakpad { namespace bsd {

//
// Process helper functions.
//

static bool GetProcessTos(pid_t pid, TopOfStack &tos) {
  memset(&tos, 0, sizeof(tos));
#if defined(DEFAULT_TOP_OF_STACK_LOCATION64)
  if (ReadProcess(pid, &tos, DEFAULT_TOP_OF_STACK_LOCATION64,
                  sizeof(tos)) == sizeof(tos))
    return true;
#endif
  return ReadProcess(pid, &tos, DEFAULT_TOP_OF_STACK_LOCATION,
                       sizeof(tos)) == sizeof(tos);
}

static bool ReadProcessExecutablePath(pid_t pid, char *buf, size_t bufsize) {
  char path[PATH_MAX + 1];
  MakeProcPath(pid, kPROCFS_EXECUTABLE_FILE_LINK_NAME, path, sizeof(path));
  memset(buf, 0, bufsize);
  if (readlink(path, buf, bufsize) < 0)
    return false;
  struct stat st;
  if (stat(buf, &st) != 0 ||
      ((st.st_mode & S_IFMT) != S_IFREG &&
       (st.st_mode & (S_IXUSR|S_IXGRP|S_IXOTH)) == 0)) {
    *buf = 0;
    return false;
  }
  return true;
}

ElfW(Auxinfo) *GetProcessElfAuxVector(pid_t pid, size_t *count) {
  TopOfStack tos;
  if (!GetProcessTos(pid, tos))
    return NULL;

  size_t size = kELF_AUX_ENTRIES * sizeof(ElfW(Auxinfo));
  char *buf = reinterpret_cast<char*>(calloc(1, size));
  if (buf == NULL)
    return NULL;

  bool success = false;

  uintptr_t offset = (tos.envp + (tos.nenvs + 1) * sizeof(uintptr_t));
  if (pid == 0 || pid == getpid()) {
    memcpy(buf, reinterpret_cast<void*>(offset), size);
    success = true;
  } else {
    size = ReadProcess(pid, buf, offset, size);
    success = !(size < 0);
  }

  if (!success) {
    free(buf);
    return NULL;
  }

  ElfW(Auxinfo) *auxv = reinterpret_cast<ElfW(Auxinfo)*>(buf);
  for (*count = 0; *count < kELF_AUX_ENTRIES; (*count)++) {
    if (ELF_AUX_TYPE(&auxv[*count]) == AT_NULL) {
      (*count)++;
      break;
    }
  }

  return auxv;
}

} }
