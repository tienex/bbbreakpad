// Copyright (c) 2015, Orlando Bassotto.
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are
// met:
//
//     * Redistributions of source code must retain the above copyright
// notice, this list of conditions and the following disclaimer.
//     * Redistributions in binary form must reproduce the above
// copyright notice, this list of conditions and the following disclaimer
// in the documentation and/or other materials provided with the
// distribution.
//     * Neither the name of Google Inc. nor the names of its
// contributors may be used to endorse or promote products derived from
// this software without specific prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
// "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
// LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
// A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
// OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
// SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
// LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
// DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
// THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
// (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
// OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

#ifndef CLIENT_BSD_BSD_FREEBSD_UTILS_H_
#define CLIENT_BSD_BSD_FREEBSD_UTILS_H_

namespace google_breakpad { namespace bsd {

#define BSD_OSNAME "FreeBSD"
#define BSD_PLATFORM_ID MD_OS_FREEBSD

#define ELFOSABI_DEFAULT ELFOSABI_FREEBSD

#if defined(__i386__)
#define UCONTEXT_GPR_SP(uc) ((uc)->uc_mcontext.mc_esp)
#define UCONTEXT_GPR_PC(uc) ((uc)->uc_mcontext.mc_eip)
#elif defined(__x86_64__)
#define UCONTEXT_GPR_SP(uc) ((uc)->uc_mcontext.mc_rsp)
#define UCONTEXT_GPR_PC(uc) ((uc)->uc_mcontext.mc_rip)
#else
#error "Architecture not supported."
#endif

} }

#endif  // CLIENT_BSD_BSD_FREEBSD_UTILS_H_
