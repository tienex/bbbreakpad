// Copyright (c) 2015, Orlando Bassotto.
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are
// met:
//
//     * Redistributions of source code must retain the above copyright
// notice, this list of conditions and the following disclaimer.
//     * Redistributions in binary form must reproduce the above
// copyright notice, this list of conditions and the following disclaimer
// in the documentation and/or other materials provided with the
// distribution.
//     * Neither the name of Google Inc. nor the names of its
// contributors may be used to endorse or promote products derived from
// this software without specific prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
// "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
// LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
// A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
// OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
// SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
// LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
// DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
// THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
// (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
// OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

//
// Code shared across FreeBSD and DragonFly.
//

namespace google_breakpad { namespace bsd {

//
// System Target helper functions.
//

static int GetCorePatternType() {
  char buf[32];
  if (!GetCorePattern(buf, sizeof(buf)))
    return 0;

  if (strlen(buf) == 7 && memcmp(buf, "%N.core", 7) == 0)
    return BSD_CORE_PATTERN_NAMED;
  else if (strlen(buf) == 4 && memcmp(buf, "core", 4) == 0)
    return BSD_CORE_PATTERN_SIMPLE;
  else
    return 0;
}

bool GetCorePattern(char *buf, size_t bufsize) {
  if (sysctlbyname("kern.corefile", buf, &bufsize, NULL, 0) < 0)
    return false;

  buf[bufsize] = 0;
  return true;
}

//
// Process helper functions.
//

bool GetProcessThreads(pid_t pid, std::vector<lwpid_t> &lwpids) {
  int mib[] = { CTL_KERN, kKERN_PROC, KERN_PROC_PID | kKERN_PROC_INC_THREAD,
                MIB_PID(pid) };

  size_t size;
  if (sysctl(mib, sizeof(mib)/sizeof(int), 0, &size, NULL, 0) < 0)
    return false;

  kinfo_proc_t *kp = reinterpret_cast<kinfo_proc_t*>(calloc(1, size));
  if (kp == NULL)
    return false;

  if (sysctl(mib, sizeof(mib)/sizeof(int), kp, &size, NULL, 0) < 0) {
    free(kp);
    return false;
  }

  lwpids.clear();

  size_t count = size / sizeof(kinfo_proc_t);
  for (size_t n = 0; n < count; n++) {
    lwpids.push_back(KinfoProcGetLWPid(&kp[n]));
  }
  free(kp);

  return true;
}

bool GetProcessTimes(pid_t pid, ProcessTimes &times) {
  int mib[] = { CTL_KERN, kKERN_PROC, KERN_PROC_PID, MIB_PID(pid) };

  kinfo_proc_t kp;
  size_t size = sizeof(kp);
  if (sysctl(mib, sizeof(mib)/sizeof(int), &kp, &size, NULL, 0) < 0)
    return false;

  times.start = KinfoProcGetStartTime(&kp);
  times.user = KinfoProcGetUserTime(&kp);
  times.system = KinfoProcGetSystemTime(&kp);
  return true;
}

bool EnableProcessCoreDump(pid_t pid, bool enable) {
#if defined(PROC_TRACE_CTL) && defined(PROC_TRACE_CTL_ENABLE)
  if (pid == 0) {
    pid = getpid();
  }
  int flag = enable ? PROC_TRACE_CTL_ENABLE : PROC_TRACE_CTL_DISABLE;
  return !(procctl(P_PID, pid, PROC_TRACE_CTL, &flag) < 0);
#else
  return true;
#endif
}

} }
