// Copyright (c) 2015, Orlando Bassotto.
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are
// met:
//
//     * Redistributions of source code must retain the above copyright
// notice, this list of conditions and the following disclaimer.
//     * Redistributions in binary form must reproduce the above
// copyright notice, this list of conditions and the following disclaimer
// in the documentation and/or other materials provided with the
// distribution.
//     * Neither the name of Google Inc. nor the names of its
// contributors may be used to endorse or promote products derived from
// this software without specific prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
// "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
// LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
// A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
// OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
// SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
// LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
// DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
// THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
// (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
// OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

#if defined(__i386__)
#define GET_GREG_CASE(name, field) \
  case GREG_##name: return gregs.r_##field
#define GETP_GREG(name, field) \
  case GREG_##name: return \
      reinterpret_cast<const unsigned long*>(&gregs.r_##field)
#define SET_GREG_CASE(name, field, value) \
  case GREG_##name: gregs.r_##field = (value); return true
#elif defined(__x86_64__)
#define GET_GREG_CASE(name, field) \
  case GREG_##name: return gregs.regs[_REG_##name]
#define GETP_GREG(name, field) \
  case GREG_##name: return \
      reinterpret_cast<const unsigned long*>(&gregs.regs[_REG_##name])
#define SET_GREG_CASE(name, field, value) \
  case GREG_##name: gregs.regs[_REG_##name] = (value); return true
#else
#error "This file is only for x86 and x86-64 architectures."
#endif

#define GET_GREG_CASE_FIXED(name, value) \
  case GREG_##name: return (value)
#define GETP_CASE_FIXED(name, value) \
  case GREG_##name: { \
    static const unsigned long constant_value = (value); \
    return &constant_value; \
  }
#define SET_GREG_CASE_FIXED(name) \
  case GREG_##name: return true

#define GET_DREG_CASE(index) \
  case index: return 0
#define GETP_DREG_CASE(index) \
  case index: { static const unsigned long zero = 0; return &zero; }
#define SET_DREG_CASE(index, value) \
  case index: return true;

#if defined(__i386__)
#define _GET_FP_ENV1(field, cast) _GET_FP_ENV2(field, field, cast)
#define _GET_FP_ENV2(field87, fieldxmm, cast) \
  (is_x87 ? *reinterpret_cast<const cast*>(&fregs.sv_87.sv_env.en_##field87) : \
   *reinterpret_cast<const cast*>(&fregs.sv_xmm.sv_env.en_##fieldxmm))

#define _SET_FP_ENV1(field, cast, value) _SET_FP_ENV2(field, field, cast, value)
#define _SET_FP_ENV2(field87, fieldxmm, cast, value) \
  if (is_x87) { \
    *reinterpret_cast<cast*>(&fregs.sv_87.sv_env.en_##field87) = (value); \
  } else { \
    *reinterpret_cast<cast*>(&fregs.sv_xmm.sv_env.en_##fieldxmm) = (value); \
  } \
  return true

#define GET_FP_ENV_CW _GET_FP_ENV1(cw, uint32_t)
#define SET_FP_ENV_CW(value) _SET_FP_ENV1(cw, uint32_t, value)
#define GET_FP_ENV_SW _GET_FP_ENV1(sw, uint32_t)
#define SET_FP_ENV_SW(value) _SET_FP_ENV1(sw, uint32_t, value)
#define GET_FP_ENV_TW _GET_FP_ENV1(tw, uint32_t)
#define SET_FP_ENV_TW(value) _SET_FP_ENV1(tw, uint32_t, value)
#define GET_FP_ENV_FOP _GET_FP_ENV1(opcode, uint16_t)
#define SET_FP_ENV_FOP(value) _SET_FP_ENV1(opcode, uint16_t, value)
#define GET_FP_ENV_FIP _GET_FP_ENV1(fip, uint32_t)
#define SET_FP_ENV_FIP(value) _SET_FP_ENV1(fip, uint32_t, value)
#define GET_FP_ENV_FCS _GET_FP_ENV1(fcs, uint16_t)
#define SET_FP_ENV_FCS(value) _SET_FP_ENV1(fcs, uint16_t, value)
#define GET_FP_ENV_FOO _GET_FP_ENV1(foo, uint32_t)
#define SET_FP_ENV_FOO(value) _SET_FP_ENV1(foo, uint32_t, value)
#define GET_FP_ENV_FOS _GET_FP_ENV1(fos, uint16_t)
#define SET_FP_ENV_FOS(value) _SET_FP_ENV1(fos, uint16_t, value)
#define GET_FP_ENV_MXCSR (is_x87 ? 0 : fregs.sv_xmm.sv_env.en_mxcsr)
#define SET_FP_ENV_MXCSR(value) \
  if (!is_x87) { \
    fregs.sv_xmm.sv_env.en_mxcsr = value; \
    return true; \
  } \
  break

#define FP_REG_ST(n) \
  (is_x87 ? (void*)(&fregs.sv_87.sv_ac[n]) : \
   (void*)(&fregs.sv_xmm.sv_ac[n]))
#define FP_REG_ST_LEN \
  (is_x87 ? sizeof(fregs.sv_87.sv_ac[0]) : \
   sizeof(fregs.sv_xmm.sv_ac[0]))
#define FP_REG_XMM(n) \
  (is_x87 ? NULL : &fregs.sv_xmm.sv_xmmregs[n])
#define FP_REG_XMM_LEN \
  (is_x87 ? 0 : sizeof(fregs.sv_xmm.sv_xmmregs[0]))

#elif defined(__x86_64__)

#define _FP_ENV(name) fregs.fp_fxsave.fx_##name

#define FP_ENV_CW         _FP_ENV(fcw)
#define FP_ENV_SW         _FP_ENV(fsw)
#define FP_ENV_TW         _FP_ENV(ftw)
#define FP_ENV_FOP        _FP_ENV(fop)
#define FP_ENV_RIP        _FP_ENV(rip)
#define FP_ENV_RDP        _FP_ENV(rdp)
#define FP_ENV_MXCSR      _FP_ENV(mxcsr)
#define FP_ENV_MXCSR_MASK _FP_ENV(mxcsr_mask)
#define FP_REG_ST(n)      &fregs.fp_fxsave.fx_st[n]
#define FP_REG_ST_LEN     sizeof(fregs.fp_fxsave.fx_st[0])
#define FP_REG_XMM(n)     &fregs.fp_fxsave.fx_xmm[n]
#define FP_REG_XMM_LEN    sizeof(fregs.fp_fxsave.fx_xmm[0])
#else
#error "This file is only for x86 and x86-64 architectures."
#endif

#include "common/bsd/bsd_x86_threadcontext.inc.cc"

namespace google_breakpad { namespace bsd {

ThreadContext::ThreadContext(const ucontext_t &uc) {
  if (uc.uc_flags & _UC_CPU) {
#if defined(__i386__)
#define GREG_COPY(reg, in) gregs.r_##reg = uc.uc_mcontext.__gregs[_REG_##in]
    GREG_COPY(eax, EAX);
    GREG_COPY(ebx, EBX);
    GREG_COPY(ecx, ECX);
    GREG_COPY(edx, EDX);
    GREG_COPY(esi, ESI);
    GREG_COPY(edi, EDI);
    GREG_COPY(ebp, EBP);
    GREG_COPY(esp, ESP);
    GREG_COPY(eip, EIP);
    GREG_COPY(eflags, EFL);
    GREG_COPY(cs, CS);
    GREG_COPY(ds, DS);
    GREG_COPY(es, ES);
    GREG_COPY(fs, FS);
    GREG_COPY(gs, GS);
    GREG_COPY(ss, SS);
#elif defined(__x86_64__)
    memcpy(&gregs, &uc.uc_mcontext.__gregs, sizeof(gregs));
#else
#error "This file is only for x86 and x86-64 architectures."
#endif
  }

  if ((uc.uc_flags & _UC_FPU) == 0) {
    memset(&fregs, 0, sizeof(fregs));
  } else {
#if defined(__i386__)
    is_x87 = (uc.uc_flags & _UC_FXSAVE) != 0;
    memcpy(&fregs, &uc.uc_mcontext.__fpregs, sizeof(fregs));
#elif defined(__x86_64__)
    memcpy(&fregs, uc.uc_mcontext.__fpregs, sizeof(fregs));
#else
#error "This file is only for x86 and x86-64 architectures."
#endif
  }
}

} }
